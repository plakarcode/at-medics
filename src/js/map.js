
function initMap() {

	var myLatlng = new google.maps.LatLng(51.447042, -0.127849);
	var mapOptions = {
		zoom: 14,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
        animation: google.maps.Animation.DROP,
        scrollwheel: false
	}

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	//Callout Content
	var contentString = 'AT Medics Head Office,<br /> 26-28 Streatham Place<br />London, SW2 4QY';

	//Set window width + content
	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 500
	});

	//Add Marker
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: 'images/map/marker.png'
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
		map.setZoom(16);
	});

	markers.push(marker);

	//Resize Function
	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	});
}

var markers = [];

function myClick(id){
    google.maps.event.trigger(markers[id], 'click');
}

//ON DOCUMENT READY
$(document).ready(function() {

	//contact page map
	var map = $("#map");

	if (map.length) {
	  initMap();
	}


});
