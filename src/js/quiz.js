var atQuiz = (function () {

    var currentQuestion = 1;
    var totalQuestions = $('.question-item').length;

    var resetQuiz = function () {
        
        // close the popup
        $('.mfp-close').click();
        
        // wait a little and then reset the form
        setTimeout(function() {
            
            // hide the result boxes
            $('.js-quiz-successbox').addClass('is-hidden');
            $('.js-quiz-failurebox').addClass('is-hidden');

            // uncheck the checkboxes
            $('.js-quiz .styled-checkbox').prop('checked', false);

            // show the progress bar
            $('.js-quiz').show();
            $('.js-quiz-bar').show();

            // show the first slide
            $('.js-quiz .question-item').removeClass('is-active');
            $('.js-quiz .question-item').first().addClass('is-active');

            // reset the question counter
            currentQuestion = 1;
            totalQuestions = $('.question-item').length;

            // reset the text and buttons
            $('.js-quiz-percentage').text('0');
            $('.js-quiz-progressbar').css('width', '0%');

            $('.js-quiz-nextbutton').attr('disabled', 1).show();
            $('.js-quiz-finbutton').attr('disabled', 1).hide();

            // remove the result graph
            $('.js-quiz-resultgraph').empty();
            
        }, 500);
    };

    var clickAnswer = function () {

        // check how many answers have been click
        var totalChecked = $('.js-question-' + currentQuestion + ' input:checked').length;
        var correctCount = $('.js-question-' + currentQuestion + ' .checkbox_wrap[data-quiz-c=1]').length;
        
        // check if we have supplied enough answers
        if (totalChecked === correctCount) {
            // enable the next button
            $('.js-quiz-nextbutton').removeAttr('disabled').effect('shake');

            // if it's the last question - show the finish button
            if (currentQuestion === totalQuestions) {
                $('.js-quiz-finbutton').removeAttr('disabled').effect('shake');
            }
        } else {
            // disable the next button
            $('.js-quiz-nextbutton').attr('disabled', true);
            $('.js-quiz-finbutton').attr('disabled', true);
        }
    };

    var showNext = function () {
        currentQuestion++;

        $('.js-quiz-nextbutton').attr('disabled', true);

        updateProgress();
    };

    var showPrev = function () {
        if (currentQuestion > 1) {
            currentQuestion--;
        }

        $('.js-quiz-nextbutton').removeAttr('disabled');

        updateProgress();
    };

    var updateProgress = function () {
        
        var progressPercentage = parseInt((currentQuestion - 1) / totalQuestions * 100);

        $('.js-quiz-progressbar').css('width', progressPercentage + '%');
        $('.js-quiz-percentage').text(progressPercentage);
    };

    var showEnd = function (e) {

        e.preventDefault();

        var score = calculateScore();
        setFinalScore(score);

        $('.js-quiz').hide();
        $('.js-quiz-bar').hide();

        if (score === 100) {
            $('.js-quiz-successbox').removeClass('is-hidden');
        } else {
            $('.js-quiz-failurebox').removeClass('is-hidden');
        }
    };

    var calculateScore = function () {

        // count the number of inputs
        var totalInputs = $('.js-quiz .styled-checkbox').length;

        // how many are correctly ticked
        var totalTickRequired = $('.js-quiz .checkbox_wrap[data-quiz-c=1]').length;

        // how many are correctly ticked
        var totalCorrectTicked = $('.js-quiz .checkbox_wrap[data-quiz-c=1] input:checked').length;

        // how many should not be clicked
        var totalUntickRequired = $('.js-quiz .checkbox_wrap[data-quiz-c!=1]').length;

        // home many are correctly not ticked
        var totalIncorrectTicked = $('.js-quiz .checkbox_wrap[data-quiz-c!=1] input:checked').length;

        var totalCorrectUnticked = totalUntickRequired - totalIncorrectTicked;

        var totalCorrect = totalCorrectTicked + totalCorrectUnticked;

        var percentCorrect = parseInt(totalCorrect / totalInputs * 100);

        return percentCorrect;
    };

    var setGraph = function (graphId) {
        
        var el = document.getElementById(graphId); // get canvas

        var options = {
            percent: el.getAttribute('data-percent') || 25,
            size: el.getAttribute('data-size') || 240,
            lineWidth: el.getAttribute('data-line') || 20,
            rotate: el.getAttribute('data-rotate') || 0
        };

        var canvas = document.createElement('canvas');
        var span = document.createElement('span');
        span.textContent = options.percent + '%';

        if (typeof (G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        var ctx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        el.appendChild(span);
        el.appendChild(canvas);

        ctx.translate(options.size / 2, options.size / 2); // change center
        ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        //imd = ctx.getImageData(0, 0, 240, 240);
        var radius = (options.size - options.lineWidth) / 2;

        var drawCircle = function (color, lineWidth, percent) {
            percent = Math.min(Math.max(0, percent || 1), 1);
            ctx.beginPath();
            ctx.arc(0, 0, radius, 0, -Math.PI * 2 * percent, true);
            ctx.strokeStyle = color;
            ctx.lineCap = 'round'; // butt, round or square
            ctx.lineWidth = lineWidth
            ctx.stroke();
        };

        drawCircle('#f5f5f6', options.lineWidth, 100 / 100);
        
        if (options.percent == 100) {
            var graphColor = '#49ce49';
        } else {
            var graphColor = '#db1e1e';
        }
        
        drawCircle(graphColor, options.lineWidth, options.percent / 100);
    };

    var setFinalScore = function (score) {
        // update the progress bar
        $('.js-quiz-progressbar').css('width', '100%');
        $('.js-quiz-percentage').text(100);

        $('.js-quiz-percentcorrect').text(score + ' %');
        $('.js-quiz-percentincorrect').text((100 - score) + ' %');
        
        $('.js-quiz-resultgraph').attr('data-percent', score);
        
        setGraph('failgraph');
        setGraph('successgraph');
    };

    var init = function () {
        $('.js-quiz-label').mouseup(function () {
            setTimeout(clickAnswer, 100);
        });
        $('.js-quiz-nextbutton').click(showNext);
        $('.js-quiz-prevbutton').click(showPrev);
        $('.js-quiz-finbutton').click(showEnd);
        $('.js-quiz-reset').click(resetQuiz);
    };

    return {
        'init': init
    };
})();

$(window).load(function () {
    if ($('.js-quiz') && $('.js-quiz').length) {
        atQuiz.init();
    }
});