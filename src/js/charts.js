'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {


    // Pie chart - case study page
    var ctx = $("#pieChart");

    if (ctx.length) {
        var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: data,
            options: {
                    hover: {
                        mode: 'label'
                    },
                    legend: {
                        display: true,
                        position: 'right',
                        labels: {
                            fontColor: '#2d2e2f',
                            fontSize: 14,
                            usePointStyle: true,
                            padding: 20,
                            boxWidth: 20
                        }
                    }
                }
        });
    }

    // Bar chart - case study page
    var ctx1 = $("#barChart");

    if (ctx1.length) {

        var barChart = new Chart(ctx1, {
            type: 'bar',
            data: data1,
            options: {
                legend: {
                    position: 'bottom',
                    labels: {
                        usePointStyle: true,
                        padding: 20,
                        boxWidth: 20
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines : {
                            display : false
                        }
                    }]
                }
            }
        });
    }

    //multiline chart - case study page
     var ctx3 = $("#multilineChart");

     if (ctx3.length) {
        var multilineChart = new Chart(ctx3, config);
     }


}); // End of document ready function


// Data for pie chart
var data = {
    labels: [
        "Patients delivered",
        "Post",
        "Fax",
        "Electronic/Email/EDT"
    ],
    datasets: [
        {
            data: [6, 120, 10, 48],
            backgroundColor: [
                "#c2eeff",
                "#4e436b",
                "#907fbf",
                "#5fccf5"
            ],
            hoverBackgroundColor: [
                "#c2eeff",
                "#4e436b",
                "#907fbf",
                "#5fccf5"
            ]
        }]
};

// Data for bar chart
var data1 = {
    labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    datasets: [
        {
            label: "In minutes",
            backgroundColor: '#5fccf5',
            borderWidth: 1,
            data: [5, 10, 5, 10, 20, 5, 60, 30, 10, 0],
        }
    ]
};

// Data and config for multi line chart
 var config = {
    type: 'line',
    data: {
        labels: ["Week 1", "Week 2", "Week 3", "Week 4"],
        datasets: [{
            label: "Docs sent to GPs",
            data: [300, 200, 100, 200],
            backgroundColor: "rgba(205,238,251, .9)",
            borderColor: "rgb(95,204,245)",
            pointBorderColor: "rgb(95,204,245)",
            pointRadius: 4,
            pointBorderWidth: 3,
            pointHoverRadius: 5,
            fill: true

        }, {
            label: "Total docs sent to all clinicians",
            data: [450, 350, 300, 300],
            backgroundColor: "rgba(220,211,244, .9)",
            borderColor: "rgb(144,127,191)",
            pointRadius: 4,
            pointBorderWidth: 3,
            pointHoverRadius: 5,
        }]
    },
    options: {
        responsive: true,
        legend: {
            position: 'bottom',
            reverse: true,
            labels: {
                usePointStyle: true,
                padding: 20,
                boxWidth: 20
            }
        },
        tooltips: {
            mode: 'label',
            callbacks: {

            }
        },
        hover: {
            mode: 'dataset'
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    show: true,
                    labelString: 'Week'
                },
                gridLines : {
                   lineWidth: 1
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    show: true,
                    labelString: 'Value'
                },
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 500,
                },
                gridLines : {
                    display : false,
                    drawTicks: true
                }
            }]
        }
    }
};