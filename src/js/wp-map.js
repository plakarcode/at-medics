/**
 * Google map for WordPress version
 * with user input on all data
 */
function initWPMap() {

	var myLatlng = new google.maps.LatLng(HOUSE.lat, HOUSE.long);
	var mapOptions = {
		zoom: 14,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
        animation: google.maps.Animation.DROP,
        scrollwheel: false
	}

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	//Callout Content
	var contentString = HOUSE.office + ',<br />' + HOUSE.street + '<br />' + HOUSE.city;

	//Set window width + content
	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 500
	});

	//Add Marker
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: HOUSE.images + 'map/marker.png'
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
		map.setZoom(16);
	});

	markers.push(marker);

	//Resize Function
	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	});
}

var markers = [];

function myClick(id){
    google.maps.event.trigger(markers[id], 'click');
}

//ON DOCUMENT READY
$(document).ready(function() {

	//contact page map
	var map = $("#map");

	if (map.length) {
	  initWPMap();
	}
});
