'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

	//SVG for Everybody (ie9+, ...)
	svg4everybody();

	$(document).on('click', function(event) {
			$('.input-wrap').removeClass('is-active');
	  	if ($(event.target).closest('.input-wrap').length) {
	   		$(event.target).closest('.input-wrap').addClass('is-active');
	  	}
	});

	if ($('select').length) {
	    //select box
	    $("select").selectOrDie({
	        onChange: function () {

	        }
	    });
	}

	var slider_single_image = $('.slider-single-image');
	if (slider_single_image.length) {
	    //select box
	    slider_single_image.slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			autoplay: true,
			arrows: false,
			autoplaySpeed: 3000,
			adaptiveHeight: true
	    });
	}

	var hero_slider = $('.hero-slider');
	if (hero_slider.length) {
	    //select box
	    hero_slider.slick({
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			autoplay: false,
			arrows: true,
			adaptiveHeight: false
	    });
	}

	//thumbs slider
	$('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for',
	  dots: false,
	  centerMode: false,
	  focusOnSelect: true,
	  // the magic
	    responsive: [{

			breakpoint: 1200,
			settings: {
				slidesToShow: 5,
			}
		},
		{
			breakpoint: 1000,
			settings: {
				slidesToShow: 4,
			}
		},
		{
			breakpoint: 800,
			settings: {
				slidesToShow: 3,
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
			}
		},
		{
			breakpoint: 400,
			settings: {
				slidesToShow: 1,
			}
		}]
	});

	//TABS
    $('.tabs .tabs__item').hide();
    $('.tabs').each(function(){
      var $this = $(this);
      $this.find('.tabs__item:first').show().addClass('active-tab');
      $this.find('.tabs__item ul li:first a').addClass('is-active');
    });
    $('.tabs__nav li a').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var tab_item = $this.parents(".tabs").find('.tabs__item');
        tab_item.removeClass('active-tab');
        $this.addClass('is-active').parent().siblings().find('a').removeClass('is-active');
        var currentTab = $this.attr('href');
        tab_item.hide();
        $(currentTab).fadeIn().addClass('active-tab');
        return false;
    });

    //accordion
	var accordion__item = $('.accordion__item');
	var accordion_title = accordion__item.find('.accordion__item-title');
	var accordion__item_first = accordion__item.first();

	accordion__item_first.find('.accordion__item-content').slideDown();

	accordion_title.on('click', function (e) {
		e.preventDefault();
		$(this).siblings('.accordion__item-content').stop().slideToggle().parent('.accordion__item').stop().toggleClass('is-active').siblings().removeClass('is-active').find('.accordion__item-content').slideUp();
		});

	//back to top
	var back_to_top = $('.back-to-top');

	back_to_top.on('click', function (e) {
	    e.preventDefault();
	    $("html, body").animate({scrollTop: 0}, "fast");
	    return false;
	});

	var header = $('.header-main');
	var header_height = header.outerHeight();
	var nav_primary = $('.nav-primary');
	var nav_trigger = $('.nav-trigger');

	nav_trigger.on('click', function(){
		nav_primary.stop().slideToggle();
		header.toggleClass('active-nav');
	});

	$('.has-dropdown > a, .icon-arrow').on('click', function() {
		$(this).siblings('ul').stop().slideToggle();
	});

	//nav secondary and nav steps trigger for mobile menu
	$('.nav-secondary__active, .nav-steps__active').on('click', function() {
		$(this).siblings('ul').stop().slideToggle();
		$(this).toggleClass('opened');
	});

	//nav secondary mobile menu - add active item on top
	if($('.nav-secondary li').is(".is-active")) {
		var activeElcontent = $('.nav-secondary li.is-active > a').text();
		$('.nav-secondary__active').text(activeElcontent);
	}


	//nav steps mobile menu - add active item on top
	if($('.nav-steps li').is(".is-active")) {
		var activeElcontentSteps = $('.nav-steps li.is-active > a').text();
		$('.nav-steps__active').text(activeElcontentSteps);
	}


	//open members popup
	$('.js-open-popup').magnificPopup({
	      type: 'inline',
	      fixedContentPos: true,
	      fixedBgPos: true,
	      overflowY: 'auto',
	      closeBtnInside: true,
	      preloader: false,
	      midClick: true,
	      removalDelay: 300,
	      mainClass: 'my-mfp-slide-up'
	  });

	//open login, sign in and book workshop popup
	$('.js-login-popup, .js-workshop, .js-training-popup').magnificPopup({
	      type: 'inline',
	      fixedContentPos: true,
	      fixedBgPos: true,
	      overflowY: 'auto',
	      closeBtnInside: true,
	      preloader: false,
	      midClick: true,
	      removalDelay: 300,
	      mainClass: 'my-mfp-fade-move',
	      closeMarkup: '<button title="%title%" type="button" class="mfp-close">Close <span>&#215;</span></button>',
	      tClose: 'Close'
	  });

	//video popup - resources page
	$('.video-popup').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'my-mfp-fade-move',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});


	// WINDOW SCROLL
	$(window).on('scroll', function() {
		if (header_height < $(window).scrollTop()) {
			header.addClass('is-active');
		}
		else {
			header.removeClass('is-active');
		}

	}).trigger('scroll');


	/*
	Add or remove header bottom border on scroll
	for get started and step 1-5 pages
	*/
	if ($('.page-template-template-get-started, .page-template-template-steps').length) {

		$(window).on('scroll', function() {

			var stepMenuHeight = $('.nav-steps').height();

			if (stepMenuHeight > $(window).scrollTop()) {
				header.addClass('no-border');
				$('body').addClass('header-no-border');
			}
			else {
				header.removeClass('no-border');
				$('body').removeClass('header-no-border');
			}

		}).trigger('scroll');
	}

	/*
	Remove header border when slider is in viewport,
	and add border if slider is not in viewport (used for home page)
	*/
	if ($('.home').length) {

		$(window).on('scroll', function() {

			var heroSliderHeight = $('.hero-slider-wrap').height();
			if (heroSliderHeight > $(window).scrollTop()) {
					header.addClass('no-border');
					$('body').addClass('header-no-border');
			} else {
				$('.header-main').removeClass('no-border');
				$('body').removeClass('header-no-border');
			}

		}).trigger('scroll');
	}


	$('.slider-tasks').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 2,
	  variableWidth: true
	});


	// If checkbox :checked toogle class
    $(".checkbox_wrap--square .styled-checkbox").on("click", function(){
        var check = $(this).is(":checked");
        if(check) {
           $(this).parent().addClass('is-active')
        } else {
           $(this).parent().removeClass('is-active')
        }
    });

    // Select a date range
    $( ".datepicker--one" ).datepicker({
        minDate: 0,
        maxDate: "+2Y",
        showWeek: false,
        weekHeader: 'Wk',
        onSelect: function( selectedDate ) {
            $( ".datepicker--two" ).datepicker("option", "minDate", selectedDate );
            setTimeout(function(){
                $( ".datepicker--two" ).datepicker('show').attr('disabled', false);
            }, 16);
        }
    });

    $( ".datepicker--two" ).datepicker({
        maxDate: "+2Y",
        showWeek: false,
        weekHeader: 'Wk'
    });

    //datepicker default
     $( ".datepicker" ).datepicker({
        maxDate: "+2Y",
        showWeek: false,
        weekHeader: 'Wk',
        minDate: 0
    });

    /*
	Add width to calendar widget to be the
	same as daptepicker form field width
	*/
	if (('.datepicker').length) {
  		var calendarWidth = $('.datepicker-wrap').width();
  		$('.ui-datepicker').css('min-width', calendarWidth);
  	}

 	//check if datepicker is opened
	$('.datepicker').focus(function () {
	    $(this).parent().addClass('is-opened');
	}).blur(function () {
	    $(this).parent().removeClass('is-opened');
	});

	//Weekly audit table
	$('.audit-content').hide();
	  $('#option1').show();
	  $('#selectOne').change(function () {
	    $('.audit-content').hide();
	    $('#'+$(this).val()).show();
	  });


	  /*
	   Add fax log reports on button click
	   (adding new empty accordion item with default data)
	   */
	 $('.js-add-fax-log').on('click', function() {
		var default_acc_item = $('.accordion--fax .accordion__item.hidden').clone(true);
	  	default_acc_item.appendTo('.accordion--fax').removeClass('hidden');
	 });

	  //if step active add class is-active to all prev sibilings
	  if ($('.nav-steps li').hasClass('is-active')) {
	  	$('.nav-steps').addClass('step-active');
	  	$('.nav-steps li.is-active').prevAll().delay('slow').fadeIn().addClass('is-active');
	  }

	  if ($('.nav-steps li').hasClass('current-menu-item')) {
	  	$('.nav-steps').addClass('step-active');
	  	$('.nav-steps li.current-menu-item').prevAll().delay('slow').fadeIn().addClass('current-menu-item');
	  }

	//scroll to section
	$('.contact-form a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top - 120
	    }, 800, 'swing', function () {
	        window.location.hash = target;
	    });
	});


    //STEPS NAV
    $( '#menu-steps' ).wrap( '<div class="container"></div>' );

    //ARROW DOWN
    $( '.icon-arrow2-down' ).parent().addClass( 'icon-arrow2-down-parent' );


    /*
   	Fix sidebar at some point and remove
   	fixed position at content bottom
   	*/
	$(window).scroll(function () {

		var fixSidebar = 80;
	  	var check = $('.main-content').height() - $('.sidebar__list').height();

		//execute script if screen width larger than 1135px
		if ( $(window).width() > 1134) {

	      	if ($(window).scrollTop() >= fixSidebar) {
	          	$('.sidebar__list').addClass('fixed');
	      	} else {
	          	$('.sidebar__list').removeClass('fixed');
	      	}

	      	if ($(window).scrollTop() >= check) {
	          	$('.sidebar__list').addClass('bottom');
	      	} else {
	          	$('.sidebar__list').removeClass('bottom');
	      	}
	    }
    });


	 //Check if .info-popup-wrap element exists
	 if(('.info-popup-wrap').length) {

	 	//Detect if checkbox with attribute "disabled" clicked
	 	$('.checkbox_wrap').on('click', function () {
	 		if ($(this).find('.styled-checkbox').attr('disabled')) {
	 			//Open popup
		   		$('.info-popup-wrap').fadeIn('fast');
		   		//Add class to body (add white bg)
		   		$('body').addClass('info-popup-active');
		  	}
	 	});

	 	//On button click remove white bg and close popup
	 	$('.btn--info').on('click', function() {
		 	$('.info-popup-wrap').fadeOut('fast');
		 	$('body').removeClass('info-popup-active');
		});
	 }

	 //If checklist item active allow content
	 $(".styled-checkbox").on("click", function(){
   		var check = $(this).is(":disabled");
        if(check) {
           $('.step-part').addClass('disabled')
        } else {
           $(this).parent().removeClass('disabled')
        }
	});

	//Step one - If checklist item disabled disable content for item
	$('.styled-checkbox').each(function () {
		var checkID = $(this).attr('id');

		if ( $('#' + checkID).is(":disabled") ) {
		 	$('#step-' + checkID).addClass('disabled');
		 	$('#step-' + checkID + ' .btn--primary').attr('disabled', true);
		 	$('.step-box .btn--primary').attr('disabled', true);
		 	$('.step-box').addClass('disabled');
		} else {
			$('#step-' + checkID).removeClass('disabled');
			$('#step-' + checkID + '.btn--primary').attr('disabled', false);
		}
	});

   	/*
   	Click on next button hides current and showing
   	next question if condition is true
   	*/
   	$("form .question-item").first().addClass('is-active');
   	$('.question-nav .btn--prev').attr('disabled', true)

    $(".btn--next").click(function(){
        if ($("form .question-item:visible").next().length != 0) {
            $("form .question-item:visible").next().addClass('is-active').prev().removeClass('is-active');
            $('.question-nav .btn--prev').attr('disabled', false);

	        if ( $("form .question-item").last().hasClass('is-active') ) {
	        	$('.question-nav .btn--next').hide();
	        	$('.question-nav .btn--finish').show();
	        }
        }
        else {
        	$('.question-nav .btn--next').text('Finish');
        }
        return false;
    });

    /*
   	Click on prev button hides current and showing previous question if condition is true
   	*/
    $(".btn--prev").click(function(){
        if ($("form .question-item:visible").prev().length != 0) {
        	$("form .question-item:visible").prev().addClass('is-active').next().removeClass('is-active');
        	$('.question-nav .btn--next').text('Next question');

        	if ( $("form .question-item").first().hasClass('is-active') ) {
	        	$('.question-nav .btn--prev').attr('disabled', true);
	        }
	        if ( !$("form .question-item").last().hasClass('is-active') ) {
	        	$('.question-nav .btn--finish').hide();
	        	$('.question-nav .btn--next').show();
	        }
        }
        return false;
    });

    //On click hide video thumb and play video (used on step 3 page)
	function videoToggle() {

	    $('.js-video-play').on("click", function (e) {
	    	e.preventDefault();
	    	// get the wrapper
	    	var wrapper = $(this).parent('.video-figure');
	    	// get the video url and id
	    	var url = $(wrapper).attr('data-url');
	    	var id = $(wrapper).attr('data-id');

	    	if ( url.indexOf( 'youtu' ) !==-1 ) {
		    	// set the src
		    	var src = 'https://www.youtube.com/embed/' + id + '?enablejsapi=1&autoplay=1';
		    	// whole player
		    	var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
	    	} else if ( url.indexOf( 'vimeo' ) !==-1 ) {
		    	// set the src
		    	var src = 'https://player.vimeo.com/video/' + id + '?autoplay=1';
		    	// whole player
		    	var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
	    	}

	    	// hide any other player
	    	$('#video-wrap, #video-player').remove();
	    	// show any other triggers
	    	$('.video-thumb').show();

	    	// hide this trigger
	    	$(this).hide();
	    	// play the video
	    	$(wrapper).prepend(player);

        	$('#animate-text').animate({
        		marginTop: 40,
        	}, 200 );

        	$(this).parents('.intro__video').siblings('.intro__text').find('.container').animate({
        		marginTop: 80,
        	}, 200 );
	    });
	}

	//if element exists call function
	if ( $('.video-figure').length) {
		videoToggle();
	}

    //Add click trigger to sidebar check list on mobile breakpoint
	var windowSize;
	$(window).on('resize', function() {
	    windowSize = $(this).width() < 1134;
	});

	//Click on element only if window size condition is true
	$('.sidebar__list h4').on('click', function(){
		if (windowSize) {
			$(this).toggleClass('opened').siblings('form').stop().slideToggle();
		}
	});


	//If ul contain li with classes .is-completed or .not-completed, remove margin left
	$('ul:has( > li.is-completed), ul:has( > li.not-completed)').css('margin-left', 0);



}); // End of document ready function


//WINDOW ONLOAD
$(window).load(function() {

	//Preloader
	$('.preloader').fadeOut(800);



  	// WINDOW RESIZE
  	$(window).on('resize', function() {

  		if($('.member-profile--user').length) {
	  		var screen_width = $(window).width();
		    var container_width = $('.container').width();
		    var result = (screen_width - container_width) / 2;
		    $('.member-profile__first').css('padding-left', result);
  		}


  	}).trigger('resize');

});




/*
Function for detecting Animation Elements in View
*/
var $animation_elements = $('.animation-element');
var $window = $(window);
var $anim = true;

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop() + 115;
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      	$element.addClass('in-view');

  	  if($anim) {
  	  	// counter animation for home page
		$('.count').each(function () {
		    $(this).prop('Counter',0).animate({
		        Counter: $(this).text()
		    }, {
		        duration: 4000,
		        easing: 'swing',
		        step: function (now) {
		            $(this).text(Math.ceil(now));
		        }
		    });
		});

        $anim = false;
  	  }
    }
    else {
      $element.removeClass('in-view');
    }
  });
}

//call function
$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');
