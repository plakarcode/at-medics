'use strict';
/**
 * wp.js
 *
 * Some things needs extra tweaks in WordPress
 */
var $ = window.jQuery;
/**
 * Admin bar gets very annoying overlapping with
 * fixed header
 */
function adminBarHeight() {
	var $bar = $('#wpadminbar').outerHeight();

	$('#mainheader').css('margin-top', $bar);
}
/**
 * Main Navigation Tweaks
 *
 * We need extra classes and markup for main navigation.
 */
function mainNavigation() {
	var $menuUl    = $('#main-menu');
	var $parent    = $menuUl.find('.menu-item-has-children');
	var $link      = $('#main-menu .menu-item-has-children > a');
	var $iconArrow = '<span class="icon-arrow"><img src="' + HOUSE.images + 'icon-select-box-arrow-down-dark.svg"></span>';

	// parent's link text is wrapped in <span>
	$.each($link, function() {
		var $text = $(this).html();
		var $new = '<span>' + $text + '</span> ';

		$(this).addClass('parent-link').html($new);
	});

	// there's an arrow icon appended to parent's link
	$.each($parent, function() {
		$(this).addClass('has-dropdown').find('.parent-link').after($iconArrow);
	});
}
/**
 * Subnavigation Tweaks
 * Extra class for subnavigation on about/information pages
 */
function subNavigation() {
	$('#submenu').find('.current-menu-item').addClass('is-active');
}

function pagination() {
	$('#pagination').find('.current').parents('li').addClass('is-active');
}
/**
 * FAQs
 * Make first accordion item active
 */
function faqs() {
	$('.accordion .accordion__item:first-of-type').addClass('is-active');
}
/**
 * Add active class in tabs nav
 */
function tabsNav() {
	$('.tabs__nav li:first-of-type a').addClass('is-active');
}

//ON DOCUMENT READY
$(document).ready(function() {

	if ($('#wpadminbar').length) {
		adminBarHeight();
	}

	// main nav tweaks
	mainNavigation();

	// submenu tweaks
	// if ($('#submenu').length) {
	// 	subNavigation();
	// }
	// submenu tweaks
	if ($('#pagination').length) {
		pagination();
	}

	// FAQs tweaks
	if ($('.accordion').length) {
		faqs();
	}
	// FAQs tweaks
	if ($('.tabs').length) {
		tabsNav();
	}

	// WINDOW SCROLL
	$(window).on('scroll', function() {

	}).trigger('scroll');


});


//WINDOW ONLOAD
$(window).load(function() {

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');

});

