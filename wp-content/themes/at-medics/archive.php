<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */
get_header(); ?>

	<header class="archive-header date-header">
		<?php get_template_part( 'partials/content/title-archive' ); ?>
	</header><!-- .archive-header -->

	<?php if ( have_posts() ) : ?>

		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post();

			/**
			 * If we have template part for post format and
			 * we are on post format single.
			 *
			 * WordPress will first look for 'format-FORMAT_NAME.php',
			 * if none found fallback is 'format.php'. If that one is missing as well,
			 * it'll look for 'content.php'
			 */
			if ( has_post_format( get_post_format() ) ) {
				get_template_part( 'format', get_post_format() );
			}
			/**
			 * If we have template part for custom post type and
			 * we are on custom post type single.
			 *
			 * WordPress will first look for 'content-CPT_NAME.php',
			 * if none found fallback is 'content.php'.
			 */
			elseif ( post_type_exists( get_post_type() ) ) {
				get_template_part( 'content', get_post_type() );
			}
			/**
			 * If, in any case, none from above applies
			 */
			else {
				get_template_part( 'content' );
			}

		endwhile; ?>

		<?php
			/**
			 * Get pagination
			 */
			if ( function_exists( 'house_content_pagination' ) ) {
				house_content_pagination( 'pagination' );
			}
		?>

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>

<?php get_footer(); ?>