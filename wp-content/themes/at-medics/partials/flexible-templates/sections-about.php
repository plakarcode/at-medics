<?php
/**
 * Flexible sections for About pages
 *
 * Template part for rendering ACF flexible sections on About pages
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'about_sections';
/**
 * Define paths to template parts
 * @var string
 */
$path     = 'partials/flexible/section';
$specific = 'partials/flexible/about/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// About specific ACF
	// intro_image
	'intro_image' => [
		'dir'      => $specific,
		'template' => 'intro-image',
	],

	// intro_video
	'intro_video' => [
		'dir'      => $specific,
		'template' => 'intro-video',
	],

	// info_panel_number
	'info_panel_number' => [
		'dir'      => $specific,
		'template' => 'info-number',
	],

	// info_panel_image
	'info_panel_image' => [
		'dir'      => $specific,
		'template' => 'info-image',
	],

	// simple_image_slider
	'simple_image_slider' => [
		'dir'      => $specific,
		'template' => 'image-slider',
	],

	// intro_text
	'intro_text' => [
		'dir'      => $specific,
		'template' => 'intro-text',
	],

	// team_members
	'team_members' => [
		'dir'      => $specific,
		'template' => 'team-members',
	],

	// list - ul and ol
	'list' => [
		'dir'      => $path,
		'template' => 'list',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )