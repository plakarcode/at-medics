<?php
/**
 * Flexible sections for legal pages
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'legal_pages_content';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// heading
	'small_heading' => [
		'dir'      => $path,
		'template' => 'heading-h4',
	],

	// list
	'list' => [
		'dir'      => $path,
		'template' => 'list',
	],

	// regular_content
	'regular_content' => [
		'dir'      => $path,
		'template' => 'content',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )