<?php
/**
 * Flexible sections for steps pages
 *
 * Template part for rendering ACF flexible sections on steps pages
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'content_fields';
/**
 * Define paths to template parts
 * @var string
 */
$path     = 'partials/flexible/section';
$specific = 'partials/flexible/steps/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// General ACF
	// heading
	'heading' => [
		'dir'      => $path,
		'template' => 'heading-h2',
	],

	// lead_paragraph
	'lead_paragraph' => [
		'dir'      => $path,
		'template' => 'lead',
	],

	// list
	'list' => [
		'dir'      => $path,
		'template' => 'list',
	],

	// blockquote
	'blockquote' => [
		'dir'      => $path,
		'template' => 'blockquote',
	],

	// content
	'content' => [
		'dir'      => $path,
		'template' => 'content',
	],

	// Steps specific ACF
	// step_circles
	'step_circles' => [
		'dir'      => $specific,
		'template' => 'step-circles',
	],

	// accordion
	'accordion' => [
		'dir'      => $specific,
		'template' => 'accordion',
	],

	// box
	'box' => [
		'dir'      => $specific,
		'template' => 'box',
	],

	// video
	'videos' => [
		'dir'      => $specific,
		'template' => 'videos',
	],

	// form
	'form' => [
		'dir'      => $specific,
		'template' => 'form',
	],

	// file sor downloading
	'files_for_downloading' => [
		'dir'      => $specific,
		'template' => 'file-download',
	],

	// quiz
	'quiz' => [
		'dir'      => $specific,
		'template' => 'quiz',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )
