<?php
/**
 * Flexible sections - Steps Accordion
 *
 * Template part for rendering ACF accordion flexible sections on steps pages
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'accordion';
/**
 * Define paths to template parts
 * @var string
 */
$path     = 'partials/flexible/section';
$specific = 'partials/flexible/steps/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// General ACF
	// heading
	'heading' => [
		'dir'      => $path,
		'template' => 'heading-h3',
	],

	// content
	'content' => [
		'dir'      => $path,
		'template' => 'paragraph',
	],

	// Steps specific ACF
	// button
	'button' => [
		'dir'      => $specific,
		'template' => 'button',
	],

	// table
	'table' => [
		'dir'      => $specific,
		'template' => 'table',
	],

	// Files for downloading
	'download_file' => [
		'dir'      => $specific,
		'template' => 'file-download',
	],

	// Accordion content
	'accordion_content' => [
		'dir'      => $specific,
		'template' => 'accordion-content',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )