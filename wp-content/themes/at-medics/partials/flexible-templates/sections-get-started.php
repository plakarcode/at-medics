<?php
/**
 * Flexible sections for Get started page
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'get_started_content';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
$specific = 'partials/flexible/get-started/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// heading
	'main_heading' => [
		'dir'      => $path,
		'template' => 'heading-h1',
	],

	// intro
	'intro' => [
		'dir'      => $path,
		'template' => 'blockquote',
	],

	// Get started specific ACF
	// illustrated_steps
	'illustrated_steps' => [
		'dir'      => $specific,
		'template' => 'graphic',
	],

	// steps
	'steps' => [
		'dir'      => $specific,
		'template' => 'steps',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )