<?php
/**
 * Flexible sections for home page
 *
 * Template part for rendering ACF flexible sections on home page
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'home_sections';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/home/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// who we are
	'who_we_are' => [
		'dir'      => $path,
		'template' => 'who-we-are',
	],

	// process points
	'process_points' => [
		'dir'      => $path,
		'template' => 'process-points',
	],

	// statistics grid
	'statistics_grid' => [
		'dir'      => $path,
		'template' => 'statistics-grid',
	],

	// members
	'members' => [
		'dir'      => $path,
		'template' => 'members',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )