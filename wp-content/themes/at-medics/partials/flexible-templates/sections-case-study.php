<?php
/**
 * Flexible sections for Case Study page
 *
 * Template part for rendering ACF flexible sections on Case Study page
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'case_study_sections';
/**
 * Define paths to template parts
 * @var string
 */
$path     = 'partials/flexible/section';
$specific = 'partials/flexible/case-study/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// General ACF
	// heading
	'section_title' => [
		'dir'      => $path,
		'template' => 'heading-h2',
	],

	// heading
	'section_subheading' => [
		'dir'      => $path,
		'template' => 'heading-h3',
	],

	// heading
	'implementation_heading' => [
		'dir'      => $path,
		'template' => 'heading-h1',
	],

	// regular_content
	'regular_content' => [
		'dir'      => $path,
		'template' => 'paragraph',
	],

	// list
	'list' => [
		'dir'      => $path,
		'template' => 'list',
	],

	// Case Study specific ACF
	// atmedics_example
	'atmedics_example' => [
		'dir'      => $specific,
		'template' => 'example',
	],

	// two_columns_table
	'two_columns_table' => [
		'dir'      => $specific,
		'template' => 'table',
	],

	// key_findings
	'key_findings' => [
		'dir'      => $specific,
		'template' => 'findings',
	],

	// implementation_stages
	'implementation_stages' => [
		'dir'      => $specific,
		'template' => 'stages',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )