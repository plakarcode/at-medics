<?php
/**
 * List
 *
 * Template part for rendering ACF flexible sections - list
 *
 * Used in flexible-templates/
 *         - sections.php
 *         - sections-case-study.php
 *         - sections-legal.php
 *         - sections-steps.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Opening list tag - 'ul' or 'ol'
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'list_type', true, '<', '>' );

	if ( have_rows( 'list_items' ) ) : while ( have_rows( 'list_items' ) ) : the_row();

		/**
		 * Translators: field id, echo (false returns value), before, after
		 */
		acf_sub_field( 'item', true, '<li>', '</li>' );

	endwhile; endif; // have_rows( 'list_items' )

/**
 * Closing list tag - 'ul' or 'ol'
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'list_type', true, '</', '>' );
