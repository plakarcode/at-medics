<?php
/**
 * Table
 *
 * Template part for rendering ACF flexible sections - table
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

<table class="table table--third">

	<?php if ( have_rows( 'columns_titles' ) ) : ?>
		<thead>
			<?php while ( have_rows( 'columns_titles' ) ) : the_row(); ?>

			<tr>
				<?php 
					acf_sub_field( 'item_title', true, '<th>', '</th>' );
					acf_sub_field( 'value_title', true, '<th>', '</th>' );
				?>
			</tr>

			<?php endwhile; // have_rows( 'columns_titles' ) ?>
		</thead>
	<?php endif; // have_rows( 'columns_titles' )


	if ( have_rows( 'row' ) ) : ?>
	<tbody>

		<?php while ( have_rows( 'row' ) ) : the_row(); ?>
		<tr>
			<?php 
				acf_sub_field( 'item', true, '<td data-th="Item">', '</td>' ); 
				acf_sub_field( 'value', true, '<td data-th="Value">', '</td>' ); 
			?>
		</tr>
		<?php endwhile; // have_rows( 'row' ) ?>

	</tbody>
	<?php endif; // have_rows( 'row' ) ?>

</table><!-- table table--third -->

<?php
	$conclusion = acf_sub_field( 'total_or_summary', false );
	/**
	 * Check if we want $conclusion
	 */
	if ( ! empty( $conclusion ) ) :

		if ( $conclusion === 'total' ) :

			if ( have_rows( 'total' ) ) : while ( have_rows( 'total' ) ) : the_row(); ?>

			<table class="table table--third">
				<?php if ( acf_sub_field( 'label', false ) ) : ?>
				<thead>
					<tr>
						<?php acf_sub_field( 'label', true, '<th>', '</th>' ); ?>
					</tr>
				</thead>
				<?php endif; // get_sub_field( 'label' )

				if ( get_sub_field( 'value' ) ) : ?>
				<tbody>
					<tr>
						<?php acf_sub_field( 'value', true, '<td data-th="Item">', '</td>' ); ?>
					</tr>
				</tbody>
				<?php endif; // get_sub_field( 'value' ) ?>
			</table>

			<?php endwhile; endif; // have_rows( 'total' )

		elseif ( $conclusion === 'summary' ) :

			if ( acf_sub_field( 'summary', false ) ) : ?>
				
				<?php acf_sub_field( 'summary', true, '<blockquote>', '</blockquote>' ); ?>

			<?php endif; // get_sub_field( 'summary' )

		endif; // $conclusion === 'total'

	endif; // ! empty( $conclusion )