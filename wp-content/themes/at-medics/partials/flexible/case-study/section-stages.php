<?php
/**
 * Implementation Stages
 *
 * Template part for rendering ACF flexible sections - stages
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Set the stages counter
 * @var integer
 */
$i = 1;

if ( have_rows( 'stages' ) ) : while ( have_rows( 'stages' ) ) : the_row();

	/**
	 * Get the stage title and append 'STAGE {No.}' to it
	 */
	$title = acf_sub_field( 'stage_title', false );
	if ( acf_sub_field( 'stage_title', false ) ) : ?>
		<h3><strong><?php printf( __( 'Stage %s.', 'house' ), $i ); ?></strong> <?php echo $title; ?></h3>
	<?php endif; // get_sub_field( 'stage_title' )

	/**
	 * Get list items as stage points
	 */
	if ( have_rows( 'stage_points' ) ) : ?>
		<ul>
			<?php while ( have_rows( 'stage_points' ) ) : the_row();
				acf_sub_field( 'point', true, '<li>', '</li>' );
			endwhile; // have_rows( 'stage_points' ) ?>
		</ul>
	<?php endif; // have_rows( 'stage_points' )

	/**
	 * Get key benefits label if provided
	 */
	if ( acf_sub_field( 'key_benefits_label', false ) ) {
		$label = acf_sub_field( 'key_benefits_label', false );
	} else {
		$label = __( 'Key Benefits:', 'house' );
	} ?>

	<h3><?php echo $label; ?></h3><?php

	/**
	 * Get key benefits
	 */
	if ( have_rows( 'key_benefits' ) ) : ?>
		<ol>
			<?php while ( have_rows( 'key_benefits' ) ) : the_row();
				/**
				 * Get list items
				 */
				if ( acf_sub_field( 'benefit', false ) ) : ?>
					<li><?php the_sub_field( 'benefit' );

						/**
						 * Get sublist, if any
						 */
						if ( have_rows( 'benefit_points' ) ) : ?>
							<ul>
								<?php while ( have_rows( 'benefit_points' ) ) : the_row();
									acf_sub_field( 'point', true, '<li>', '</li>' );
								endwhile; // have_rows( 'benefit_points' ) ?>
							</ul>
						<?php endif; // have_rows( 'benefit_points' ) ?>

					</li>
				<?php endif; // get_sub_field( 'benefit' )

			endwhile; // have_rows( 'key_benefits' ) ?>
		</ol>
	<?php endif; // have_rows( 'key_benefits' )

$i++;
endwhile; endif; // have_rows( 'stages' )



