<?php
/**
 * Example
 *
 * Template part for rendering ACF flexible sections - AT Medics Example
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( acf_sub_field( 'label', false ) ) {
	$label = acf_sub_field( 'label', false );
} else {
	$label = __( 'AT Medics example', 'house' );
}
// don't do anything if we have no example
if ( get_sub_field( 'example' ) ) : ?>
	<blockquote class="quote-example">
		<h2><?php echo $label; ?></h2>
		<?php acf_sub_field( 'example', true, '<p>', '</p>' ); ?>
	</blockquote>
<?php endif; // get_sub_field( 'example' )