<?php
/**
 * Key Findings
 *
 * Template part for rendering ACF flexible sections - key findings
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'findings' ) ) : ?>
<dl>
	<?php while ( have_rows( 'findings' ) ) : the_row();

		acf_sub_field( 'key', true, '<dt>', '</dt>' );

		if ( have_rows( 'points' ) ) : while ( have_rows( 'points' ) ) : the_row();
			acf_sub_field( 'point', true, '<dd>', '</dd>' );
		endwhile; endif; // have_rows( 'points' )

	endwhile; // have_rows( 'findings' ) ?>
</dl>
<?php endif; // have_rows( 'findings' )