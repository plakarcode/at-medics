<?php
/**
 * Intro Text
 *
 * Template part for rendering ACF flexible sections - intro text
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="container">
	<?php 
		acf_sub_field( 'title', true, '<div class="heading-blue"><h1>', '</h1></div>' );
		acf_sub_field( 'content', true, '<p class="text-highlighted">', '</p>' ); 
	?>
</div>