<?php
/**
 * Intro Image
 *
 * Template part for rendering ACF flexible sections - intro image
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="container">
	<div class="intro">

	<?php
		/**
		 * Get the image
		 */
		if ( get_sub_field( 'image' ) ) :
			$image = get_sub_field( 'image' );
			$src = $image['sizes']['intro']; ?>

			<div class="intro__img" style="background-image:url(<?php echo $src; ?>);">
				<img src="<?php echo $src; ?>" alt="" />
			</div><!-- /.intro__img -->

		<?php endif; // get_sub_field( 'image' )

		/**
		 * Get the texts
		 */
		if ( have_rows( 'content' ) ) : while ( have_rows( 'content' ) ) : the_row(); ?>

			<div class="intro__text">
				<div class="container container--narrow">

					<?php acf_sub_field( 'title', true, '<div class="heading-blue"><h1>', '</h1></div><!-- /.heading-blue -->' ); ?>
					
					<blockquote class="quote-intro">
	                    <?php 
		                    acf_sub_field( 'quote', true, '<p>', '</p>' );
		                    acf_sub_field( 'cite', true, '<cite>', '</cite>' );
		                ?>
                  	</blockquote>

					<?php 
						acf_sub_field( 'lead_paragraph', true, '<p class="text-highlighted">', '</p>' );
						acf_sub_field( 'regular_text', true );
					?>

				</div><!-- /.container container--narrow -->
			</div><!-- /.intro__text -->

		<?php endwhile; endif; // have_rows( 'content' ) ?>

	</div><!-- /.intro -->
</div><!-- container -->

