<?php
/**
 * Info panel - image
 *
 * Template part for rendering ACF flexible sections - info panel image
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'panel' ) ) :
/**
 * Initiate counter
 * @var integer
 */
$i = 0;

while ( have_rows( 'panel' ) ) : the_row();
	/**
	 * Count panels and add class to every even
	 */
	if ( $i%2 == 0 ) {
		$class = '';
	} else {
		$class = 'layout--rev';
	}
	/**
	 * Get background color
	 * @var string
	 */
	$color = acf_sub_field( 'background_color', false );

	if ( $color === 'pink' ) {
		$class_color = 'class="bgr-second"';
	} elseif ( $color === 'blue' ) {
		$class_color = 'class="bgr-third"';
	} elseif ( $color === 'white' ) {
		$class_color = '';
	} 

	$image = acf_sub_field( 'image', false );
	$path = strstr( $image, 'about' ) ? '/images/about-us/' : '/images/how-it-works/';

	$src = get_template_directory_uri() . $path . $image . '.png';
	$retina = get_template_directory_uri() . $path . $image . '-retina.png 2x'; ?>

	<section <?php echo $class_color; ?>>
		<div class="container container--middle">
			<div class="number-info-panel-wrap">
				<div class="number-info-panel">

					<div class="layout layout--middle <?php echo $class; ?>">
						<div class="layout__item large-and-up-1/2 text-center">
							<picture>
								<source srcset="<?php echo $src . ', ' . $retina; ?>">
								<img srcset="<?php echo $src . ', ' . $retina; ?>" alt="">
							</picture>
						</div>

						<div class="layout__item large-and-up-1/2">

							<?php 
								acf_sub_field( 'title', true, '<h2>', '</h2>' );
								acf_sub_field( 'description', true ); 
							?>
		
							<div class="entry-content">
								<?php get_template_part( 'partials/flexible/section', 'list' ); ?>
							</div><!-- /.entry-content -->

						</div><!-- layout__item large-and-up-1/2 -->
					</div><!-- layout layout--middle <?php echo $class; ?> -->

				</div><!-- number-info-panel -->
			</div><!-- number-info-panel-wrap -->
		</div><!-- container -->

		<?php $i++; ?>
	</section>

<?php endwhile; endif; // have_rows( 'panel' )