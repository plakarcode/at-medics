<?php
/**
 * Image Slider
 *
 * Template part for rendering ACF flexible sections - image slider
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'slides' ) ) : ?>

	<div class="container slider-container">
		<div class="slider slider-single-image">
			<?php while ( have_rows( 'slides' ) ) : the_row();
				$image = get_sub_field( 'image' );
				$src = $image['sizes']['image-slider']; ?>

				<div>
					<img src="<?php echo $src; ?>">
				</div>

			<?php endwhile; // have_rows( 'slides' ) ?>
		</div><!-- slider slider-single-image -->
	</div><!-- container -->

<?php endif; // have_rows( 'slides' )