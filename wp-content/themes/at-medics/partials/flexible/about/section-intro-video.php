<?php
/**
 * Intro Video
 *
 * Template part for rendering ACF flexible sections - intro video
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="container">
	<div class="intro">

		<?php 
			if ( acf_sub_field( 'video_url', false ) ) :
				$url = acf_sub_field( 'video_url', false ); ?>
				<div class="intro__video">
					<div class="video-object video-figure" <?php if ( get_sub_field( 'video_url' ) ) : echo 'data-url="' . get_sub_field( 'video_url' ) . '"'; echo 'data-id="' . set_video_id( get_sub_field( 'video_url' ) ) . '"'; endif; ?>>

					<?php
						/**
						 * Get the video cover
						 */
						if ( acf_sub_field( 'image', false ) ) {
							$image = acf_sub_field( 'image', false );
							$src = $image['sizes']['intro']; 
						} else {
							$src = set_video_image_src( $url );
						} ?>
						
						<a href="javascript:;" class="js-video-play video-thumb" style="background-image:url(<?php echo $src; ?>);">
							<img src="<?php echo $src; ?>" alt="video" />
							<div class="video-object__play">
								<?php echo house_svg_icon( 'play' ); ?>
							</div>
						</a>


					</div><!-- /.video-object -->
				</div><!-- /.intro__video -->
		<?php endif; // acf_sub_field( 'video_url', false ) ?>

	<?php
		/**
		 * Get the texts
		 */
		if ( have_rows( 'content' ) ) : while ( have_rows( 'content' ) ) : the_row(); ?>

			<div class="intro__text">
				<div class="container container--narrow">

					<?php 
						acf_sub_field( 'title', true, '<div class="heading-blue"><h1>', '</h1></div><!-- /.heading-blue -->' );
						acf_sub_field( 'lead_paragraph', true, '<p class="text-highlighted">', '</p>' );
						acf_sub_field( 'regular_text', true );
					?>

				</div><!-- /.container container--middle -->
			</div><!-- /.intro__text -->

		<?php endwhile; endif; // have_rows( 'content' ) ?>

	</div><!-- /.intro -->
</div><!-- container -->

