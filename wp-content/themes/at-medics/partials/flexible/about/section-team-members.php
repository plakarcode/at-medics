<?php
/**
 * Intro Text
 *
 * Template part for rendering ACF flexible sections - team members
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="container">
	<div class="slider-thumb">

	    <div class="slider slider-nav">
			<?php if ( have_rows( 'team_member' ) ) : while ( have_rows( 'team_member' ) ) : the_row(); ?>
	        <div>
	            <div class="img-wrap" style="background: url(<?php the_sub_field( 'image' ); ?>);">
	                <img src="<?php the_sub_field( 'image' ); ?>" alt="" />
	            </div><!-- end of .img-wrap -->
	            <div class="text-wrap">
	                <?php acf_sub_field( 'name', true, '<h4>', '</h4>' ); ?>
	                <?php acf_sub_field( 'position', true, '<p>', '</p>' ); ?>
	            </div><!-- end of .text-wrap -->
	        </div>
		    <?php endwhile; endif; ?>
	    </div>

	    <div class="slider slider-for">
			<?php if ( have_rows( 'team_member' ) ) : while ( have_rows( 'team_member' ) ) : the_row(); ?>
	        <div>
	        	<div class="slider-thumb__big-img" style="background-image: url(<?php the_sub_field( 'image' ); ?>);">
		            <img src="<?php the_sub_field( 'image' ); ?>" alt="" class="slider-thumb__big-img" />
		        </div>
	            <div class="slider-thumb__content">
	                <?php 
	                	acf_sub_field( 'name', true, '<h2>', '</h2>' ); 
	                	acf_sub_field( 'position', true, '<span class="slider-thumb__title">', '</span>' );
	                	acf_sub_field( 'content', true, '<p class="slider-thumb__text">', '</p>' );
	                ?>
	                <div class="slider-thumb__social">
	                	<?php 
		                	$twitter_url = acf_sub_field( 'twitter', false ); 
		                	$linkedin_url = acf_sub_field( 'linkedin', false ); 
		                ?>
	                	<?php if ( acf_sub_field( 'twitter', false ) ) : ?>
	                    <a href="<?php echo $twitter_url; ?>">TWITTER</a>
		                <?php endif; ?>
		                <?php if ( acf_sub_field( 'linkedin', false ) ) : ?>
	                    <a href="<?php echo $linkedin_url; ?>">Linkedin</a>
	                    <?php endif; ?>
	                </div><!-- end of .slider-thumb__social -->
	            </div><!-- end of .slider-thumb__content -->
	        </div>
			<?php endwhile; endif; ?>
	    </div>

	</div>
</div>