<?php
/**
 * Members
 *
 * Template part for rendering ACF flexible sections - members
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="members-home">
	<div class="container container--narrow">
		<?php
			/**
			 * Get title
			 */
			acf_sub_field( 'title', true, '<h2>', '</h2>' ); 
			/**
			 * Get description
			 */
			acf_sub_field( 'description', true ); 
		?>
	</div><!-- end of .container container--middle -->

<?php
	/**
	 * Get the number of members for the query
	 */
	if ( acf_sub_field( 'number_of_members', false ) ) {
		$posts_per_page = acf_sub_field( 'number_of_members', false );
	} else {
		$posts_per_page = 4;
	}
	/**
	 * Get members query
	 * @var obj|Error
	 */
	$members = get_members( $posts_per_page );

	if ( $members->have_posts() ) : ?>

	<div class="container">
		<div class="layout">

			<?php while ( $members->have_posts() ) : $members->the_post(); ?>
				<div class="layout__item large-and-up-1/4">

					<?php if ( has_post_thumbnail() ) :
						$src = get_featured_image_src( get_the_ID(), 'medium' ); ?>
						<div class="members__image members__image--circle" style="background-image:url(<?php echo $src; ?>);">
							<?php house_featured_image( 'medium', false ); ?>
						</div><!-- /.members__image -->
					<?php endif; // has_post_thumbnail() ?>

					<h3><?php the_title(); ?></h3>

				</div><!-- /.layout__item -->
			<?php endwhile; // $members->have_posts() ?>

		</div><!-- end of .layout -->
	</div><!-- end of .container -->
	<?php endif; // $members->have_posts() ?>
</section>