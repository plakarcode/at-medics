<?php
/**
 * Who we are
 *
 * Template part for rendering ACF flexible sections - who we are
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="who-we-are">
	<div class="container container--narrow">

		<?php
			acf_sub_field( 'title', true, '<h2>', '</h2>' );
			acf_sub_field( 'description', true );
		?>

	</div><!-- container container--narrow -->
</section><!-- who-we-are -->

