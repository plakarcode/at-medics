<?php
/**
 * Process Points
 *
 * Template part for rendering ACF flexible sections - process points
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'points' ) ) :
/**
 * Initiate counter
 * @var integer
 */
$i = 0; ?>

<section class="who-we-are">

		<div class="who-we-are__panel">
			<div class="container">

				<?php while ( have_rows( 'points' ) ) : the_row();
					/**
					 * Count panels and add class to every even
					 */
					if ( $i%2 == 0 ) {
						$class = '';
					} else {
						$class = 'layout--rev';
					}

					$image = acf_sub_field( 'image', false );

					$src = get_template_directory_uri() . '/images/' . $image . '.png';
					$retina = get_template_directory_uri() . '/images/' . $image . '-retina.png 2x';
				?>
				<div class="layout layout--middle <?php echo $class; ?>">
					<div class="layout__item large-and-up-1/2 text-center">
						<?php if ( get_sub_field( 'image' ) ) : ?>
							<picture>
	                            <source srcset="<?php echo $src . ', ' . $retina; ?>">
	                            <img srcset="<?php echo $src . ', ' . $retina; ?>" alt="how it works">
	                        </picture>
						<?php endif; // get_sub_field( 'image' ) ?>
					</div>
					<div class="layout__item large-and-up-1/2">
						<?php 
							acf_sub_field( 'title', true, '<h2>', '</h2>' );
							acf_sub_field( 'description', true );

							if ( acf_sub_field( 'link_label', false ) ) {
								$link_label = acf_sub_field( 'link_label', false );
							} else {
								$link_label = __( 'Learn more', 'house' );
							}

						if ( acf_sub_field( 'link_url', false ) ) : 
							$link_url = acf_sub_field( 'link_url', false ); ?>
							<a href="<?php echo $link_url; ?>" class="read-more"><?php echo $link_label; ?></a>
						<?php endif; //  get_sub_field( 'link_url' ) ?>
					 </div>
				</div><!-- /.layout layout--middle <?php echo $class; ?> -->
				<?php $i++; ?>
				<?php endwhile; // have_rows( 'points' ) ?>

			</div><!-- end of .container -->
		</div>

</section><!-- text-img -->

<?php endif; // have_rows( 'points' )
