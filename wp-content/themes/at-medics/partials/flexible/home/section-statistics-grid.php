<?php
/**
 * Statistics grid
 *
 * Template part for rendering ACF flexible sections - statistics grid
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="animation-element">
	<div class="flex-grid">

	<?php
		/**
		 * Start left box
		 */
		if ( have_rows( 'left_box' ) ) : while ( have_rows( 'left_box' ) ) : the_row();
			/**
			 * Image field returns attachment array so that we can use 'large' size
			 * @var array
			 */
			$image = acf_sub_field( 'image', false ); ?>
		<div class="flex-grid__left" style="background-image:url(<?php echo $image['sizes']['large']; ?>)">
			<div class="flex-grid__content">

				<?php acf_sub_field( 'before_number', true, '<div>', '</div>' );  ?>

				<dl>
					<?php 
						acf_sub_field( 'number', true, '<dt>', '</dt>' );  
						acf_sub_field( 'after_number', true, '<dd>', '</dd>' );  
					?>
				</dl>

			</div><!-- /.flex-grid__content -->
		</div><!-- /.flex-grid__left -->
	<?php endwhile; endif; // have_rows( 'left_box' ) ?>

	<?php
		/**
		 * Start right box
		 */
		if ( have_rows( 'right_box' ) ) : while ( have_rows( 'right_box' ) ) : the_row();
			$top_left = get_sub_field( 'top_left' );
			$top_right = get_sub_field( 'top_right' );
			$bottom = get_sub_field( 'bottom' ); ?>

		<div class="flex-grid__right">

			<?php if ( $top_left ) : ?>
			<div class="flex-grid__half">
				<div class="flex-grid__content">

					<?php if ( $top_left[0]['before_number'] ) : ?>
						<div><?php echo $top_left[0]['before_number']; ?></div>
					<?php endif; // $top_left[0]['before_number'] ?>

					<dl>
						<?php if ( $top_left[0]['number'] ) : ?>
							<dt><?php echo $top_left[0]['number']; ?></dt>
						<?php endif; // $top_left[0]['number'] ?>

						<?php if ( $top_left[0]['after_number'] ) : ?>
							<dd><?php echo $top_left[0]['after_number']; ?></dd>
						<?php endif; // $top_left[0]['after_number'] ?>
					</dl>

				</div><!-- /.flex-grid__content -->
			</div><!-- /.flex-grid__half -->
			<?php endif; // $top_left ?>

			<?php if ( $top_right ) : ?>
			<div class="flex-grid__half" style="background-image:url(<?php echo $top_right[0]['image']['sizes']['large']; ?>)">
				<div class="flex-grid__content">

					<?php if ( $top_right[0]['before_number'] ) : ?>
						<div><?php echo $top_right[0]['before_number']; ?></div>
					<?php endif; // $top_right[0]['before_number'] ?>

					<dl>
						<?php if ( $top_right[0]['number'] ) : ?>
							<dt><span class="count"><?php echo $top_right[0]['number']; ?></span></dt>
						<?php endif; // $top_right[0]['number'] ?>

						<?php if ( $top_right[0]['after_number'] ) : ?>
							<dd><?php echo $top_right[0]['after_number']; ?></dd>
						<?php endif; // $top_right[0]['after_number'] ?>
					</dl>

				</div><!-- /.flex-grid__content -->
			</div><!-- /.flex-grid__half -->
			<?php endif; // $top_right ?>

			<?php if ( $bottom ) : ?>
			<div class="flex-grid__full">
				<div class="flex-grid__content">

					<?php if ( $bottom[0]['before_number'] ) : ?>
						<div><?php echo $bottom[0]['before_number']; ?></div>
					<?php endif; // $bottom[0]['before_number'] ?>

					<dl>
						<?php if ( $bottom[0]['number'] ) : ?>
							<dt><?php echo $bottom[0]['number']; ?></dt>
						<?php endif; // $bottom[0]['number'] ?>

						<?php if ( $bottom[0]['after_number'] ) : ?>
							<dd><?php echo $bottom[0]['after_number']; ?></dd>
						<?php endif; // $bottom[0]['after_number'] ?>
					</dl>

				</div><!-- /.flex-grid__content -->
			</div><!-- /.flex-grid__full -->
			<?php endif; // $bottom ?>

		</div><!-- /.flex-grid__right -->

	<?php endwhile; endif; // have_rows( 'right_box' ) ?>

	</div><!-- /.flex-grid -->
</section><!-- animation-element -->

