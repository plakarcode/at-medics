<?php
/**
 * Image
 *
 * Template part for rendering ACF flexible sections - image
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * Translators: field id, echo (false returns value), before, after
 */
$image = acf_sub_field( 'image', false );

if ( $image ) :
	/**
	 * Prepare sizes
	 * @var string
	 */
	$thumbnail = $image['sizes']['thumbnail'];
	$medium    = $image['sizes']['medium'];
	$large     = $image['sizes']['large'];
	$full      = $image['url'];
?>

<img src="<?php echo $full; ?>" >

<?php endif; // $image