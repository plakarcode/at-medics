<?php
/**
 * Blockquote
 *
 * Template part for rendering ACF flexible sections - blockquote
 *
 * Used in flexible-templates/
 *         - sections.php
 *         - sections-get-started.php
 *         - sections-steps.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'blockquote' ) ) : while ( have_rows( 'blockquote' ) ) : the_row( 'blockquote' );

	/**
	 * Translators: field id, echo (false returns value), before, after
	 */
	acf_sub_field( 'quote', true, '<blockquote class="quote-intro"><p>', '</p></blockquote>' );

endwhile; endif;