<?php
/**
 * Button
 *
 * Template part for rendering ACF flexible sections - button
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'button' ) ) {
	return;
} ?>

	<?php if ( have_rows( 'button' ) ) : while ( have_rows( 'button' ) ) : the_row(); ?>

		<?php 
			if ( get_sub_field( 'type' ) == 'url' ) : ?>
				<a href="<?php the_sub_field( 'url' ); ?>" class="btn btn--primary btn--secondary-border" target="_blank"><?php the_sub_field( 'button_text' ); ?></a>
			<?php else : ?>
				<a href="<?php the_sub_field( 'documents' ); ?>" class="btn btn--primary btn--secondary-border" download><?php the_sub_field( 'button_text' ); ?></a>
			<?php endif;
		?>


	<?php endwhile; endif; ?>