<?php
/**
 * Files for downloading
 *
 * Template part for rendering ACF flexible sections - files
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

acf_sub_field( 'title', true, '<h3>', '</h3>' );

if ( have_rows( 'files' ) ) : ?>

	<div class="documents">

		<?php while ( have_rows( 'files' ) ) : the_row();

			if ( get_sub_field( 'file' ) ) :
				/**
				 * Get the file array
				 * @var array
				 */
				$file = get_sub_field( 'file' );
				$url = $file['url'];
				$name = $file['filename']; ?>

				<div class="documents__item">
					<a href="<?php echo $url; ?>" download>
						<?php house_file_icon( $file ); ?>
						<span><?php echo $name; ?></span>
					</a>
				</div><!-- /.documents__item -->

			<?php endif; // get_sub_field( 'file' )

		endwhile; // have_rows( 'files' ) ?>
	</div><!-- /.documents -->

<?php endif; // have_rows( 'files' )
