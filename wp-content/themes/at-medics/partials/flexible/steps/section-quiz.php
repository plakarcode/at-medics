<?php
/**
 * Quiz
 *
 * Template part for rendering ACF flexible sections - quiz
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$questions = get_sub_field( 'questions' );
$letters = [
	1 => 'A',
	2 => 'B',
	3 => 'C',
	4 => 'D',
];
?>
<div class='entry-content mt+'>
	<a href="#training-popup" class="js-training-popup btn btn--primary btn--secondary">Start training Assessment </a>
</div> <!-- end of .entry-content -->

<div id = "training-popup" class = "training-popup magnific-popup mfp-hide magnific-animate--fade">

	<?php acf_sub_field( 'title', true, '<h1>', '</h1>' ); ?>
	
	<div class = "progress-bar js-quiz-bar">
		<p><span class='js-quiz-percentage'>0</span> % completed</p>
		<div class = "w3-progress-container w3-round-xlarge">
			<div class = "w3-progressbar w3-round-xlarge js-quiz-progressbar" style = "width:0%"></div>
		</div>
	</div><!--end of .training-popup__progress -->

	<form action = "" method = "get" class = "question js-quiz">
		
		<?php $q_count = $a_count = 1; foreach ( $questions as $question ) : $o_count = 1; ?>
		
			<div class = "question-item js-question-<?php echo $q_count; ?>">
				<div class="question-item__title">
					<h3><?php echo $question['question']; ?></h3>
					<p><?php echo $question['instruction']; ?></p>
				</div>

				<?php foreach ( $question['answers'] as $answer ) : ?>

					<div class = "checkbox_wrap" data-quiz-c='<?php echo $answer['correct']; ?>'>
						<input class = "styled-checkbox " type = "checkbox" id = "chk<?php echo $a_count; ?>" name = "chk<?php echo $a_count; ?>" />
						<label for = "chk<?php echo $a_count; ?>" class='js-quiz-label'>
							<span><b></b></span>
							<b class = "type-medium"><?php echo $letters[$o_count]; ?>.</b> <?php echo $answer['answer']; ?>
						</label>
					</div>
				
				<?php $o_count++; $a_count++; endforeach; ?>
				
			</div><!--end of .question -->
			
		<?php $q_count++; endforeach; ?>
			
		<div class = "question-nav">
			<button class = "btn btn--primary btn--secondary btn--prev js-quiz-prevbutton">Prev question</button>
			<button class = "btn btn--primary btn--secondary btn--next js-quiz-nextbutton" disabled>Next question</button>
			<input type = "submit" class = "btn btn--primary btn--secondary btn--finish js-quiz-finbutton" value = "Finish" disabled>
		</div><!--end of .question-item__nav -->
			
	</form>
	<div class = "results js-quiz-failurebox is-hidden">
		<div class = "layout layout--middle">
			<div class = "layout__item 5/12">
				<div class = "chart js-quiz-resultgraph" id = "failgraph" data-percent = "92"></div>
			</div>
			<div class = "layout__item 7/12">
				<h2>Your Result</h2>
				<div class = "results--percents">
					<span><strong class='js-quiz-percentcorrect'>92 %</strong> Correct answers</span>
					<span><strong class='js-quiz-percentincorrect'>8 %</strong> Incorrect answers</span>
				</div>
				<?php acf_sub_field( 'success_message', true, '<p>', '</p>' ); ?>
			</div>
		</div>
		<div class = "question-nav">
			<button class = "btn btn--primary btn--secondary btn--wva js-quiz-reset">Watch videos again</button>
		</div><!--end of .question-item__nav -->
	</div>
	<div class = "results js-quiz-successbox is-hidden">
		<div class = "layout layout--middle">
			<div class = "layout__item 5/12">
				<div class = "chart js-quiz-resultgraph" id = "successgraph" data-percent = "100"></div>
			</div>
			<div class = "layout__item 7/12">
				<h2>Congratulation!</h2>
				<div class = "results--percents">
					<span><strong class='js-quiz-percentcorrect'>100 %</strong> Correct answers</span>
					<span><strong class='js-quiz-percentincorrect'>0 %</strong> Incorrect answers</span>
				</div>
				<?php acf_sub_field( 'failure_message', true, '<p>', '</p>' ); ?>
			</div>
		</div>
		<div class = "question-nav">
			<a href='/get-started/step-4/' class="btn btn--primary btn--secondary btn--wva">proceed to step 4</a>
		</div><!--end of .question-item__nav -->
	</div>
</div><!--/ .training-popup -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/quiz.min.js"></script>