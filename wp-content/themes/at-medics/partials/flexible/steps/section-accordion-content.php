<?php
/**
 * Accordion Content
 *
 * Template part for rendering ACF flexible sections - accordion content
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'accordion_content' ) ) {
	return;
}
?>
<?php 
	$i = 1;
	if( have_rows('accordion_content') ) : while( have_rows('accordion_content') ) : the_row(); 
?>
	<div class="step-part" id="step-chk<?php echo $i++; ?>">
							
		<?php
		/**
		 * Get subtitle
		 * Translators: field id, echo
		 */
		acf_sub_field( 'subtitle', true, '<h3>', '</h3>' );
		acf_sub_field( 'content', true, '<p>', '</p>' );
		$url = acf_sub_field( 'button', false );

		echo '<a href="' . $url . '" class="btn btn--primary btn--secondary-border" download>Download document</a>';
		?>

	</div>
	<!-- /.step-part -->
<?php endwhile; endif;?>
