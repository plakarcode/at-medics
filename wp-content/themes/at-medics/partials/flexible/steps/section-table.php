<?php
/**
 * Table
 *
 * Template part for rendering ACF flexible sections - table
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

<table class="table">

	<?php if ( have_rows( 'columns_titles' ) ) : ?>
		<thead>
			<?php while ( have_rows( 'columns_titles' ) ) : the_row(); ?>

			<tr>
				<?php if ( get_sub_field( 'item_title' ) ) : ?>
				<th><?php the_sub_field( 'item_title' ); ?></th>
				<?php endif; // get_sub_field( 'item_title' )

				if ( get_sub_field( 'value_title' ) ) : ?>
					<th><?php the_sub_field( 'value_title' ); ?></th>
				<?php endif; // get_sub_field( 'value_title' ) ?>
			</tr>

			<?php endwhile; // have_rows( 'columns_titles' ) ?>
		</thead>
	<?php endif; // have_rows( 'columns_titles' )


	if ( have_rows( 'row' ) ) : ?>
	<tbody>

		<?php while ( have_rows( 'row' ) ) : the_row(); ?>
		<tr>
			<td data-th="Time">
				<?php if ( get_sub_field( 'item' ) ) :
					the_sub_field( 'item' );
				endif; // get_sub_field( 'item' ) ?>
			</td>

			<td data-th="Suggested agenda">
				<?php if ( get_sub_field( 'value' ) ) :
					the_sub_field( 'value' );
				endif; // get_sub_field( 'value' ) ?>
			</td>
		</tr>
		<?php endwhile; // have_rows( 'row' ) ?>

	</tbody>
	<?php endif; // have_rows( 'row' ) ?>

</table><!-- table -->
