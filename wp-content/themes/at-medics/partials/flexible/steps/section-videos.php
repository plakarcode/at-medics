<?php
/**
 * Video
 *
 * Template part for rendering ACF flexible sections - video
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$video = 1;
if ( have_rows( 'video' ) ) : while ( have_rows( 'video' ) ) : the_row();

	if ( acf_sub_field( 'url', false ) ) :
		$url = acf_sub_field( 'url', false );
		$video_id = get_youtube_video_id( $url ); ?>

		<div class="video-object">
			<figure class="video-figure" data-url="<?php echo $url; ?>" data-id="<?php echo $video_id; ?>">
				<?php
					/**
					 * Get the video cover
					 */
					if ( acf_sub_field( 'image', false ) ) {
						$image = get_sub_field( 'image' );
						$src = $image['sizes']['large'];
					} else {
						$src = set_video_image_src( $url );
					}
				?>

				<a href="javascript:;" class="js-video-play video-thumb" style="background-image:url(<?php echo $src; ?>);">
					<img src="<?php echo $src; ?>" alt="video" />
					<div class="video-object__play"><?php echo house_svg_icon( 'play' ); ?></div>
				</a>

				<?php acf_sub_field( 'caption', true, '<figcaption>', '</figcaption>' ); ?>

			</figure>
		</div><!-- /.video-object -->

	<?php endif; // acf_sub_field( 'url', false )

endwhile; endif; // have_rows( 'video' )