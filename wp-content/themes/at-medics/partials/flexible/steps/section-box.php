<?php
/**
 * BOX
 *
 * Template part for rendering ACF flexible sections - box
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="step-box">

	<?php
		/**
		 * Get content
		 * Translators: field id, echo by default
		 */
		acf_sub_field( 'content' );

		/**
		 * Get button type and label, if any
		 * Translators: field id, echo
		 * @var string
		 */
		$button = acf_sub_field( 'select_button', false );

		if ( $button === 'book' ) {
			/**
			 * Translators: field id, echo, before, after
			 */
			acf_sub_field( 'button_label', true, '<a href="#open-workshop" class="js-workshop btn btn--primary btn--secondary">', '</a>' );
		} elseif ( $button === 'upload' ) {
			// get themplate part with file upload form
			// <button class="btn btn--primary btn--secondary" >Upload documents</button>
			/**
			 * Translators: field id, echo, before, after
			 */
			acf_sub_field( 'button_label', true, '<button class="btn btn--primary btn--secondary">', '</button>' );
		}
	?>

</div><!-- /.step-box -->
