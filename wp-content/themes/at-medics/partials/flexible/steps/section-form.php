<?php
/**
 * Form
 *
 * Template part for rendering ACF flexible sections - form
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="week-plan">
	<?php acf_sub_field( 'title', true, '<h2>', '</h2>' ); ?>

	<form action="" method="post" id="implementationform" target="formtarget" name="implementationform">

		<?php // fake ajax with iframe ?>
		<iframe name="formtarget" class="is-hidden"></iframe>

		<div class="layout">
			<div class="layout__item large-and-up-1/2">
				<input type="text" class="input input--border" value="NHS Lambeth CCG" name="practice_name" id="practice_name">
			</div><!-- /.layout__item -->

			<div class="layout__item large-and-up-1/2">
				<input type="text" class="input input--border" value="Streatham High Practice" name="site" id="site">
			</div><!-- /.layout__item -->

			<div class="layout__item large-and-up-1/2">
				<div class="datepicker-wrap">
					<input class="input input--border datepicker" placeholder="Date of implementation" type="text" name="start_date" id="start_date">
					<?php house_svg_icon( 'calendar' ); ?>
				</div><!-- /.datepicker-wrap -->
			</div><!-- /.layout__item -->

			<div class="layout__item large-and-up-1/2">
				<input type="text" class="input input--border" placeholder="Implementation lead" name="implementation_lead" id="implementation_lead">
			</div><!-- /.layout__item -->

			<div class="layout__item large-and-up-1/2">
				<input type="text" class="input input--border" placeholder="Clinical governance lead" name="clinical_lead" id="clinical_lead">
			</div><!-- /.layout__item -->

			<div class="layout__item large-and-up-1/2">
				<!-- <input type="submit" value="submit" class="btn btn--primary btn--secondary btn--full" /> -->
				<a href="javascript:;" class="btn btn--primary btn--secondary btn--full" id="submit">Submit</a>
			</div><!-- /.layout__item -->
		</div><!-- /.layout -->
	</form>
</div><!-- week-plan -->

<script type="text/javascript">
	'use strict';

	var $ = window.jQuery;

	//ON DOCUMENT READY
	$(document).ready(function() {

		$("#submit").on( 'click', function(e) {
			e.preventDefault();
			// clear previous inputs
			$('.success').html('');

			$('.success').prepend('<ul>');

			if ( $('#practice_name').val() ) {
				var value = $('#practice_name').val();
				$('.success').append( '<li>' + value + '</li>' );
			}
			if ( $('#site').val() ) {
				var value = $('#site').val();
				$('.success').append( '<li>' + value + '</li>' );
			}
			if ( $('#start_date').val() ) {
				var value = $('#start_date').val();
				$('.success').append( '<li>' + value + '</li>' );
			}
			if ( $('#implementation_lead').val() ) {
				var value = $('#implementation_lead').val();
				$('.success').append( '<li>' + value + '</li>' );
			}
			if ( $('#clinical_lead').val() ) {
				var value = $('#clinical_lead').val();
				$('.success').append( '<li>' + value + '</li>' );
			}
			$('.success').append( '</ul>' );

			$('#implementationform').submit();
			$('.success').show();
		});
	});
</script>

<div class="success"></div>

<?php
/**
 * Get the posted data
 */
if ( isset( $_POST ) && ! empty( $_POST ) ) :

endif; // isset( $_POST ) && ! empty( $_POST )
?>