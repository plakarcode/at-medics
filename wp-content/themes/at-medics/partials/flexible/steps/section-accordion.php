<?php
/**
 * Accordion
 *
 * Template part for rendering ACF flexible sections - accordion
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'accordion' ) ) {
	return;
}
?>
<div class="accordion">


		<div class="accordion__item">
			<?php if ( get_sub_field( 'title' ) ) : ?>
				<h2 class="accordion__item-title">
					<a href=""><?php the_sub_field( 'title' ); ?></a>
					<span class="icon-arrow"><?php echo house_svg_icon( 'arrow-down' ); ?></span>
				</h2>
			<?php endif; // get_sub_field( 'title' )?>

				<div class="accordion__item-content">

					<?php
						get_template_part( 'partials/flexible-templates/sections', 'accordion' );
					?>
				</div>

		</div><!-- accordion__item -->

</div><!-- accordion -->
