<?php
/**
 * Step Circles
 *
 * Template part for rendering ACF flexible sections - Step Circles
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'circles' ) ) : ?>

	<div class="circles">
		<div class="justifize">

			<?php while ( have_rows( 'circles' ) ) : the_row( 'circles' ); ?>
				<div class="justifize__box">
					<div class="three-phase-circles__icon-wrap">
						<?php if ( get_sub_field( 'title' ) ) : ?>
							<div class="three-phase-circles__icon-circle">
								<span><?php the_sub_field( 'title' ); ?></span>
							</div>
						<?php endif; // get_sub_field( 'title' )

						if ( get_sub_field( 'tooltip_content' ) ) : ?>
							<div class="three-phase-circles__tooltip">
								<?php the_sub_field( 'tooltip_content' ); ?>
							</div><!-- end of .three-phase-circles__tooltip -->
						<?php endif; // get_sub_field( 'tooltip_content' ) ?>
					</div><!-- three-phase-circles__icon-wrap -->
				</div><!-- justifize__box -->

				<div class="justifize__box icon-wrap">
					<?php echo house_svg_icon( 'arrow2-right' ); ?>
				</div><!-- justifize__box icon-wrap -->
			<?php endwhile; // have_rows( 'circles' ) ?>

		</div><!-- justifize -->
	</div><!-- /.circles -->

<?php endif; // have_rows( 'circles' )
