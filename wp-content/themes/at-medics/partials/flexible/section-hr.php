<?php
/**
 * Horizontal rule
 *
 * Template part for rendering ACF flexible sections - hr
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<hr>