<?php
/**
 * GRAPHIC
 *
 * Template part for rendering ACF flexible sections - graphic
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

</div><!-- /.container -->

	<section class="managm-flow">
		<div class="container">
			<picture>
				<source srcset="<?php echo get_template_directory_uri(); ?>/images/ez-doc-cycle.png, <?php echo get_template_directory_uri(); ?>/images/ez-doc-cycle-2.png 2x">
				<img srcset="<?php echo get_template_directory_uri(); ?>/images/ez-doc-cycle.png, <?php echo get_template_directory_uri(); ?>/images/ez-doc-cycle-2.png 2x">
			</picture>
		</div>
	</section><!-- end of .managm-flow -->

<div class="container">