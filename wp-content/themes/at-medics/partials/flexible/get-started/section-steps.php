<?php
/**
 * Steps
 *
 * Template part for rendering ACF flexible sections - steps
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

if ( have_rows( 'steps' ) ) : ?>
<section class="steps entry-content">
	<div class="container">

		<?php
			$i = 1;
			while ( have_rows( 'steps' ) ) : the_row( 'steps' );
				$steps = array( 'step one', 'step two', 'step three',  'step four', 'step five', );

				$a = $i - 1;
				$src = get_template_directory_uri() . '/images/step'. $i . '.jpg';
				$retina = get_template_directory_uri() . '/images/step'. $i . '-retina.jpg 2x'; ?>

				<div class="steps__list">
					<div class="layout">
						<div class="layout__item large-and-up-2/5">
							<picture>
								<source srcset="<?php echo $src . ', ' . $retina; ?>">
								<img srcset="<?php echo $src . ', ' . $retina; ?>" alt="">
							</picture>
						</div>
						<div class="layout__item large-and-up-3/5">
							<span><?php echo $steps[$a]; ?></span>

							<?php 
								acf_sub_field( 'title', true, '<h2>', '</h2>' );
								acf_sub_field( 'content', true, '<p>', '</p>' );
								$url = acf_sub_field( 'url', false );

							if ( get_sub_field( 'url' ) ) : ?>
								<a href="<?php echo $url; ?>" class="read-more">Learn more</a><!-- /.read-more -->
							<?php endif; // acf_sub_field( 'url', false ) ?>
						</div>
					</div>
				</div><!-- /.steps__list -->
		<?php $i++; endwhile; ?>

		<?php if ( acf_sub_field( 'button_url', false ) ) : 
			$button_url = acf_sub_field( 'button_url', false ); ?>
			<div class="text-center">
				<a href="<?php echo $button_url; ?>" class="btn btn--primary btn--secondary">Start now</a>
			</div><!-- end of .text-center -->
		<?php endif; // acf_sub_field( 'button_url', false ) ?>

	</div>
</section>
<?php endif; ?>