<?php
/**
 * "Small" Heading
 *
 * Template part for rendering ACF flexible sections - h4 heading
 *
 * Used in flexible-templates/
 *         - sections.php
 *         - sections-legal.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'heading', true, '<h4>', '</h4>' );
