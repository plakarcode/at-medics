<?php
/**
 * Regular paragraph
 *
 * Template part for rendering ACF flexible sections - regular paragraph
 *
 * Used in flexible-templates/
 *         - sections.php
 *         - sections-accordion.php
 *         - sections-case-study.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'paragraph', true, '<p>', '</p>' );
