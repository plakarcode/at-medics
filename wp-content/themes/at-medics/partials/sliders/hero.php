<?php
/**
 * Hero Slider
 *
 * Template part for rendering hero slider section.
 *
 * @package WordPress
 */
if ( have_rows( 'hero_slides' ) ) : ?>

<section class="hero-slider-wrap">
	<div class="hero-slider">

	<?php while ( have_rows( 'hero_slides' ) ) : the_row(); ?>

		<?php
			/**
			 * Don't even think about slide without the image
			 */
			if ( get_sub_field( 'image' ) ) : ?>

			<div class="hero-slider__item" style="background-image: url('<?php the_sub_field( 'image' ); ?>');">

			<?php
				/**
				 * Don't start text markup without text elements
				 */
				if ( get_sub_field( 'slide_text' ) ) :
					// get the text repeater in array
					$text = get_sub_field( 'slide_text' );
					// only one row is allowed, index [0] i safe here
					$text = $text[0]; ?>

					<div class="tableize tableize--full tableize--middle">
						<div class="tableize__cell">
							<div class="container fade-in-top">

							<?php
								/**
								 * Ge ttitle
								 */
								if ( $text['title'] ) : ?>

									<div class="heading-blue"><h1><?php echo $text['title']; ?></h1></div>

								<?php endif; // $text['title']

								/**
								 * Get description
								 */
								if ( $text['description'] ) :

									echo $text['description'];

								endif; // $text['description']

								/**
								 * Don't start button if we have no url
								 */
								if ( $text['link_url'] ) :
									// set default button label
									if ( $text['link_label'] ) {
										$label = $text['link_label'];
									} else {
										$label = 'Find out';
									} ?>

									<a href="<?php echo $text['link_url']; ?>" class="btn btn--primary btn--white"><?php echo $label; ?></a>
								<?php endif; // $text['link_url'] ?>

							</div><!-- container fade-in-top -->
						</div><!-- tableize__cell -->
					</div><!-- tableize tableize--full tableize--middle -->

				<?php endif; // get_sub_field( 'slide_text' ) ?>

			</div><!-- style -->
		<?php endif; // get_sub_field( 'image' )

		endwhile; // have_rows( 'hero_slides' ) ?>

	</div><!-- hero-slider -->
</section><!-- hero-slider-wrap -->

<?php endif; // have_rows( 'hero_slides' )