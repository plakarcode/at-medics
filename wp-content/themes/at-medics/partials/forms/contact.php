<?php
/**
 * Contact form
 *
 * Template part for rendering contact form
 *
 * @package WordPress
 */
/**
 * Don't even bother if plugin is not active
 */
if ( ! house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
	return;
}
/**
 * Get the section title
 */
if ( get_field( 'contact_section_title' ) ) : ?>

	<h2><?php the_field( 'contact_section_title' ); ?></h2>

<?php endif; // get_field( 'contact_section_title' )

/**
 * Get the form
 */
if ( get_field( 'contact_form_id' ) ) :
	echo do_shortcode( '[contact-form-7 id="' . get_field( 'contact_form_id' ) . '"]' );
endif; // get_field( 'contact_form_id' )