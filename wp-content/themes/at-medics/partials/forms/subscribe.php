<?php
/**
 * Subscribe form
 *
 * Template part for rendering subscribe section with MailChimp form
 *
 * @package WordPress
 */
?>
<section id="subscribe" class="subscription">
	<div class="container container--middle">
		<div class="layout layout--middle">

			<div class="layout__item large-and-up-2/5">
				<?php if ( get_field( 'subscribe_section_title', 'option' ) ) : ?>
					<h1><?php the_field( 'subscribe_section_title', 'option' ); ?></h1>
				<?php endif; // get_field( 'subscribe_section_title', 'option' ) ?>
			</div><!-- layout__item large-and-up-2/5 -->

			<div class="layout__item large-and-up-3/5">
				<?php get_template_part( 'partials/forms/mailchimp' ); ?>
			</div><!-- layout__item large-and-up-3/5 -->

		</div><!-- layout layout--middle -->
	</div><!-- container container--middle -->
</section><!-- /.subscription -->