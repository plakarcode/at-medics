<?php
/**
 * MailChimp
 *
 * Template part for rendering MailChimp form
 *
 * @package WordPress
 */
/**
 * Don't even bother if we have no API Key or list ID
 */
if ( ! get_field( 'mailchimp_api_key', 'option' ) || ! get_field( 'mailchimp_list_id', 'option' ) ) {
	return;
}
/**
 * API Key
 * Account -> Extras -> API Keys
 *
 * @var MailChimp
 */
if ( get_field( 'mailchimp_api_key', 'option' ) ) {
	$apikey = get_field( 'mailchimp_api_key', 'option' );
} else {
	$apikey = '';
}
/**
 * List ID
 * @var string
 */
if ( get_field( 'mailchimp_list_id', 'option' ) ) {
	$list_id = get_field( 'mailchimp_list_id', 'option' );
} else {
	$list_id = '';
}
/**
 * Check if email address is submitted
 * and set $email var
 */
if ( ! empty( $_POST['email'] ) ) {
	$email = $_POST['email'];
} else {
	$email = '';
}
/**
 * Define vars
 * @var string
 */
$message = '';
$class = '';
/**
 * If email is submitted add subscriber to the list
 * and build the thank you message
 */
if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) {
	/**
	 * Get the MailChimp response
	 * @var array
	 */
	$response = get_mailchimp_response( $list_id, $email, $apikey );

	/**
	 * If status is not 'subscribed' we have error
	 */
	if ( $response['status'] !== 'subscribed' ) {
		$message = 'Error: ' . $response['title'] . '.';
		$class = 'error1';

	} else {

		if ( get_field( 'mailchimp_thank_you_message', 'option' ) ) {
			$message = get_field( 'mailchimp_thank_you_message', 'option' );
		} else {
			$message = __( 'Thank you for subscribing.', 'house' );
		}

		$class = 'thanks';
	}
}
/**
 * If empty email field is submitted
 */
elseif ( isset( $_POST['email'] ) && empty( $_POST['email'] ) ) {

	if ( get_field( 'mailchimp_empty_field_message', 'option' ) ) {
		$message = get_field( 'mailchimp_empty_field_message', 'option' );
	} else {
		$message = __( 'Address field is empty. Please type your email address and try submitting again.', 'house' );
	}

	$class = 'error2';
}

/**
 * We are sending action to #subscribe as it is the id of the whole section.
 * This way, upon submitting user will be scrolled down to the section
 * and see feedback messages.
 */
?>
<form action="#subscribe" method="POST" class="subscription__form">
	<?php if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) : ?>
	<input type="email" name="email" id="email" class="input input--primary" placeholder="<?php echo $_POST['email']; ?>">
	<?php else : ?>
	<input type="email" name="email" id="email" class="input input--primary" placeholder="Your email address">
	<?php endif; // isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ?>
	<input type="submit" value="SUBMIT" name="submit" id="submit" class="btn btn--primary btn--text">
</form>

<?php if ( isset( $_POST['email'] ) ) : ?>
<div class="subscribe-message-wrap">
	<div class="subscribe-message subscribe-message--<?php echo $class; ?>">
		<span class="subscribe-message__text"><?php echo $message; ?></span>
		<span class="subscribe-message__close"><a href=""><?php echo house_svg_icon( 'close' ); ?></a></span>
	</div><!-- end of .subscribe-message -->
</div><!-- end of .subscribe-message-wrap -->
<?php endif; // isset( $_POST['email'] ) ?>