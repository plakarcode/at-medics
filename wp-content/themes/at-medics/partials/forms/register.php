<?php
/**
 * Register form
 *
 * Template part for rendering user registration form
 *
 * @package WordPress
 */
?>
<div id="open-signup" class="login-popup login-popup--signup magnific-popup mfp-hide magnific-animate--fade">

	<div class="login-popup__left">
	</div><!-- /.login-popup__left -->

	<div class="login-popup__form">
		<h2>SIGN UP</h2>
		<p>If you already have account plase login <a href="#open-login" class="js-login-popup">here</a></p>

		<?php echo do_shortcode( '[register_form]' ); ?>

	</div><!-- /.login-popup__form -->

</div><!-- /.login__popup -->