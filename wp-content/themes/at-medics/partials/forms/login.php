<?php
/**
 * Login form
 *
 * Template part for rendering user login form
 *
 * @package WordPress
 */
?>
<div id="open-login" class="login-popup magnific-popup mfp-hide magnific-animate--fade">

	<div class="login-popup__left">
	</div><!-- /.login-popup__left -->

	<div class="login-popup__form">
		<h2>Login</h2>
		<p>If you don’t have account plase sign up <a href="#open-signup" class="js-login-popup">here</a></p>
		<?php echo do_shortcode( '[login_form]' ); ?>

	</div><!-- /.login-popup__form -->

</div><!-- /.login__popup -->
