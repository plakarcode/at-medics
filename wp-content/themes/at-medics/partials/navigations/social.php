<?php
/**
 * Social links
 *
 * Template part for rendering social links list.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>

<div class="social-icons">
	<ul>

	<?php
		/**
		 * Get facebook link
		 */
		if ( get_social_profile_url( 'facebook' ) ) : ?>
			<li>
				<a href="<?php social_profile_url( 'facebook' ); ?>">
					<?php echo house_svg_icon( 'facebook' ); ?>
					<span>facebook</span>
				</a>
			</li>
		<?php endif; // get_social_profile_url( 'facebook' )

		/**
		 * Get twitter link
		 */
		if ( get_social_profile_url( 'twitter' ) ) : ?>
			<li>
				<a href="<?php social_profile_url( 'twitter' ); ?>">
					<?php echo house_svg_icon( 'twitter' ); ?>
					<span>Twitter</span>
				</a>
			</li>
		<?php endif; // get_social_profile_url( 'twitter' )

		/**
		 * Get google plus link
		 */
		if ( get_social_profile_url( 'google' ) ) : ?>
			<li>
				<a href="<?php social_profile_url( 'google' ); ?>">
					<?php echo house_svg_icon( 'google-plus' ); ?>
					<span>Google Plus</span>
				</a>
			</li>
		<?php endif; // get_social_profile_url( 'google' )

		/**
		 * Get instagram link
		 */
		if ( get_social_profile_url( 'instagram' ) ) : ?>
			<li>
				<a href="<?php social_profile_url( 'instagram' ); ?>">
					<?php echo house_svg_icon( 'instagram' ); ?>
					<span>Instagram</span>
				</a>
			</li>
		<?php endif; // get_social_profile_url( 'instagram' ) ?>

	</ul>
</div>
