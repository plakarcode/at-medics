<?php
/**
 * Subnavigation template part
 *
 * Template part for rendering subnav on parent/children pages.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
/**
 * Get all menu items from 'steps' menu
 * @var array
 */
$items = wp_get_nav_menu_items( 'steps' );

// current page id
$current_ID = get_the_ID();
/**
 * Declare custom array
 * @var array
 */
$array = [];

if ( isset( $items ) ) {
	foreach ( $items as $item ) {
		// all we need for each menu item is:
		// $item->menu_order - its order in menu
		// $item->object_id - page id (not menu item id)
		$array[$item->menu_order] = $item->object_id;
	}
}

if ( isset( $array ) ) : ?>

	<div class="step-links clearfix">

	<?php
		/**
		 * Search for current page id in array
		 * @var array
		 */
		$current_index = array_search( $current_ID, $array );
		// Find the index of the next/prev items
		$next = $current_index + 1;
		$prev = $current_index - 1;


		// prev can not exist on first page
		if ( $prev > 0 ) :
			/**
			 * Get the page url by id
			 * @var string
			 */
			$prev_url = get_permalink( $array[$prev] );

			echo house_primary_button( 'Prev Step', $prev_url, 'btn--secondary-border prev' );

		endif; // $prev > 0

		// next can not exist on last page
		if ( $next <= count( $array ) ) :
			/**
			 * Get the page url by id
			 * @var string
			 */
			$next_url = get_permalink( $array[$next] );

			echo house_primary_button( 'Next Step', $next_url, 'btn--secondary-border next' );

		endif; // $next <= count( $array ) ?>

	</div><!-- step-links clearfix -->

<?php endif; // isset( $array )