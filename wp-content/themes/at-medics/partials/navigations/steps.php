<?php
/**
 * Steps navigation template part
 *
 * Template part for rendering steps navigation for steps pages.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */

/**
 * Get the menu
 */
wp_nav_menu( array(
	'theme_location'  => 'steps',
	'container'       => 'nav',
	'container_class' => 'nav-steps',
	'walker'          => new Steps_Walker_Nav_Menu()
));


