<?php
/**
 * Breadcrumb template part
 *
 * Template part for rendering breadcrumbs.
 *
 * @package WordPress
 */
/**
 * Get the posts page id
 * That's the only way to get page title for blog page
 */
global $globalSite, $post;
$blog = $globalSite['blog']; ?>

<div class="container">
	<ul class="breadcrumbs">

		<li><a href="<?php echo $globalSite['home']; ?>">Home</a></li>

	<?php
		/**
		 * Any singular page, regardless the template
		 */
		if ( is_page() ) :
			$obj = get_post( get_the_ID() );

			// if page has parent, add it to breadcrumbs
			if ( $obj->post_parent > 0 ) :
				echo '<li><a href="' . get_permalink( $obj->post_parent ) . '">' . get_the_title( $obj->post_parent ) . '</a></li>';
			endif; // $obj->post_parent > 0 ?>

			<li class="is-active"><?php the_title(); ?></li>

	<?php
		/**
		 * Title for Blog page
		 * index.php
		 */
		elseif ( is_home() ) : ?>

			<li class="is-active"><?php echo get_the_title( $blog ); ?></li>

	<?php
		/**
		 * Category title
		 */
		elseif ( is_category() ) : ?>

			<li class="is-active"><?php single_cat_title(); ?></li>

	<?php
		/**
		 * Everything else
		 */
		else : ?>

		<li class="is-active"><?php the_title(); ?></li>

	<?php endif; // is_home() ?>

	</ul><!-- breadcrumbs -->
</div><!-- container -->

