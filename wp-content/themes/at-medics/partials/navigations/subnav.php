<?php
/**
 * Subnavigation template part
 *
 * Template part for rendering subnav on parent/children pages.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<div class="container">
	<?php
		/**
		 * Get the menu
		 */
		wp_nav_menu( array(
			'theme_location'  => 'submenu',
			'container'       => 'nav',
			'container_class' => 'nav-secondary',
			'menu_id'         => 'submenu'
		));
	?>
</div><!-- container -->

