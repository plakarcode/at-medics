<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'house' ); ?>"><?php _e( 'Skip to content', 'house' ); ?></a>

<nav id="site-navigation" class="nav-primary" role="navigation">
	<div>
		<?php
			/**
			 * Get the main menu
			 */
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'container'      => 'ul',
				'menu_id'        => 'main-menu',
				'menu_class'     => 'main-menu',
			));

		/**
		 * Show login and register buttons for not logged in visitors
		 */
		if ( ! is_user_logged_in() ) : ?>
			<div class="nav-primary__login">
				<a href="#open-login" class="btn btn--primary btn--secondary-border js-login-popup">login</a>
				<a href="#open-signup" class="btn btn--primary btn--secondary js-login-popup">sign up</a>
			</div><!-- end of .nav-primary__login -->
		<?php endif; // ! is_user_logged_in() ?>
	</div>
</nav>

<div class="nav-trigger"><span></span></div>