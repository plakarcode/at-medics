<?php
/**
 * Secondary navigation template part
 *
 * Template part for rendering secondary navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
wp_nav_menu( array(
	'theme_location'  => 'secondary',
	'container'       => 'nav',
	'container_class' => 'nav-footer',
));