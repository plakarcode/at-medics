<?php
/**
 * Resources Document
 *
 * Template part for rendering document for resorces
 *
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_title( '<h1>', '</h1>' ); ?>

</article><!-- #post -->