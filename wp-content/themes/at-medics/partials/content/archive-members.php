<?php
/**
 * Archive Members
 *
 * Template part for rendering archive listing for 'member' post type
 *
 * @package WordPress
 */
/**
 * Get members query
 * Translator: number of posts per page.
 * @var obj|Error
 */
$members = get_members( 4 );

if ( $members->have_posts() ) : ?>

<div class="layout">

	<?php while ( $members->have_posts() ) : $members->the_post(); ?>

	<div class="layout__item large-and-up-1/2">

		<div class="members">
			<?php if ( has_post_thumbnail() ) :
				$src = get_featured_image_src( get_the_ID(), 'large' ); ?>
				<div class="members__image" style="background-image:url(<?php echo $src; ?>);">
					<?php house_featured_image( 'large', false ); ?>
				</div><!-- /.members__image -->
			<?php endif; // has_post_thumbnail() ?>

			<div class="members__info">
				<h2><a href="#open-popup-<?php the_ID(); ?>" class="js-open-popup"><?php the_title(); ?></a></h2>

				<?php if ( acf_field( 'member_contact_info', false ) ) :
					$repeater = acf_field( 'member_contact_info', false ); ?>
					<div class="members__contact">
						<?php
							/**
							 * Get contact info
							 */
							if ( $repeater[0]['address'] ) {
								echo '<div>' . $repeater[0]['address'] . '</div>';
							}
							if ( $repeater[0]['phone_number'] ) {
								echo '<div>P: ' . $repeater[0]['phone_number'] . '</div>';
							}
						?>
					</div><!-- /.members__contact -->
				<?php endif; // acf_field( 'member_contact_info', false )

				/**
				 * Get description
				 */
				if ( acf_field( 'member_description', false ) ) :
					$str = acf_field( 'member_description', false );
					echo '<p>' . string_trim_words( $str, 20, '...' ) . '</p>';
				endif; // acf_field( 'member_description', false ) ?>

				<a href="#open-popup-<?php the_ID(); ?>" class="read-more js-open-popup">More info</a>

			</div><!-- /.members__info -->
		</div><!-- /.members -->

		<div id="open-popup-<?php the_ID(); ?>" class="members__popup magnific-popup mfp-hide magnific-animate">
			<div class="container">
				<div class="layout">

					<div class="layout__item large-and-up-1/2">
						<?php if ( has_post_thumbnail() ) :
							$src = get_featured_image_src( get_the_ID(), 'large' ); ?>
							<div class="members__image" style="background-image:url(<?php echo $src; ?>);">
								<?php house_featured_image( 'large', false ); ?>
							</div><!-- /.members__image -->
						<?php endif; // has_post_thumbnail() ?>
					</div><!-- /.layout__item -->

					<div class="layout__item large-and-up-1/2">
						<div class="members__info">
							<h2><?php the_title(); ?></h2>

							<?php if ( acf_field( 'member_contact_info', false ) ) :
								$repeater = acf_field( 'member_contact_info', false ); ?>
								<div class="members__contact">
									<?php
										/**
										 * Get contact info
										 */
										if ( $repeater[0]['address'] ) {
											echo '<div>' . $repeater[0]['address'] . '</div>';
										}
										if ( $repeater[0]['phone_number'] ) {
											echo '<div>P: ' . $repeater[0]['phone_number'] . '</div>';
										}
									?>
								</div><!-- /.members__contact -->
							<?php endif; // acf_field( 'member_contact_info', false )

							/**
							 * Get description
							 */
							acf_field( 'member_description', true, '<p>', '</p>' );

							/**
							 * Get member website link
							 */
							if ( acf_field( 'member_contact_info', false ) ) :
								$repeater = acf_field( 'member_contact_info', false );

								if ( $repeater[0]['website_url'] ) {
									/**
									 * Create link from url
									 * translators: url, class, target blank (true/false)
									 */
									house_website_link( $repeater[0]['website_url'] );
								}

							endif; // acf_field( 'member_contact_info', false ) ?>

						</div><!-- /.members__info -->
					</div><!-- /.layout__item -->

				</div><!-- /.layout -->
			</div><!-- /.container -->
		</div><!-- /.members__popup -->
	</div><!-- /.layout__item -->

	<?php endwhile; // $members->have_posts() ?>

</div><!-- /.layout -->
<?php endif; // $members->have_posts() ?>

<div class="text-center">
	<?php house_simple_content_pagination( 'pagination', $members, 'pagination' ); ?>
</div><!-- text-center -->

