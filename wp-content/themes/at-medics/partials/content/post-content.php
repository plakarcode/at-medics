<?php
/**
 * Content
 *
 * Template part for rendering content for posts and pages
 *
 * @package WordPress
 */

if ( is_search() ) :

	the_excerpt();

else :

	/* translators: %s: Name of current post */
	the_content( sprintf(
		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'house' ),
		get_the_title()
	) );

	wp_link_pages( array(
		'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'house' ) . '</span>',
		'after'       => '</div>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'house' ) . ' </span>%',
		'separator'   => '<span class="screen-reader-text">, </span>',
	) );

endif;