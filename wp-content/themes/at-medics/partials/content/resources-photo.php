<?php
/**
 * Resources Photo
 *
 * Template part for rendering photo for resorces
 *
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if ( have_rows( 'photo' ) ) : while ( have_rows( 'photo' ) ) : the_row();

	if ( acf_sub_field( 'photo', false ) ) : ?>
		<img src="<?php acf_sub_field( 'photo' ); ?>" alt="">
	<?php endif;

endwhile; endif; ?>
</article><!-- #post -->