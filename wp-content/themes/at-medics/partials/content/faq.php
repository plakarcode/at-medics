<?php
/**
 * FAQs
 *
 * Template part for rendering faqs accordion.
 *
 * @package WordPress
 */
if ( have_rows( 'faqs' ) ) : ?>
	<div class="accordion">

		<?php while ( have_rows( 'faqs' ) ) : the_row(); ?>

			<div class="accordion__item">
				<?php if ( acf_sub_field( 'question', false ) ) : ?>
					<h2 class="accordion__item-title">
						<?php acf_sub_field( 'question', true, '<a href="">', '</a>' ); ?>
						<span class="icon-arrow"><?php echo house_svg_icon( 'arrow-down' ); ?></span>
					</h2>
				<?php endif; // acf_sub_field( 'question', false )

				acf_sub_field( 'answer', true, '<div class="accordion__item-content">', '</div>' ); ?>
			</div><!-- accordion__item -->

		<?php endwhile; // have_rows( 'faqs' ) ?>
	</div><!-- accordion -->
<?php endif; // have_rows( 'faqs' )