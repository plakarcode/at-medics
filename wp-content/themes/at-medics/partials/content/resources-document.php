<?php
/**
 * Resources Document
 *
 * Template part for rendering document for resorces
 *
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if ( have_rows( 'document' ) ) : while ( have_rows( 'document' ) ) : the_row();

	if ( acf_sub_field( 'document', false ) ) : ?>
		<?php acf_sub_field( 'document', true, '<p>', '</p>' ); ?>
	<?php endif;

endwhile; endif; ?>
</article><!-- #post -->