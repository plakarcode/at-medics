<?php
/**
 * Archive Resources
 *
 * Template part for rendering archive listing for 'resources' post type
 *
 * @package WordPress
 */
/**
 * Declare tabs
 * @var array
 */
$tabs = []; ?>

<div class="tabs">
	<?php
		// get just one post so that we have access to 'content_type' field
		$r = get_resources_cpt(1);
		/**
		 * Loop for just one post
		 */
		if ( $r->have_posts() ) : while ( $r->have_posts() ) : $r->the_post();
			// get the object of field so that we have all choices
			$type = get_field_object( 'content_type' );
			// store choices array into our $tabs array
			$tabs = $type['choices'];
		endwhile; endif;
		wp_reset_query(); // always reset query

		/**
		 * Get the real resources query
		 * Translator: number of posts per page.
		 * @var obj|Error
		 */
		$resources = get_resources_cpt();

		if ( $resources->have_posts() ) :

			/**
			 * If we have $tabs
			 * there's probably no scenario in which we will have one post and no $tabs
			 */
			if ( $tabs ) : ?>
				<ul class="tabs__nav">
					<?php
						/**
						 * Use $tabs array to build tabs nav
						 * @var array
						 */
						foreach ( $tabs as $key => $value ) : ?>
						<li><a href="#tab-<?php echo $key; ?>"><?php echo house_pluralize( $value ); ?></a></li>
					<?php endforeach; // $tabs as $key => $value ?>
				</ul>

			<?php
				/**
				 * Use $tabs array to build tabs content
				 * @var array
				 */
				foreach ( $tabs as $key => $value ) : ?>
					<div id="tab-<?php echo $key; ?>" class="tabs__item">
						<div class="layout">

							<?php while ( $resources->have_posts() ) : $resources->the_post(); ?>
								<?php if ( get_field( 'content_type' ) ==  $key ) : ?>
										<div class="layout__item large-and-up-1/3">
											<?php get_template_part( 'partials/content/resources', $key ); ?>
										</div>
								<?php endif; ?>
							<?php endwhile; // $resources->have_posts() ?>

						</div><!-- /.layout -->
					</div><!-- /.tabs__item -->
				<?php endforeach; // $tabs as $key => $value
			endif; // $tabs
		endif; // $resources->have_posts() ?>

</div><!-- /.tabs -->

<div class="text-center">
	<?php house_simple_content_pagination( 'pagination', $resources, 'pagination' ); ?>
</div><!-- text-center -->

