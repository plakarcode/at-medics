<?php
/**
 * Blue heading
 *
 * Template part for rendering specific titles with blue background on pages
 *
 * @package WordPress
 */
the_title( '<div class="heading-blue"><h1>', '</h1></div>' );
