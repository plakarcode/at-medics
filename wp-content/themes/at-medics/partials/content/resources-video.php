<?php
/**
 * Resources Video
 *
 * Template part for rendering video for resorces
 *
 * @package WordPress
 */
if ( have_rows( 'video' ) ) : while ( have_rows( 'video' ) ) : the_row();
    if ( acf_sub_field( 'url', false ) ) : ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="video-object video-object--small">
                
                <?php 
                if ( acf_sub_field( 'image', false ) ) :
                    $image = acf_sub_field( 'image', false );
                    $src = $image['sizes']['intro']; 
                    $url = acf_sub_field( 'url', false ); ?>
                    <a href="<?php echo $url; ?>" class="video-popup" style="background-image:url(<?php echo $src; ?>);">
                        <img src="<?php echo $src; ?>" alt="video" />
                        <div class="video-object__play">
                            <?php echo house_svg_icon( 'play' ); ?>
                        </div>
                    </a>
                <?php else : 
                    $url = acf_sub_field( 'url', false ); ?>
                    <a href="<?php echo $url; ?>" class="video-popup" style="background-image:url(<?php echo set_video_image_src( acf_sub_field( 'url', false ) ); ?>);">
                        <img src="<?php echo set_video_image_src( acf_sub_field( 'url', false ) ); ?>" alt="video" />
                        <div class="video-object__play">
                            <?php echo house_svg_icon( 'play' ); ?>
                        </div>
                    </a>
                <?php endif; // acf_sub_field( 'image' ) ?>

                <?php acf_sub_field( 'title', true, '<p>', '</p>' ); ?>
                
            </div><!-- /.video-object video-object--small -->

        </article><!-- #post -->

    <?php endif; // acf_sub_field( 'url', false )
endwhile; endif; 