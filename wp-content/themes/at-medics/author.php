<?php
/**
 * Author
 *
 * The Template for displaying author single.
 *
 * @link https://codex.wordpress.org/Author_Templates
 *
 * @link https://support.advancedcustomfields.com/forums/topic/acf_form-on-user-profile/
 * @link https://www.advancedcustomfields.com/resources/acf_form/
 * @link https://www.advancedcustomfields.com/resources/create-a-front-end-form/
 * @link https://support.advancedcustomfields.com/forums/topic/using-acf_form-to-create-a-new-post/
 *
 * @package WordPress
 */
acf_form_head();
get_header(); ?>

<div class="container">

<?php
	/**
	 * This page can not be seen by guests
	 */
	if ( ! is_user_logged_in() ) {
		echo '<h3>Sorry, you have to be logged in to see this page</h3>';
		return;
	}
	/**
	 * Get profile's ID
	 * @var int
	 */
	$profile_id = get_profile_user_id();
	/**
	 * Get visitor's ID
	 * @var int
	 */
	$loggedin_id = get_loggedin_user_id();

	/**
	 * Display the data to all logged in users
	 */
	if ( have_rows( 'set_of_questions_for_user', 'user_' . $profile_id ) ) :
		while ( have_rows( 'set_of_questions_for_user', 'user_' . $profile_id ) ) : the_row();
			$question_1 = get_sub_field( 'question_1', 'user_' . $profile_id );
			$question_2 = get_sub_field( 'question_2', 'user_' . $profile_id );
			$question_3 = get_sub_field( 'question_3', 'user_' . $profile_id );

			if ( $question_1 ) {
				echo '<div>Answer 1: ' . $question_1 . '</div>';
			}
			if ( $question_2 ) {
				echo '<div>Answer 1: ' . $question_2 . '</div>';
			}
			if ( $question_3 ) {
				echo '<div>Answer 1: ' . $question_3 . '</div>';
			}

		endwhile; //  have_rows( 'set_of_questions_for_user', 'user_' . $profile_id )
	endif; // have_rows( 'set_of_questions_for_user', 'user_' . $profile_id )

	/**
	 * If currently logged in user can manage currently viewed profile user,
	 * or currently logged in user is the currently viewed user,
	 * display the form for managing the profile data.
	 * @var bool
	 */
	if ( loggedin_user_can_manage_profile() || $profile_id === $loggedin_id ) :
		/**
		 * Putting these inside the loop on archive page (author is an archive)
		 * apparently creates multiple forms.
		 * @var array
		 */
		$options = array(
			'post_id' => 'user_' . $profile_id,
			'field_groups' => array( 104 ), // this is the id of ACF group, just like post id
		);

		acf_form( $options );
	endif; // loggedin_user_can_manage_profile() || $profile_id === $loggedin_id
	?>

</div><!-- container -->
<?php get_footer();