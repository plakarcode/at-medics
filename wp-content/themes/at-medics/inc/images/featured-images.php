<?php
/**
 * Featured images
 *
 * Get default and theme defined image sizes for featured images.
 *
 * @package WordPress
 */
/**
 * House featured image
 *
 * Get post/page featured image. Optional set images size and
 * whether the image should link to post.
 * @link https://developer.wordpress.org/reference/functions/the_post_thumbnail/
 *
 * Uses Justin Tadlock's 'Get The Image' plugin.
 * @see inc/images/get-the-image.php
 *
 * @param  string  $size   Image size id, 'thumbnail', 'medium', 'large' etc
 * @param  boolean $link   Shoud image link to post or be just image
 * @param  boolean $echo   Return/echo the image
 * @return string          Returns/echoes image markup
 */
function house_featured_image( $size = 'full', $link = true, $echo = true ) {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( $size ), // array|string
		'featured'           => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'scan'               => false,
		'callback'           => null,
		'default'            => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes[$size] ) ? $size : $size,

		/* Format/display of image. */
		'link_to_post'       => $link, // just image or image in link
		'image_class'        => '',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.

		/* Return/echo image. */
		'echo'               => $echo,
	);

	return get_the_image( $args );
}
/**
 * Get featured image src
 *
 * Previous function house_featured_image() returns whole <img> tag.
 * Sometimes we need just src for the featured image.
 *
 * @param  int $post_id Post id
 * @param  string $size    Image size
 * @return string          Returns src for the featured image
 */
function get_featured_image_src( $post_id, $size = 'thumbnail' ) {
	$thumbnail_id = get_post_thumbnail_id( $post_id );

	if ( $thumbnail_id ) {
		$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, $size );
		$src = $thumbnail_url[0];
		return $src;
	}
}