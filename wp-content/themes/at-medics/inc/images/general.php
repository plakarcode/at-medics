<?php
/**
 * General images functions
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_filter( 'shortcode_atts_gallery', 'gallery_should_link_to_files', 10, 3 );
add_action( 'admin_init', 'content_image_should_not_be_link', 10 );
add_filter( 'post_thumbnail_html', 'remove_width_height_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_height_attribute', 10 );
add_filter( 'upload_mimes', 'house_mime_types' );
/**
 * Make all images in gallery shortcode link to file instead to
 * attachment page. This function is attached to
 * 'shortcode_atts_gallery' filter hook.
 *
 * Usable for lightbox js plugins
 *
 * @link http://wordpress.stackexchange.com/questions/115368/overide-gallery-default-link-to-settings#answer-183736
 */
function gallery_should_link_to_files( $out, $pairs, $atts ) {
	$atts = shortcode_atts( array(
		'link' => 'file'
	), $atts );

	$out['link'] = $atts['link'];
	return $out;
}
/**
 * Do not link content images
 *
 * By default, all images inserted in content link to somewhere. This shouldn't happen
 * unless we want it to. This function is attached to 'admin_init' action hook.
 *
 * @link http://www.wpbeginner.com/wp-tutorials/automatically-remove-default-image-links-wordpress/
 */
function content_image_should_not_be_link() {
	$image_set = get_option( 'image_default_link_type' );

	if ( $image_set !== 'none' ) {
		update_option( 'image_default_link_type', 'none' );
	}
}
/**
 * Remove width and height image attributes
 *
 * WordPress is adding width and height attributes to images by default. This is pain
 * for responsive sites. This function is attached to 'post_thumbnail_html' and 'image_send_to_editor'
 * filter hooks.
 *
 * @param  string $html Image markup for filtering
 * @return string       Filtered image markup
 * @link https://css-tricks.com/snippets/wordpress/remove-width-and-height-attributes-from-inserted-images/
 */
function remove_width_height_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
/**
 * Mime types
 *
 * Add .svg to mime types allowed to upload through media uploader.
 * This function is attached to 'upload_mimes' filter hook.
 *
 * @param  array $mimes Array of allowed mime types
 * @return array        Returnes filtered mime
 */
function house_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}
