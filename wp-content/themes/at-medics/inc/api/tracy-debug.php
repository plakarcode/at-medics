<?php
/**
 * Debugger
 *
 * Quickly detect and correct errors, log errors, dump variables, measure execution time of scripts/queries
 * and see memory consumption
 *
 * Uses third party script 'Tracy'.
 * @link https://packagist.org/packages/tracy/tracy
 * @link https://github.com/nette/tracy
 *
 * @package WordPress
 * @subpackage Tracy
 */

/**
 * These lines are mandatory.
 */
$file = get_template_directory() . '/inc/api/vendor/tracy/tracy/src/tracy.php';

/**
 * Let's not kill all if someone forgot to run composer,
 * or is frontend :P
 */
if ( ! file_exists( $file ) ) {
	return;
}

require_once( $file );

use Tracy\Debugger;

Debugger::enable();