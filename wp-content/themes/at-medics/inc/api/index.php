<?php
/**
 * Here include all external api related functions
 */
/**
 * Third party scripts and composer packages
 */
// detect mobile
include( get_template_directory() . '/inc/api/device-functions.php' );
// instagram API
include( get_template_directory() . '/inc/api/instagram-functions.php' );
// MailChimp API
include( get_template_directory() . '/inc/api/mailchimp-functions.php' );

/**
 * Debug only in localhost
 */
if ( substr( $_SERVER['REMOTE_ADDR'], 0, 4 ) == '127.' || $_SERVER['REMOTE_ADDR'] == '::1' ) {
    // code for localhost here
	include( get_template_directory() . '/inc/api/tracy-debug.php' );
}

/**
 * Video API and functions
 */
// youtube functions
include( get_template_directory() . '/inc/api/youtube-functions.php' );
// vimeo functions
include( get_template_directory() . '/inc/api/vimeo-functions.php' );
// video functions
include( get_template_directory() . '/inc/api/video-functions.php' );