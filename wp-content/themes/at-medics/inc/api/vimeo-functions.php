<?php
/**
 * Vimeo API functions
 *
 * @package WordPress
 */
/**
 * Get vimeo video id
 *
 * @param  string $url 		Vimeo video url
 * @return string      		Returns video id
 * @link http://stackoverflow.com/questions/10488943/easy-way-to-get-vimeo-id-from-a-vimeo-url#answer-29860052
 */
function get_vimeo_video_id( $url ) {
	$vimeo = $url;
	$vimeo_arrays = preg_match( "/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $vimeo, $output_array );

	if ( $output_array ) {
		return $output_array[5];
	}
}

/**
 * Get vimeo video data
 *
 * Makes an API request and get the video data based on video id.
 *
 * @link http://stackoverflow.com/questions/1361149/get-img-thumbnails-from-vimeo
 * @param  string $url  Video url
 * @return array  Returns array of video data
*/
function get_vimeo_video_data( $url ) {
	$id = get_vimeo_video_id( $url );
    $request = file_get_contents( 'http://vimeo.com/api/v2/video/' . $id . '.json');
    $data = json_decode( $request );

    return $data;
}
/**
 * Get vimeo video image
 *
 * Get the video thumbnail from vimeo API.
 *
 * @param  string $url  Video url
 * @param  string $size Image size, 'small', 'medium' and 'large' are valid values.
 * @return string       Returns image url
 */
function get_vimeo_image( $url, $size = 'large' ) {
	/**
	 * Get video data
	 * @var array
	 */
	$data = get_vimeo_video_data( $url );
	/**
	 * Set the size
	 * @var string
	 */
	$image = 'thumbnail_' . $size;
	/**
	 * Get the url
	 * @var string
	 */
	$src = $data[0]->$image;

	return $src;
}
/**
 * Vimeo video image
 *
 * Echo video thumbnail from vimeo API.
 *
 * @param  string $url  Video url
 * @param  string $size Image size, 'small', 'medium' and 'large' are valid values.
 * @return string       Echoes image url
 */
function vimeo_image( $url, $size = 'large' ) {
	echo get_vimeo_image( $url, $size );
}