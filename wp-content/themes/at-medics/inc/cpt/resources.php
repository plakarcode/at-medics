<?php
/**
 * Resources Custom Post Type
 *
 * Resources cpt related functionality.
 *
 * @package WordPress
 */

/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'thumbnail'
);

/**
 * Standard arguments for registering post type
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$args = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => $supports,
);

/**
 * Create the custm post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'resource', $args );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */

// $cpt->menu_icon( 'dashicons-awards' );

/**
 * Get resources
 *
 * Prepare query for getting resourcess posts.
 *
 * @param  integer $posts_per_page Number of posts to return
 * @return obj|Error   Returns query object or error
 */
function get_resources_cpt( $posts_per_page = -1 ) {
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
		'post_type'      => 'resource',
		'posts_per_page' => $posts_per_page,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'paged'          => $paged,
	);
	$query = new WP_Query( $args );

	return $query;
}
