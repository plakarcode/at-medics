<?php
/**
 * Here include all custom post type related functions
 */
// Members cpt related functionality.
include( get_template_directory() . '/inc/cpt/members.php' );
// Resources cpt related functionality.
include( get_template_directory() . '/inc/cpt/resources.php' );

