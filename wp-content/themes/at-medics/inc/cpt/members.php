<?php
/**
 * Members Custom Post Type
 *
 * Members cpt related functionality.
 *
 * @package WordPress
 */
/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'thumbnail'
);
/**
 * Standard arguments for registering post type
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$args = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => $supports,
);
/**
 * Create the custm post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'member', $args );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-awards' );
/**
 * Dashboard posts listing columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#columns
 */
$cpt->columns( array(
	'cb'       => '<input type="checkbox" />',
	'title'    => __( 'Title', 'house' ),
	'date'     => __( 'Date', 'house' ),
	'website'  => __( 'Website', 'house' ),
	'address'  => __( 'Address', 'house' ),
	'phone'    => __( 'Phone number', 'house' ),
	'featured' => __( 'Featured Image', 'house' ),
));
/**
 * Populate custom columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#populating-columns
 */
// website columns
$cpt->populate_column( 'website', function( $column, $post ) {

	if ( get_field( 'member_contact_info' ) ) :
		$repeater = get_field( 'member_contact_info' );

		if ( $repeater[0]['website_url'] ) {
			/**
			 * Create link from url
			 * translators: url, class, target blank (true/false)
			 */
			house_website_link( $repeater[0]['website_url'] );
		}

	endif; // get_field( 'member_contact_info' )
});
// address column
$cpt->populate_column( 'address', function( $column, $post ) {

	if ( get_field( 'member_contact_info' ) ) :
		$repeater = get_field( 'member_contact_info' );

		if ( $repeater[0]['address'] ) {
			echo $repeater[0]['address'];
		}

	endif; // get_field( 'member_contact_info' )
});
// phone column
$cpt->populate_column( 'phone', function( $column, $post ) {

	if ( get_field( 'member_contact_info' ) ) :
		$repeater = get_field( 'member_contact_info' );

		if ( $repeater[0]['phone_number'] ) {
			echo $repeater[0]['phone_number'];
		}

	endif; // get_field( 'member_contact_info' )
});
// featured image column
// default 'icon' column is using 'thumbnail' size which is
// too big
$cpt->populate_column( 'featured', function( $column, $post ) {
	if ( has_post_thumbnail( $post ) ) {
		$src = get_featured_image_src( $post, 'featured-preview' );
		echo '<img src="' . $src . '" />';
	}
});
/**
 * Get members
 *
 * Prepare query for getting members posts.
 *
 * @param  integer $posts_per_page Number of posts to return
 * @return obj|Error   Returns query object or error
 */
function get_members( $posts_per_page = -1 ) {
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
		'post_type'      => 'member',
		'posts_per_page' => $posts_per_page,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'paged'          => $paged,
	);
	$query = new WP_Query( $args );

	return $query;
}