<?php
/**
 * Custom functions not related to anything specific
 *
 * @package  WordPress
 */
/**
 * Admin menu Chrome fix
 *
 * Dashboard menu gets broken on hover in Chrome.
 *
 * @link https://core.trac.wordpress.org/ticket/33199
 * @link http://wordpress.stackexchange.com/questions/200096/admin-sidebar-items-overlapping-in-admin-panel
 */
add_action( 'admin_enqueue_scripts', 'chrome_fix' );

function chrome_fix() {

	if ( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], 'Chrome' ) !== false ) {
		wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0) }' );
	}
}
/**
 * Define irregular plurals
 *
 * @return array Array of irregular plurals
 */
function irregular_plurals() {

	$irregulars = array(
		'man'           => 'men',
		'woman'         => 'women',
		'fungus'        => 'fungi',
		'thief'         => 'thieves',
		'medium'        => 'media',
		'person'        => 'people',
		'echo'          => 'echoes',
		'hero'          => 'heroes',
		'potato'        => 'potatoes',
		'veto'          => 'vetoes',
		'auto'          => 'autos',
		'memo'          => 'memos',
		'pimento'       => 'pimentos',
		'pro'           => 'pros',
		'knife'         => 'knives',
		'leaf'          => 'leaves',
		'bus'           => 'busses',
		'child'         => 'children',
		'quiz'          => 'quizzes',
		# words whose singular and plural forms are the same
		'equipment'     => 'equipment',
		'fish'          => 'fish',
		'information'   => 'information',
		'money'         => 'money',
		'moose'         => 'moose',
		'news'          => 'news',
		'rice'          => 'rice',
		'series'        => 'series',
		'sheep'         => 'sheep',
		'species'       => 'species'
	);

	return $irregulars;
}

/**
 * Pluralize string
 *
 * Check if string is irregular and, relevant to its ending,
 * create plural form.
 *
 * @param  string $string Singular that needs to be pluralized
 * @return string         Returns pluralized sting
 */
function house_pluralize( $string ) {

	$irregulars = irregular_plurals();

	$es = array( 's', 'z', 'ch', 'sh', 'x' );

	$last_letter = substr( $string, -1 );

	if ( array_key_exists( $string, $irregulars ) ) {

		return $irregulars[$string];

	} elseif ( $last_letter == 'y' ) {

		if ( substr( $string, -2 ) == 'ey' ) {

			return substr_replace( $string, 'ies', -2, 2 );

		} else {

			return substr_replace( $string, 'ies', -1, 1 );

		}

	} elseif ( in_array( substr( $string, -2 ), $es ) || in_array( substr( $string, -1 ), $es ) ) {

		return $string . 'es';

	} else {

		return $string . 's';

	}
}
/**
 * Get website link
 *
 * Get the url and build link with domain name (host) as link label. Used mostly
 * for displaying website's links (for partners, team members etc).
 *
 * @uses FILTER_VALIDATE_URL
 * @link http://php.net/manual/en/filter.filters.validate.php
 *
 * @param  string  $url   URL
 * @param  string  $class Class for the link
 * @param  boolean $blank Should link be opened in new tab or not
 * @return string         Returns either link markup or error message if url not valid
 */
function get_house_website_link( $url, $class = '', $blank = true ) {
	// check if url is valid
	if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
		return sprintf( __( '%s is not a valid URL', 'house' ), $url );
	} else {
		$parse = parse_url( $url );
		$website = $parse['host'];

		// check if there's a class
		if ( ! empty( $class ) ) {
			$class = ' class="' . $class . '"';
		} else {
			$class = '';
		}

		// open link in new tab or not?
		if ( $blank === true ) {
			$target = 'target="_blank"';
		} else {
			$target = '';
		}

		return '<a href="' . $url . '" ' . $target . $class . '>' . $website . '</a>';
	}
}
/**
 * Website link
 *
 * Echoes get_house_website_link().
 *
 * @param  string  $url   URL
 * @param  string  $class Class for the link
 * @param  boolean $blank Should link be opened in new tab or not
 * @return string         Echoes either link markup or error message if url not valid
 */
function house_website_link( $url, $class = '', $blank = true ) {
	echo get_house_website_link( $url, $class, $blank );
}