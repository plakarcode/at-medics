<?php
/**
 * Filter post format
 *
 * We are using built in taxonomy 'post_format' for all our posts. Tweets are 'status',
 * Instagram 'gallery' etc. However, posts screen in dashboard gets cluttered with tweets
 * as they come in more often than blog posts. Filtering posts by format is easy way for
 * reducing clutter. Problem occurs with 'standard' format, which actually means that
 * posts are NOT in 'post_format' taxonomy.
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'pre_get_posts', 'house_query_format_standard' );
add_action( 'restrict_manage_posts', 'house_add_taxonomy_filters' );
/**
 * Status Format Query
 *
 * Get posts without 'post_format' taxonomy, set fake 'standard' term for these
 * and hook it before the actual query happens. This function is attached to
 * 'pre_get_posts' action hook.
 *
 * @param  obj $query WP Query
 * @return void
 * @link http://alexking.org/blog/2012/01/05/wp_query-by-standard-post-format
 */
function house_query_format_standard( $query ) {

	if ( isset( $query->query_vars['post_format'] ) && $query->query_vars['post_format'] == 'post-format-standard' ) {
		if ( ( $post_formats = get_theme_support( 'post-formats' ) ) && is_array( $post_formats[0] ) && count( $post_formats[0] ) ) {
			$terms = array();

			foreach ( $post_formats[0] as $format ) {
				$terms[] = 'post-format-' . $format;
			}

			$query->is_tax = null;
			unset( $query->query_vars['post_format'] );
			unset( $query->query_vars['taxonomy'] );
			unset( $query->query_vars['term'] );
			unset( $query->query['post_format'] );

			$query->set( 'tax_query', array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'post_format',
					'terms'    => $terms,
					'field'    => 'slug',
					'operator' => 'NOT IN'
				)
			));
		} // $post_formats = get_theme_support( 'post-formats' ) )
	} // isset( $query->query_vars['post_format'] )
}
/**
 * Create custom filter
 *
 * Create custom filter for posts on edit.php screen for 'post_format' taxonomy. This function
 * is attached to 'restrict_manage_posts' action hook.
 *
 * @link https://pippinsplugins.com/post-list-filters-for-custom-taxonomies-in-manage-posts/
 * @return string Returns filter markup
 */
function house_add_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'post_format' );

	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'post' ) {

		foreach ( $taxonomies as $tax_slug ) {

			$tax_obj = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms( $tax_slug );
			$standard = 'post-format-standard';

			if ( count( $terms ) > 0 ) {

				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";

					echo '<option value="">All ' . $tax_name . 's</option>';

					if ( ! empty( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $standard ) {
						echo '<option value='. $standard . ' selected="selected">Standard posts</option>';
					} else {
						echo '<option value='. $standard . '>Standard posts</option>';
					}

					foreach ( $terms as $term ) {
						if ( $term->slug === 'post-format-aside' ) :
							$label = __( 'Aside posts', 'house' );
						elseif ( $term->slug === 'post-format-image' ) :
							$label = __( 'Image posts', 'house' );
						elseif ( $term->slug === 'post-format-video' ) :
							$label = __( 'Video posts', 'house' );
						elseif ( $term->slug === 'post-format-quote' ) :
							$label = __( 'Quote posts', 'house' );
						elseif ( $term->slug === 'post-format-link' ) :
							$label = __( 'Link posts', 'house' );
						elseif ( $term->slug === 'post-format-gallery' ) :
							$label = __( 'Gallery posts', 'house' );
						elseif ( $term->slug === 'post-format-status' ) :
							$label = __( 'Status posts', 'house' );
						elseif ( $term->slug === 'post-format-audio' ) :
							$label = __( 'Audio posts', 'house' );
						elseif ( $term->slug === 'post-format-chat' ) :
							$label = __( 'Chat posts', 'house' );
						else :
							$label = $term->name;
						endif;

						if ( ! empty( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) {
							$selected = 'selected="selected"';
						} else {
							$selected = '';
						}
						echo '<option value='. $term->slug . ' ' . $selected . '>' . $label .' (' . $term->count .')</option>';
					} // foreach ( $terms as $term )

				echo "</select>";
			} // if ( count( $terms ) > 0 )
		} // foreach ( $taxonomies as $tax_slug )
	} // if ( $typenow == 'post' )
}

