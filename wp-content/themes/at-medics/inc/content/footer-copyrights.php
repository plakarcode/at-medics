<?php
/**
 * Footer copyrights functions
 *
 * Get First Post Date
 * Render copyrights
 *
 * @package WordPress
 */

/**
 * Get First Post Date
 *
 * @param  $format Type of date format to return, using PHP date standard, default Y
 * @return Date of first post
 * @link http://alex.leonard.ie/2010/07/27/wordpress-tip-get-the-date-of-your-first-post/
 */
function house_first_post_date( $format = "Y" ) {
	// Setup get_posts arguments
	$args = array(
		'numberposts' => 1,
		'post_status' => 'publish',
		'order' => 'ASC'
	);

	// Get all posts in order of first to last
	$get_all = get_posts( $args );

	// Extract first post from array
	$first_post = $get_all[0];

	// Assign first post date to var
	$first_post_date = $first_post->post_date;

	// return date in required format
	$output = date( $format, strtotime( $first_post_date ) );

	return $output;
}

/**
 * Render copyrights
 *
 * Check if current year is the same as the year of the first published post and render copyrights
 * range accordingly. This function uses function_exists() call for easier overriding in child themes.
 *
 * @return string
 */
function get_house_footer_copyrights() {
	global $globalSite;
	$first_year 	= house_first_post_date();
	$current_year 	= date( 'Y' );

	$output = '';
	// $output .= '&copy; ';
	$output .= 'Copyright ';

	if ( $first_year == $current_year ) {
		$output .= $current_year . ' ';
	}
	else {
		$output .= $first_year . '-' . $current_year . ' ';
	}

	$output .= $globalSite['name'];

	return $output;
}
/**
 * Footer copyrights
 * @return string Echoes footer copyrights
 */
function house_footer_copyrights() {
	echo get_house_footer_copyrights();
}