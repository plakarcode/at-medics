<?php
/**
 * Ajax posts loading functions
 *
 * Ajax load more posts functionality. Uses House Buzz plugin.
 *
 * @package WordPress
 * @subpackage House Buzz
 */
// post queries
include( get_template_directory() . '/inc/content/ajax/post-queries.php' );
// load scripts
include( get_template_directory() . '/inc/content/ajax/scripts.php' );