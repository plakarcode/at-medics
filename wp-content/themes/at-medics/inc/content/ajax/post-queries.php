<?php
/**
 * Post queries for ajax loading
 *
 * Ajax load more posts functionality. Uses House Buzz plugin.
 *
 * @package WordPress
 * @subpackage House Buzz
 */
/**
 * Hooks
 */
add_action( 'wp_ajax_nopriv_ajax_load_posts', 'ajax_load_posts' );
add_action( 'wp_ajax_ajax_load_posts', 'ajax_load_posts' );
/**
 * Prepare AJAX query args
 *
 * We need to filter Buzz plugin's query args and add
 * stuff needed for the ajax.
 *
 * @param  array $args   Query args
 * @return array         Returns filtered query args
 */
function get_ajax_query_args( $args ) {
	global $globalSite;

	$all = get_all_posts_ids( 'twitter_data_wrap', 'option' );

	$posts_num = $globalSite['posts_per_page'];
	$ppp = ( isset( $_POST["ppp"] ) ) ? $_POST["ppp"] : $posts_num;
	$page = ( isset( $_POST['pageNumber'] ) ) ? $_POST['pageNumber'] : 0;

	// header( "Content-Type: text/html" );

	$args['posts_per_page'] = $ppp;
	$args['paged'] = $page;
	$args['post__in'] = $all;

	return $args;
}
/**
 * Get Ajax query
 *
 * Get prepared query args and run the query.
 * @see  get_ajax_query_args()
 *
 * @return obj|error Returns wp_query object
 */
function get_ajax_query() {
	global $wp_query;
	/**
	 * Fire filter for modifying query
	 * @see get_ajax_query_args()
	 */
	add_filter( 'buzz_posts_query', 'get_ajax_query_args' );
	/**
	 * Get the query
	 * @var obj
	 */
	if ( function_exists( 'get_all_posts_query' ) ) {
		$query = get_all_posts_query( 'twitter_data_wrap', 'option' );
	} else {
		$query = $wp_query;
	}

	return $query;

	/**
	 * Remove filter for modifying query
	 * so that it doesn't effect other queries
	 *
	 * @see get_ajax_query_args()
	 */
	remove_filter( 'buzz_posts_query', 'get_ajax_query_args' );
}
/**
 * AJAX action handle
 * @return mix Returns posts
 */
function ajax_load_posts() {
	/**
	 * Fire filter for modifying query
	 * @see get_ajax_query_args()
	 */
	add_filter( 'buzz_posts_query', 'get_ajax_query_args' );
	/**
	 * Get the query
	 * @var obj
	 */
	$query = get_all_posts_query( 'twitter_data_wrap', 'option' );

	$out = '';

	if ( $query->have_posts() ) :  while ( $query->have_posts() ) : $query->the_post();

		get_template_part( 'content', 'masonry' );

	endwhile; endif;

	/**
	 * Always reset custom queries
	 */
	wp_reset_postdata();

	die( $out );
	/**
	 * Remove filter for modifying query
	 * so that it doesn't effect other queries
	 *
	 * @see get_ajax_query_args()
	 */
	remove_filter( 'buzz_posts_query', 'get_ajax_query_args' );
}
/**
 * Set new globals needed for use in javascript part of ajax
 */
global $globalSite;

	/**
	 * Get the prepared query and store total page number in globals
	 */
	$query = get_ajax_query();
	$globalSite['ajaxpages'] = $query->max_num_pages;

	/**
	 * Store ajax action handle in globals
	 */
	$globalSite['ajax_action'] = 'ajax_load_posts';