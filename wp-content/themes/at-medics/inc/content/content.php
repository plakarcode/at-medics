<?php
/**
 * Custom modifications of the the_content()
 *
 * @package WordPress
 */

/**
 * String trim characters
 *
 * When we need limited number of characters for a string (i.e excerpt )
 *
 * @link http://stackoverflow.com/questions/12423407/limiting-characters-retrived-from-a-database-field#answer-12423453
 *
 * @param  string 	$str    		String to be trimmed
 * @param  integer 	$length 		Number of characters
 * @return string         			Trimmed string
 */
function string_trim_characters( $str, $length, $after = '...' ) {

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = substr( $str, 0, strpos( $str, ' ', $length ) ) . ' ' . $after;
	}

	return $str;
}
/**
 * String trim words
 *
 * @param  string $str      String to be trimmed
 * @param  int $length 		Number of words
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed  string
 */
function string_trim_words( $str = '', $length, $after = '...' ) {

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = wp_trim_words( $str, $length, $after );
	}

	return $str;
}

/**
 * Content trim characters
 *
 * Trim content down to exact number of characters.
 *
 * @uses string_trim_characters()
 *
 * @param  int $length 		Number of characters
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_characters( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = string_trim_characters( $str, $length, $after );
	}

	return $str;
}
/**
 * Content trim words
 *
 * Trim content down to exact number of words.
 *
 * @param  int $length 		Number of words
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_words( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = wp_trim_words( $str, $length, $after );
	}

	return $str;
}

function upload_user_file( $file = array() ) {
	require_once( ABSPATH . 'wp-admin/includes/admin.php' );
      $file_return = wp_handle_upload( $file, array('test_form' => false ) );
      if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
          return false;
      } else {
          $filename = $file_return['file'];
          $attachment = array(
              'post_mime_type' => $file_return['type'],
              'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
              'post_content' => '',
              'post_status' => 'inherit',
              'guid' => $file_return['url']
          );
          $attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
          require_once(ABSPATH . 'wp-admin/includes/image.php');
          $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
          wp_update_attachment_metadata( $attachment_id, $attachment_data );
          if( 0 < intval( $attachment_id ) ) {
          	return $attachment_id;
          }
      }
      return false;
}