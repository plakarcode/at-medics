<?php
/**
 * Documents related functions
 *
 * @package WordPress
 */
/**
 * Spreadsheet documents
 *
 * Set the array of spreadsheet documents extensions and mime types.
 *
 * @link https://gist.github.com/davidmottershead/3da317ea435eade439a7
 * @return array Returns array of spreadsheet documents extensions and mime types
 */
function house_spreadsheet_documents() {
	$array = [
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
		'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
		'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
		'xltm' => 'application/vnd.ms-excel.template.macroEnabled.12',
		'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
		'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
	];

	return $array;
}

/**
 * Word documents
 *
 * Set the array of word documents extensions and mime types.
 *
 * @link https://gist.github.com/davidmottershead/3da317ea435eade439a7
 * @return array Returns array of word documents extensions and mime types
 */
function house_word_documents() {
	$array = [
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
		'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
		'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
		'doc'  => 'application/msword',
		'odt'  => 'application/vnd.oasis.opendocument.text',
	];

	return $array;
}
/**
 * Get file mime type
 *
 * Get the array with file' info (provided by ACF)
 * and check it's type and mime type.
 *
 * @param  array  $array Array with file info
 * @return string        Returns file' mime type
 */
function house_get_file_mime( $array = array() ) {
	$mime = '';
	if ( $array ) {
		if ( $array['type'] === 'application' ) {
			$mime = $array['mime_type'];
		}
	}
	return $mime;
}
/**
 * Set file icon
 *
 * Get the array with file' info (provided by ACF)
 * and set icon according to mime. We are particularly
 * watching for word and spreadsheet documents.
 *
 * @param  array  $array Array with file info
 * @return string        Returns icon markup
 */
function house_set_file_icon( $array = array() ) {
	$icon = '';
	/**
	 * Get word documents array
	 * @var array
	 */
	$words = house_word_documents();
	/**
	 * Get spreadsheet documents array
	 * @var array
	 */
	$spreadsheets = house_spreadsheet_documents();
	/**
	 * Get the file's mime type
	 * @var string
	 */
	$mime = house_get_file_mime( $array );
	/**
	 * Perform the check
	 */
	if ( in_array( $mime, $words ) ) {
		$icon = house_image( 'icon-docs.svg', 'icon-file' );
	} elseif ( in_array( $mime, $spreadsheets ) ) {
		$icon = house_image( 'icon-xls.svg', 'icon-file' );
	}

	return $icon;
}
/**
 * Icon for the file
 * @param  array  $array Array with file info
 * @return string        Echoes icon markup.
 */
function house_file_icon( $array = array() ) {
	echo house_set_file_icon( $array );
}