<?php
/**
 * User related forms
 *
 * @package WordPress
 */

/**
 * Select Usergroup
 *
 * We are displaying different select form depending on user's
 * permissions.
 *
 * @param  array  $allowed Array of allowed usergroups' slugs
 * @return string          Returns the select form markup
 */
function house_select_usergroup( $allowed = array() ) {
	global $wp_roles;
	$output = '';

	if ( $allowed ) :
		$output .= '<form class="select-user-group" method="GET">';
		$output .= '<select name="usergroup">';

		if ( ! isset( $_GET['usergroup'] ) ) :
			$output .= '<option selected="selected" value="">Select group</option>';
		// else :
			// $output .= '<option value="none">No group</option>';
		endif; // isset( $_GET['usergroup'] )

		foreach ( $allowed as $a ) :
			$value = $a;
			$display = $wp_roles->roles[$a]['name'];

			if ( isset( $_GET['usergroup'] ) && $_GET['usergroup'] === $value ) {
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}

			$output .= '<option value="' . $value . '"' . $selected . '>' . $display . '</option>';

		endforeach; // $allowed as $a

		$output .= '</select>';
		$output .= '<input type="submit" class="btn btn--primary" value="Show list">';
		$output .= '</form>';

	endif; // $allowed

	return $output;
}
/**
 * Search User
 *
 * Form for searching users.
 *
 * @return string Returns search form markup
 */
function house_search_user() {
	$output = '';

	if ( isset( $_GET['user'] ) ) {
		$value = $_GET['user'];
	} else {
		$value = '';
	}

	$output .= '<form class="select-user-group" method="GET">';
	$output .= '<input type="text" value="' . $value . '" name="user" id="user" placeholder="Search members"  />';
	$output .= '</form>';

	return $output;
}
