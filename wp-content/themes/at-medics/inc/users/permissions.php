<?php
/**
 * Permissions
 *
 * Helper functions concerning currently logged in user and currently
 * viewed user profile.
 *
 * @package WordPress
 */

/**
 * Get logged in user id
 *
 * Can be used on any page.
 *
 * @return int Returns id for currently logged in user
 */
function get_loggedin_user_id() {
	$loggedin = wp_get_current_user();
	return $loggedin->ID;
}
/**
 * Get loggedin user role
 *
 * Can be used on any page.
 *
 * @return string Returns role for currently logged in user
 */
function get_loggedin_user_role() {
	$loggedin_id = get_loggedin_user_id();
	$loggedin_role = get_user_role( $loggedin_id );
	return $loggedin_role;
}
/**
 * Logged in user can manage roles
 *
 * Get all roles managable by currently logged in user.
 * Can be used on any page.
 *
 * @return array Returns array of roles
 */
function loggedin_user_can_manage_roles() {
	$loggedin_role = get_loggedin_user_role();
	$array = role_can_manage( $loggedin_role );
	return $array;
}
/**
 * Logged in user can manage user
 *
 * Checks if currently logged in user can manage other user
 * based on id of that other user. Can be used on any page.
 *
 * @param  int $user_id    ID for user to be managed
 * @return bool            Returns true or false
 */
function loggedin_user_can_manage_user( $user_id ) {
	$array = loggedin_user_can_manage_roles();
	$user_role = get_user_role( $user_id );

	if ( in_array( $user_role, $array ) ) {
		return true;
	} else {
		return false;
	}
}
/**
 * Get profile user id
 *
 * Get the id for currently viewed user profile.
 * Can be used only in author.php template.
 *
 * @return int Returns profile user id
 */
function get_profile_user_id() {
	global $wp_query;
	/**
	 * Get the user whose profile we are looking at
	 * @var obj
	 */
	$profile = $wp_query->get_queried_object();
	return $profile->ID;
}
/**
 * Get profile user role
 *
 * Get the role for currently viewed user profile.
 * Can be used only in author.php template.
 *
 * @return string Returns profile user role
 */
function get_profile_user_role() {
	$profile_id = get_profile_user_id();
	$profile_role = get_user_role( $profile_id );
	return $profile_role;
}
/**
 * Logged in user can manage profile
 *
 * Checks if currently loggedin user can manage currently viewed user profile.
 * Can be used only in author.php template.
 *
 * @return bool Returns true or false
 */
function loggedin_user_can_manage_profile() {
	$user_id = get_profile_user_id();
	if ( loggedin_user_can_manage_user( $user_id ) ) {
		return true;
	} else {
		return false;
	}
}