<?php
/**
 * Here include all users related functions
 */
// custom roles functions
include( get_template_directory() . '/inc/users/roles.php' );
// Capabilities helper functions
include( get_template_directory() . '/inc/users/capabilities.php' );
// Permissions helper functions
include( get_template_directory() . '/inc/users/permissions.php' );
// User related forms
include( get_template_directory() . '/inc/users/forms.php' );
// registration-login forms
include( get_template_directory() . '/inc/users/forms-login-register.php' );