<?php
/**
 * Capabilities helper functions
 *
 * @link https://codex.wordpress.org/Class_Reference/WP_User
 * @link http://wordpress.stackexchange.com/a/188889
 * @link http://mannieschumpert.com/blog/wordpress-capabilities-magic-with-map_meta_cap/
 * @link https://gist.github.com/mannieschumpert/8886289
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/editable_roles
 *
 * @package WordPress
 */
if ( house_is_plugin_active( 'acf-role-selector-field/acf-role_selector.php' ) ) {
	add_filter( 'acfrsf/allowed_roles', 'remove_builtin_from_role_selector', 10, 2 );
}
/**
 * Remove builtin roles from role selector
 *
 * Remove WordPress' builtin roles from role selector plugin allowed roles. We want
 * to use this plugin only with custom roles for specific user management system.
 * This function is attached to 'acfrsf/allowed_roles' filter hook.
 *
 * @link https://github.com/danielpataki/ACF-Role-Selector#for-developers
 *
 * @param  array $roles   Array of roles
 * @param  array $field   Field details
 * @return array          Returns filtered array of allowed roles
 */
function remove_builtin_from_role_selector( $roles, $field ) {
	unset( $roles['administrator'] );
	unset( $roles['editor'] );
	unset( $roles['author'] );
	unset( $roles['contributor'] );
	unset( $roles['subscriber'] );
	return $roles;
}
/**
 * No one can delete users
 *
 * Except for administrators :D
 *
 * @param array  $required_caps   Returns the user's actual capabilities.
 * @param string $cap             Capability name.
 * @return array                  Returns filtered array of capabilities
 */
function no_one_delete_users( $required_caps, $cap ) {
	if ( 'administrator' != get_loggedin_user_role() ) {
		if ( 'delete_user' == $cap || 'delete_users' == $cap ) {
			$required_caps[] = 'do_not_allow';
		}
	}

	return $required_caps;
}
add_filter( 'map_meta_cap', 'no_one_delete_users', 10, 2 );
/**
 * All existing roles, no matter if they have users or not
 * @return array Returns array of all roles slugs
 */
function all_existing_roles() {
	global $wp_roles;
	$roles = $wp_roles->roles;
	$array = array();
	foreach ( $roles as $role => $val ) {
		$array[] = $role;
	}
	return $array;
}
/**
 * House all roles
 *
 * We are taking all roles on website with users and
 * storing their slugs into custom array.
 *
 * @return array Returns the array of role slugs
 */
function house_all_roles() {
	/**
	 * All roles with users
	 * @var array
	 */
	$all = count_users();
	$avail = $all['avail_roles'];

	$roles = [];

	foreach ( $avail as $key => $val ) {
		if ( $key != 'none' ) {
			$roles[] = $key;
		}
	}

	return $roles;
}

/**
 * Helper function get getting roles that the user is allowed to create/edit/delete.
 *
 * @param   WP_User $user
 * @return  array
 */
function user_get_allowed_roles() {

	$allowed = array();

	if ( get_field( 'set_user_permissions', 'option' ) ) :
		$all = get_field( 'set_user_permissions', 'option' );

		foreach ( $all as $each ) :
			$manager = $each['usergroup_with_permissions'][0];
			$users = array_filter( $each['managed_usergroups'] );

			$managed = array();

			foreach ( $users as $u ) :
				$managed[] = $u;
			endforeach; // $users as $u

			$allowed[$manager] = $managed;

		endforeach; // $all as $each

	endif; // get_field( 'set_user_permissions', 'option' )

	return $allowed;
}
/**
 * Get all managers
 *
 * Get all roles which have custom permissions to manage other roles.
 *
 * @return array Returns array of user manager roles
 */
function get_all_managers() {

	$manager = array();

	if ( get_field( 'set_user_permissions', 'option' ) ) :
		$all = get_field( 'set_user_permissions', 'option' );

		foreach ( $all as $each ) :
			$manager[] = $each['usergroup_with_permissions'][0];
		endforeach; // $all as $each

	endif; // get_field( 'set_user_permissions', 'option' )

	return $manager;
}
/**
 * Remove higher level role
 *
 * Remove higher level roles from roles dropdown and
 * prevent from user with 'edit_users' capability to
 * assign to other user role that is higher than the moderator's.
 *
 * @param  array $all_roles  Array of all roles
 * @return array             Returns filtered array
 */
function remove_higher_levels( $all_roles ) {
	$user = wp_get_current_user();
	$next_level = 'level_' . ( $user->user_level + 1 );

	/**
	 * Get the role for currently logged in user
	 * @var string
	 */
	$loggedin_role = get_loggedin_user_role();
	/**
	 * Get all roles with custom permissions to manage other roles
	 * @var array
	 */
	$managers = get_all_managers();
	/**
	 * If currently logged in user is administrator $allowed are all existing roles, custom and built in;
	 * if currently logged in user has custom permissions with allowed roles, then those roles are $allowed
	 * @var bool
	 */
	if ( $loggedin_role === 'administrator' ) {
		$allowed = all_existing_roles();
	} elseif ( in_array( $loggedin_role, $managers ) ) {
		$allowed = loggedin_user_can_manage_roles();
	}

	foreach ( $all_roles as $role => $caps ) {
		/**
		 * If currently logged in user is 'administrator' or one of roles with custom permissions
		 * let them move other users through their $allowed array
		 * @var bool
		 */
		if ( $loggedin_role === 'administrator' || in_array( $loggedin_role, $managers ) ) {
			if ( ! in_array( $role, $allowed ) ) {
				unset( $all_roles[ $role ] );
			}
		}
		/**
		 * For all other users who have higher level roles in their capabilities,
		 * remove that higher level and let them assign other users only to roles that are
		 * equal or lower level than role of user who is performing the edit.
		 */
		elseif ( isset( $caps['capabilities'][$next_level] ) ) {
			unset( $all_roles[$role] );
		}
	}

	return $all_roles;
}
add_filter( 'editable_roles', 'remove_higher_levels' );
/**
 * Prevent users deleting/editing users with a role outside their allowance.
 *
 * @param array  $caps    Returns the user's actual capabilities.
 * @param string $cap     Capability name.
 * @param int    $user_id The user ID.
 * @param array  $args    Adds the context to the cap. Typically the object ID.
 */
function house_map_meta_cap( $caps, $cap, $user_id, $args ) {

	/**
	 * Don't touch user with ID 1
	 * @var integer
	 */
	$protected_user = 1; // ID of user not editable
	/**
	 * I have a feeling that this piece of code doesn't
	 * do what I wrote it for
	 * @var doubt
	 */
	if ( $user_id === $protected_user ) {
		return $caps;
	}

	/**
	 * If user can edit users and args are set
	 * @var bool
	 */
	if ( ( $cap === 'edit_user' ) && isset( $args[0] ) ) :
		/**
		 * If user is not editing self
		 */
		if ( $user_id != $args[0] ) :
			// The user performing the task
			$moderator = get_userdata( $user_id );
			// The user being edited
			$user = get_userdata( $args[0] );


			if ( $moderator && $user ) :

				$moderator_roles = $moderator->roles;
				$admins = user_get_allowed_roles();

				/**
				 * If user has id 1 or 'administrator' role, no one can do anything
				 * @var bool
				 */
				if ( $user->ID === 1 || in_array( 'administrator', $user->roles ) ) :
					foreach ( $moderator_roles as $role => $caps ) {
						// Target user has roles outside of our limits
						$caps = 'do_not_allow';
					}

				else :

					foreach ( $admins as $admin => $allowed ) :

						if ( in_array( $admin, $moderator_roles ) ) {

							foreach ( $moderator_roles as $role => $caps ) {

								if ( array_diff( $user->roles, $allowed ) ) {
									// Target user has roles outside of our limits
									$caps = 'do_not_allow';
								}
							}
						}

					endforeach; // $admins as $admin
				endif; //  $user->ID === 1
			endif; // $moderator && $user
		endif; // $user_id != $args[0]
	endif; // ( $cap === 'edit_user' || $cap === 'delete_user' ) && isset( $args[0] )

	return $caps;
}
add_filter( 'map_meta_cap', 'house_map_meta_cap', 10, 4 );

/**
 * Role can manage
 *
 * Get the array of all roles that can be managed by the param role.
 * Administrator can manage all.
 *
 * @param  string $role     Role slug (name)
 * @return array|null       Returns array of roles
 */
function role_can_manage( $role = '' ) {
	$roles = user_get_allowed_roles();
	$all = house_all_roles();

	if ( $role === 'administrator' ) {
		return $all;
	} elseif ( isset( $roles[$role] ) ) {
		return $roles[$role];
	} else {
		return 'Error: Provided role <strong>' . $role . '</strong> doesn\'t have custom permission settings.';
	}
}
/**
 * Get user roles
 *
 * Get the array or users roles based on user id.
 *
 * @param  integer $id User id
 * @return array      Returns array of user roles
 */
function house_get_user_roles( $id = 1 ) {
	$userdata = get_userdata( $id );
	$roles = $userdata->roles;
	return $roles;
}
/**
 * Get a single (first) role for the user
 *
 * Roles are packed in array even though user has only
 * one in most of the case here.
 *
 * @param  int $id   User's id
 * @return string    Returns the role name
 */
function get_user_role( $id ) {
	$user = new WP_User( $id );
	return array_shift( $user->roles );
}


// http://wordpress.stackexchange.com/questions/10742/remove-ability-for-other-users-to-view-administrator-in-user-list
// /** Hide Administrator From User List **/
// function isa_pre_user_query($user_search) {
//   $user = wp_get_current_user();
//   if (!current_user_can('administrator')) { // Is Not Administrator - Remove Administrator
//     global $wpdb;

//     $user_search->query_where =
//         str_replace('WHERE 1=1',
//             "WHERE 1=1 AND {$wpdb->users}.ID IN (
//                  SELECT {$wpdb->usermeta}.user_id FROM $wpdb->usermeta
//                     WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities'
//                     AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%')",
//             $user_search->query_where
//         );
//   }
// }
// add_action('pre_user_query','isa_pre_user_query');