<?php
/**
 * User roles
 *
 * Create 3 custom roles for simple user management system.
 *
 * @package WordPress
 * @subpackage Members
 */
/**
 * Hooks
 */
add_action( 'init', 'house_custom_roles' );
/**
 * Custom Roles
 *
 * Create custom roles for simple user management system. These roles should be used
 * for cloning and creating new roles with specified capabilities. This is done
 * with 'Members' plugin.
 *
 * This function is attached to 'init' action hook.
 *
 * @link https://codex.wordpress.org/Function_Reference/add_role
 * @link https://codex.wordpress.org/Function_Reference/add_cap
 * @link https://codex.wordpress.org/Roles_and_Capabilities
 * @return obj Returns 3 new custom roles
 */
function house_custom_roles() {
	global $wp_roles;
	/**
	 * Set arrays of capabilities
	 * @var array
	 */
	$user_capabilities = array(
		'read' => true
	);

	$manager_capabilities = array(
		'read' => true,

		'edit_users' => true,
		'list_users' => true,

		'delete_roles' => false,
		'delete_users' => false,
	);

	$administrator_capabilities = array(
		'read' => true,

		'edit_users' => true,
		'list_users' => true,
		'create_users' => true,

		'delete_roles' => false,
		'delete_users' => false,
	);
	/**
	 * Register roles
	 * Translators: $role, $display_name, $capabilities
	 */
	add_role( 'user', 'User', $user_capabilities );
	add_role( 'user_manager', 'User Manager', $manager_capabilities );
	add_role( 'user_administrator', 'User Administrator', $administrator_capabilities );

}


