<?php
/**
 * Login and register forms
 *
 * Custom functionality for register and login forms.
 *
 * @link http://wordpress.stackexchange.com/a/137794
 * @link http://wordpress.stackexchange.com/a/40634
 * @link http://code.tutsplus.com/articles/quick-tip-making-a-fancy-wordpress-register-form-from-scratch--net-11904
 * @link https://pippinsplugins.com/creating-custom-front-end-registration-and-login-forms/
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'init', 'house_login_member' );
add_action( 'init', 'house_add_new_member' );
/**
 * Shortcodes
 */
add_shortcode( 'register_form', 'house_registration_form' );
add_shortcode( 'login_form', 'house_login_form' );
/**
 * Registration form shortcode
 *
 * Check if user is not logged in and if registrations are
 * enabled and render registration form.
 * Make it shortcode for easier usage.
 *
 * @return string Returns registration form or error message
 */
function house_registration_form() {

	// only show the registration form to non-logged-in members
	if( ! is_user_logged_in() ) {

		// check to make sure user registration is enabled
		$registration_enabled = get_option( 'users_can_register' );

		// only show the registration form if allowed
		if( $registration_enabled ) {
			$output = house_registration_form_fields();
		} else {
			$output = __( 'User registration is not enabled.', 'house' );
		}

		return $output;
	}
}
/**
 * Login form shortcode
 *
 * Check if user is not logged in and render login form.
 * Make it shortcode for easier usage.
 *
 * @return string Returns login form
 */
function house_login_form() {

	if ( ! is_user_logged_in() ) {
		$output = house_login_form_fields();
	} else {
		$output = '';
	}

	return $output;
}

/**
 * Registration form fields
 *
 * The actual registration form markup.
 *
 * @return string Returns registration form markup
 */
function house_registration_form_fields() {

	ob_start();
	// show any error messages after form submission
	house_show_error_messages(); ?>

	<form id="house_registration_form" class="house_form" action="" method="POST">

		<label class="assistive-text" for="house_user_Login"><?php _e( 'Username', 'house' ); ?></label>
		<input name="house_user_login" id="house_user_login" class="required input input--primary" type="text" placeholder="<?php _e( 'Username*', 'house' ); ?>"/>

		<label class="assistive-text" for="house_user_email"><?php _e( 'Your email address', 'house' ); ?></label>
		<input name="house_user_email" id="house_user_email" class="required input input--primary" type="email" placeholder="<?php _e( 'Your email address*', 'house' ); ?>"/>

		<label class="assistive-text" for="house_user_first"><?php _e( 'First Name', 'house' ); ?></label>
		<input name="house_user_first" id="house_user_first" type="text" class="input input--primary" placeholder="<?php _e( 'First Name', 'house' ); ?>"/>

		<label class="assistive-text" for="house_user_last"><?php _e( 'Last Name', 'house' ); ?></label>
		<input name="house_user_last" id="house_user_last" type="text" class="input input--primary" placeholder="<?php _e( 'Last Name', 'house' ); ?>"/>

		<label class="assistive-text" for="password"><?php _e( 'Password', 'house' ); ?></label>
		<input name="house_user_pass" id="password" class="required input input--primary" type="password" placeholder="<?php _e( 'Password*', 'house' ); ?>"/>

		<label class="assistive-text" for="password_again"><?php _e( 'Repeat password', 'house' ); ?></label>
		<input name="house_user_pass_confirm" id="password_again" class="required input input--primary" type="password" placeholder="<?php _e( 'Repeat password*', 'house' ); ?>"/>

		<input type="hidden" name="house_register_nonce" value="<?php echo wp_create_nonce( 'house-register-nonce' ); ?>"/>
		<input type="submit" class="btn btn--primary btn--secondary btn--full" value="<?php _e( 'Sign up', 'house' ); ?>"/>
	</form><?php

	return ob_get_clean();
}

/**
 * Login form fields
 *
 * The actual login form markup.
 *
 * @return string Returns login form markup
 */
function house_login_form_fields() {

	ob_start();
	// show any error messages after form submission
	house_show_error_messages(); ?>

	<form id="house_login_form" class="house_form" action="" method="post">

		<label class="assistive-text" for="house_user_Login"><?php _e( 'Username', 'house' ); ?></label>
		<input name="house_user_login" id="house_user_login" class="required input input--primary" type="text" placeholder="<?php _e( 'Username*', 'house' ); ?>"/>

		<label class="assistive-text" for="house_user_pass"><?php _e( 'Password', 'house' ); ?></label>
		<input name="house_user_pass" id="house_user_pass" class="required input input--primary" type="password" placeholder="<?php _e( 'Password*', 'house' ); ?>"/>

		<input type="hidden" name="house_login_nonce" value="<?php echo wp_create_nonce( 'house-login-nonce' ); ?>"/>
		<input id="house_login_submit" type="submit" class="btn btn--primary btn--secondary btn--full" value="<?php _e( 'Login', 'house' ); ?>"/>

		<div class="text-center">
			<a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>" title="<?php _e( 'Forgot password', 'house' ); ?>"><?php _e( 'Forgot password', 'house' ); ?></a>
		</div><!-- /.text-center -->
	</form><?php

	return ob_get_clean();
}

/**
 * Login member
 *
 * Logs a member in after submitting a form. This function is attached to
 * 'init' action hook.
 * @return void
 */
function house_login_member() {

	if ( isset( $_POST['house_login_nonce'] ) ) {

		if ( isset( $_POST['house_user_login'] ) && wp_verify_nonce( $_POST['house_login_nonce'], 'house-login-nonce' ) ) {

			/**
			 * Get user ID and other info from the user name
			 *
			 * get_userdatabylogin() is deprecated
			 *
			 * @link https://developer.wordpress.org/reference/functions/get_user_by/
			 * @var obj
			 */
			// $user = get_userdatabylogin( $_POST['house_user_login'] );
			$user = get_user_by( 'login', $_POST['house_user_login'] );
			/**
			 * Set errors
			 */
			// if the user name doesn't exist
			if ( ! $user ) {
				house_errors()->add( 'empty_username', __( 'Invalid username', 'house' ) );
			}
			// if no password was entered
			if ( ! isset( $_POST['house_user_pass'] ) || $_POST['house_user_pass'] == '' ) {
				house_errors()->add( 'empty_password', __( 'Please enter a password', 'house' ) );
			}
			/**
			 * Check the user's login with their password
			 * if the password is incorrect for the specified user
			 *
			 * @link https://codex.wordpress.org/Function_Reference/wp_check_password
			 */
			if ( ! wp_check_password( $_POST['house_user_pass'], $user->user_pass, $user->ID ) ) {
				house_errors()->add( 'empty_password', __( 'Incorrect password', 'house' ) );
			}
			// get all error messages
			$errors = house_errors()->get_error_messages();

			/**
			 * If no errors log the user in
			 */
			if ( empty( $errors ) ) {
				/**
				 * Set a cookie for a user who just logged in.
				 * @link https://codex.wordpress.org/Function_Reference/wp_setcookie
				 */
				wp_setcookie( $_POST['house_user_login'], $_POST['house_user_pass'], true );
				/**
				 * Change the current user by ID or name.
				 * @link https://codex.wordpress.org/Function_Reference/wp_set_current_user
				 */
				wp_set_current_user( $user->ID, $_POST['house_user_login'] );
				/**
				 * @link https://developer.wordpress.org/reference/hooks/wp_login/
				 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/wp_login
				 */
				do_action( 'wp_login', $_POST['house_user_login'] );
				/**
				 * Redirect user to home after logging in
				 * @link https://developer.wordpress.org/reference/functions/wp_redirect/
				 */
				wp_redirect( home_url() ); exit;
			} // empty( $errors )
		} // isset( $_POST['house_user_login'] ) && wp_verify_nonce()
	} // isset( $_POST['house_login_nonce'] )
}

/**
 * Add new member
 *
 * If registration form is successfully submitted, create new user
 * and log them in on form submission. This function is attached to
 * 'init' action hook.
 *
 * @return obj  Registers new user and log them in
 */
function house_add_new_member() {

	if ( isset( $_POST['house_register_nonce'] ) ) {

		if ( isset( $_POST["house_user_login"] ) && wp_verify_nonce( $_POST['house_register_nonce'], 'house-register-nonce' ) ) {
			$user_login   = $_POST["house_user_login"];
			$user_email   = $_POST["house_user_email"];
			$user_first   = $_POST["house_user_first"];
			$user_last    = $_POST["house_user_last"];
			$user_pass    = $_POST["house_user_pass"];
			$pass_confirm = $_POST["house_user_pass_confirm"];

			// this is required for username checks
			require_once( ABSPATH . WPINC . '/registration.php' );

			/**
			 * Set errors
			 */
			/**
			 * Username already registered
			 * @link https://codex.wordpress.org/Function_Reference/username_exists
			 */
			if ( username_exists( $user_login ) ) {
				house_errors()->add( 'username_unavailable', __( 'Username already taken', 'house' ) );
			}
			/**
			 * Invalid username
			 * @link https://codex.wordpress.org/Function_Reference/validate_username
			 */
			if ( ! validate_username( $user_login ) ) {
				house_errors()->add( 'username_invalid', __( 'Invalid username', 'house' ) );
			}
			// Empty username
			if ( $user_login == '' ) {
				house_errors()->add( 'username_empty', __( 'Please enter a username', 'house' ) );
			}
			/**
			 * Invalid email
			 * @link https://codex.wordpress.org/Function_Reference/is_email
			 */
			if ( ! is_email( $user_email ) ) {
				house_errors()->add( 'email_invalid', __( 'Invalid email', 'house' ) );
			}
			/**
			 * Email address already registered
			 * @link https://codex.wordpress.org/Function_Reference/email_exists
			 */
			if ( email_exists( $user_email ) ) {
				house_errors()->add( 'email_used', __( 'Email already registered', 'house' ) );
			}
			/**
			 * Empty password
			 */
			if ( $user_pass == '' ) {
				house_errors()->add( 'password_empty', __( 'Please enter a password', 'house' ) );
			}
			/**
			 * Passwords do not match
			 */
			if ( $user_pass != $pass_confirm ) {
				house_errors()->add( 'password_mismatch', __( 'Passwords do not match', 'house' ) );
			}

			// get all error messages
			$errors = house_errors()->get_error_messages();

			/**
			 * If no errors create user and log them in
			 */
			if ( empty( $errors ) ) {

				$new_user_id = wp_insert_user( array(
					'user_login'      => $user_login,
					'user_pass'       => $user_pass,
					'user_email'      => $user_email,
					'first_name'      => $user_first,
					'last_name'       => $user_last,
					'user_registered' => date( 'Y-m-d H:i:s' ),
					'role'            => 'subscriber'
				));

				if ( $new_user_id ) {
					// send an email to the administrator alerting them of the registration
					wp_new_user_notification(1);
					// send an email to the newly registered user
					$message  = __( 'Hello,', 'house' ) . "\r\n\r\n";
					$message .= sprintf( __( "Welcome to %s. Here are your login details:", "house" ), get_option( 'blogname' ) ) . "\r\n\r\n";
					$message .= wp_login_url() . "\r\n";
					$message .= sprintf( __( 'Username: %s', 'house' ), $user_login ) . "\r\n";
					$message .= sprintf( __( 'Password: %s', 'house' ), $user_pass ) . "\r\n\r\n";
					$message .= sprintf( __( 'If you have any problems, please contact administrator at %s.', 'house' ), get_option( 'admin_email' ) ) . "\r\n\r\n";

					wp_mail( $user_email, sprintf( __( '[%s] Your registration is completed', 'house' ), get_option( 'blogname' ) ), $message );

					/**
					 * Set a cookie for a user who just logged in.
					 * @link https://codex.wordpress.org/Function_Reference/wp_setcookie
					 */
					wp_setcookie( $user_login, $user_pass, true );
					/**
					 * Change the current user by ID or name.
					 * @link https://codex.wordpress.org/Function_Reference/wp_set_current_user
					 */
					wp_set_current_user( $new_user_id, $user_login );
					/**
					 * @link https://developer.wordpress.org/reference/hooks/wp_login/
					 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/wp_login
					 */
					do_action( 'wp_login', $user_login );
					/**
					 * Redirect user to home after logging in
					 * @link https://developer.wordpress.org/reference/functions/wp_redirect/
					 */
					wp_redirect( home_url() ); exit;
				} // $new_user_id
			} // empty( $errors )
		} // isset( $_POST["house_user_login"] ) && wp_verify_nonce()
	} // isset( $_POST['house_register_nonce'] )
}

/**
 * Errors
 *
 * Tracking error messages.
 *
 * @return obj|null Returns WP_Error or null
 */
function house_errors(){
	static $wp_error; // Will hold global variable safely
	return isset( $wp_error ) ? $wp_error : ( $wp_error = new WP_Error( null, null, null ) );
}
/**
 * Show error messages
 *
 * Render error messages on form submission failure.
 *
 * @return string|null Returns error messages markup
 */
function house_show_error_messages() {
	if ( $codes = house_errors()->get_error_codes() ) {
		echo '<div class="house_errors">';
		echo '<h3>' . __( 'Sorry, your submission has following errors:', 'house' ) . '</h3>';
			// Loop error codes and display errors
			foreach ( $codes as $code ) {
				$message = house_errors()->get_error_message( $code );
				echo '<span class="error"><strong>' . __( 'Error', 'house' ) . '</strong>: ' . $message . '</span><br/>';
			}
		echo '</div>';
	}
}