<?php
/**
 * Social functions
 *
 * Custom functionality for social networking
 *
 * @todo build list from all social profile links
 *
 * @package WordPress
 */

/**
 * Get the sharing link
 *
 * Helper function for share links
 *
 * @param  string $network   Network to share to, will be used for svg icon as well
 * @param  string $link      Considering this is one pager, link is just id of element we want to link to
 * @param  string $text      Some networks allow extra text (Twitter)
 * @param  string $hashtags  Hashatgs (Twitter)
 * @return string            Returns share link markup
 */
function get_share_link( $network = '', $link = '', $text = '', $hashtags = '' ) {

	if ( $network == 'twitter' ) {
		$href = 'http://twitter.com/share?text=' . $text . '&url=' . $link . '&hashtags=' . $hashtags;
	} elseif ( $network == 'facebook' ) {
		$href = 'http://www.facebook.com/sharer/sharer.php?u=' . $link;
	} elseif ( $network == 'google' ) {
		$href = 'https://plus.google.com/share?url=' . $link;
	}

	$output = '<a href="' . $href . '"';

	$output .= ' onclick="javascript:window.open(this.href, \'\',\'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=450,centerscreen=yes\');return false;"';

	$output .= '>';
	$output .= house_svg_icon( $network );
	$output .= ' ' . ucwords( $network );

	$output .= '</a>';

	return $output;
}
/**
 * Share link
 *
 * Echo share link
 *
 * @param  string $network   Network to share to, will be used for svg icon as well
 * @param  string $link      Considering this is one pager, link is just id of element we want to link to
 * @param  string $text      Some networks allow extra text (Twitter)
 * @param  string $hashtags  Hashatgs (Twitter)
 * @return string            Echoes share link markup
 */
function share_link( $network = '', $link = '', $text = '', $hashtags = '' ) {
	echo get_share_link( $network, $link, $text, $hashtags );
}
/**
 * Get options social profiles
 *
 * Get the social profiles values from branding options page
 * and store them in custom array in a way that we can easily
 * use just url or build the link.
 *
 * @return array Returns custom array with values from options page
 */
function get_option_social_profiles() {
	if ( get_field( 'social_network_profiles', 'option' ) ) :

		$profiles = get_field( 'social_network_profiles', 'option' );
		/**
		 * Prepare custom array
		 * @var array
		 */
		$soc = [];

		foreach ( $profiles as $profile ) {
			$soc[$profile['network']] = $profile['url'];
		}

		return $soc;

	endif; // get_field( 'social_network_profiles', 'option' )
}
/**
 * Get social profile url
 *
 * Check our custom array with social profiles values from brancding options page
 * and return url for a single network.
 *
 * @param  string $network Network identifier, available networks:
 *                         facebook, twitter, instagram, google, pinterest, youtube, vimeo, soundcloud
 * @return string          Returns social profile url
 */
function get_social_profile_url( $network = '' ) {
	$profiles = get_option_social_profiles();

	if ( isset( $profiles[$network] ) ) {
		return $profiles[$network];
	}
}
/**
 * Social profile url
 *
 * Echo social profile url
 *
 * @param  string $network Network identifier, available networks:
 *                         facebook, twitter, instagram, google, pinterest, youtube, vimeo, soundcloud
 * @return string          Echoes social profile url
 */
function social_profile_url( $network = '' ) {
	echo get_social_profile_url( $network );
}
/**
 * Get social profile link
 *
 * Check our custom array with social profiles values from brancding options page
 * and build link for a single network.
 *
 * @param  string $network Network identifier, available networks:
 *                         facebook, twitter, instagram, google, pinterest, youtube, vimeo, soundcloud
 * @param  string $class   Class for the link
 * @param  string $icon    Icon markup
 * @return string          Returns social link markup
 */
function get_social_profile_link( $network = '', $class = '', $icon = '' ) {
	$profiles = get_option_social_profiles();

	if ( $profiles[$network] ) {
		$link = '<a href="' . $profiles[$network] . '" target="_blank" class="' . $class . '">';

		if ( $icon ) {
			$link .= $icon;
		} else {
			$link .= ucwords( $network );
		}

		$link .= '</a>';

		return $link;
	}
}
/**
 * Social profile link
 *
 * Echo social profile link
 *
 * @param  string $network Network identifier, available networks:
 *                         facebook, twitter, instagram, google, pinterest, youtube, vimeo, soundcloud
 * @param  string $class   Class for the link
 * @param  string $icon    Icon markup
 * @return string          Echoes social link markup
 */
function social_profile_link( $network = '', $class = '', $icon = '' ) {
	echo get_social_profile_link( $network, $class, $icon );
}
/**
 * All social profiles
 *
 * Get all social profiles from branding options page and build their links. Optionally,
 * allow for custom content before and after the link (e.g. <li> and </li>).
 *
 * @param  string $class   Class for the link
 * @param  string $icon    Icon markup
 * @param  string $before  Custom content for before the link
 * @param  string $after   Custom content for after the link
 * @return string          Echoes all social profiles formated as links
 */
function all_social_profiles( $class = '', $icon = '', $before = '', $after = '' ) {
	$profiles = get_option_social_profiles();

	foreach ( $profiles as $network => $url ) {

		$link = '<a href="' . $url . '" target="_blank" class="' . $class . '">';

		if ( $icon ) {
			$link .= $icon;
		} else {
			$link .= ucwords( $network );
		}

		$link .= '</a>';

		echo $before . $link . $after;
	}
}
