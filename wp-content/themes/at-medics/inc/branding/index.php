<?php
/**
 * Here include branding related functions
 *
 */
// general plugin functions
include( get_template_directory() . '/inc/branding/general.php' );
// social functions
include( get_template_directory() . '/inc/branding/social.php' );
// contact functions
include( get_template_directory() . '/inc/branding/contact.php' );
