<?php
/**
 * Contact functions
 *
 * Custom functionality for contact info.
 *
 * @todo build list from all social profile links
 *
 * @package WordPress
 */
/**
 * Contact Info Array
 *
 * Get user input contact values from option page. Used on
 * Contact page and in 'wp-map.js' for Google map.
 *
 * @return array Returns array of user input values
 */
function get_contact_info_array() {
	$array = [];

	if ( get_field( 'contact_address_office', 'option' ) ) {
		$array['office'] = get_field( 'contact_address_office', 'option' );
	}
	if ( get_field( 'contact_address_street', 'option' ) ) {
		$array['street'] = get_field( 'contact_address_street', 'option' );
	}
	if ( get_field( 'contact_address_city', 'option' ) ) {
		$array['city'] = get_field( 'contact_address_city', 'option' );
	}
	if ( get_field( 'contact_latitude', 'option' ) ) {
		$array['latitude'] = get_field( 'contact_latitude', 'option' );
	}
	if ( get_field( 'contact_longitude', 'option' ) ) {
		$array['longitude'] = get_field( 'contact_longitude', 'option' );
	}
	if ( get_field( 'footer_email_address', 'option' ) ) {
		$array['email'] = get_field( 'footer_email_address', 'option' );
	}
	if ( get_field( 'footer_phone_number', 'option' ) ) {
		$array['phone'] = get_field( 'footer_phone_number', 'option' );
	}
	if ( get_field( 'business_hours', 'option' ) ) {
		$array['business_hours'] = get_field( 'business_hours', 'option' );
	}

	return $array;
}