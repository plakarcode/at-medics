<?php
/**
 * Branding graphics
 *
 * Functions to retrieve graphics uploaded via Branding Options page
 * (logo, favicon)
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'wp_head', 'get_site_favicon' );
/**
 * Get site logo
 *
 * Get the image uploaded via Branding Options page and place it in header.
 *
 * @return mix Returns site logo with link to home markup
 */
function get_site_logo() {
	global $globalSite;

	if ( house_is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
		$logo_array = get_field( 'site_logo', 'options' );

		if ( $logo_array ) {

			$logo_src = $logo_array['url'];

			$logo = '<img src="' . esc_url( $logo_src ) . '" alt="' . $globalSite['name'] . ' - ' . $globalSite['description'] . '" />';

			$link = '<a class="site-logo" href="' . esc_url( $globalSite['home'] ) . '" title="' . $globalSite['name'] . '" rel="home">' . $logo . '</a>';

			return $link;
		}
	}
}
/**
 * Render site logo
 *
 * @return string Echoes site logo
 */
function site_logo() {
	echo get_site_logo();
}
/**
 * Get site title
 *
 * @return string Returns home link site title
 */
function get_site_title() {
	global $globalSite;

	$link = '<h1><a class="site-title" href="' . esc_url( $globalSite['home'] ) . '" title="' . $globalSite['name'] . '" rel="home">' . $globalSite['name'] . '</a></h1>';

	return $link;
}
/**
 * Render site title
 * @return string Echoes site title
 */
function site_title() {
	echo get_site_title();
}
/**
 * Get site description
 *
 * @param  string $tag   Html tag to wrap site description in
 * @return string        Returns site description
 */
function get_site_description( $tag = 'div' ) {
	global $globalSite;

	$description = '<' . $tag . ' class="site-description">' . $globalSite['description'] . '</' . $tag . '>';

	return $description;
}
/**
 * Render site description - tagline
 *
 * @param  string $tag   Html tag to wrap site description in
 * @return string        Echoes site tagline
 */
function site_description( $tag = 'div' ) {
	echo get_site_description( $tag );
}
/**
 * Get site favicon
 *
 * Get the image uploaded via Branding Options page and
 * place it in document's head. This function is attached
 * to 'wp_head' action hook.
 *
 * @return mix Returns site favicon markup
 */
function get_site_favicon() {

	if ( house_is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
		$favicon_array = get_field( 'site_favicon', 'options' );

		if ( $favicon_array ) {

			$favicon_src = $favicon_array['url']; ?>

			<link rel="shortcut icon" href="<?php echo esc_url( $favicon_src ); ?>">

		<?php
		}
	}
}
/**
 * Custom logo
 *
 * Get the custom logo from customizer. If none provided,
 * fallback to site title and tagline
 *
 * @return string Returns logo or site name markup
 */
function house_custom_logo() {
	/**
	 * Get the logo value
	 * @var int|void
	 */
	$logo = get_theme_mod( 'custom_logo' );

	if ( $logo ) {
		/**
		 * Get the logo
		 */
		if ( function_exists( 'the_custom_logo' ) ) {
			the_custom_logo();
		}

	} else {

		/**
		 * Get site title
		 */
		if ( function_exists( 'site_title' ) ) {
			site_title();
		}
		/**
		 * Get tagline
		 */
		if ( function_exists( 'site_description' ) ) {
			site_description();
		}

	} // $logo
}