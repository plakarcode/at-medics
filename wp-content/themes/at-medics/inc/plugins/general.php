<?php
/**
 *	General plugins related functions
 *
 *	If plugin is active
 *
 *	@package WordPress
 */

/**
 *	If plugin is active
 *
 *	Checks if a certain plugin is active.
 *
 *	@uses is_multisite()
 *	@uses get_site_option()
 *	@uses get_option()
 *
 * 	<code>
 *	if ( house_is_plugin_active( 'plugin/plugin.php' ) ) {
 *		// do stuff with this plugin
 *	}
 *	</code>
 *
 *	@param string 	$plugin 	Plugin folder and main file to check.
 *	@return bool
 *
 *	@link http://www.ilovecolors.com.ar/wordpress-plugin-active-network-single-site/
 */
function house_is_plugin_active( $plugin = '' ) {
	$network_active = false;
	if ( is_multisite() ) {
		$plugins = get_site_option( 'active_sitewide_plugins' );
		if ( isset( $plugins[$plugin] ) ) {
			$network_active = true;
		}
	}
	return in_array( $plugin, get_option( 'active_plugins' ) ) || $network_active;
}