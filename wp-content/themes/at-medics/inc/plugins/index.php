<?php
/**
 * Here include all plugins related functions
 *
 */
// install default plugins
include( get_template_directory() . '/inc/plugins/defaults.php' );
// general plugin functions
include( get_template_directory() . '/inc/plugins/general.php' );
// jetpack plugin custom functions
include( get_template_directory() . '/inc/plugins/jetpack.php' );
// ACF helper functions
include( get_template_directory() . '/inc/plugins/acf.php' );

// Contact Form 7 customizations
if ( house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
	/**
	 * Accessing the plugin itself and modifying default fields
	 * via plugin's filters
	 */
	// simple contact form settings
	include( get_template_directory() . '/inc/plugins/contact-form-7/simple-form.php' );
	// contact form settings
	include( get_template_directory() . '/inc/plugins/contact-form-7/contact-form.php' );

	/**
	 * Select the form template
	 */
	include( get_template_directory() . '/inc/plugins/contact-form-7/select-template.php' );
}