<?php
/**
 * Steps menu walker
 *
 * Menu for steps has too much markup modifications
 * so we have to use walker.
 *
 * @package WordPress
 */
class Steps_Walker_Nav_Menu extends Walker_Nav_Menu {

	/**
	 * Start the element output.
	 *
	 * Adds custom classes and markup to menu items.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

		$depth_classes = array(
			'step-tooltip',
			( in_array( 'current_page_item', $item->classes ) ? 'is-active' : '' ),
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

		// Passed classes.
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		// Build HTML.
		$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

		// Link attributes.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		if ( get_field( 'step_subheading', $item->object_id ) ) {
			$number = $item->menu_order;

			switch( $number ) {
				case 1:
					$word = "one";
					break;
				case 2:
					$word = "two";
					break;
				case 3:
					$word = "three";
					break;
				case 4:
					$word = "four";
					break;
				case 5:
					$word = "five";
					break;
			}

			$tooltip = '<span class="step-tooltip__text step-' . $word . '">' . get_field( 'step_subheading', $item->object_id ) . '</span>';
		} else {
			$tooltip = '';
		}
		// after
		$args->after = $tooltip;

		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$args->after
		);
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}