<?php
/**
 * General shortcodes functions
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_filter( 'the_content', 'house_remove_inactive_shortcodes' );
/**
 * Remove inactive shortcodes
 *
 * Make sure inactive shortcodes don't leave their junk in the content.
 * We are striping their tags, leaving content as is. This function is attached to
 * 'the_content' filter hook.
 *
 * @global $shortcode_tags 		Associative array of all active shortcodes
 *         						[key] 			=> value
 *         						[shortcode_id] 	=> shortcode_function
 *
 * @uses  array_keys($array) 								Return all the keys or a subset of the keys of an array.
 * @uses  implode(string $glue , array $pieces) 			Returns a string containing a string representation of
 *        													all the array elements in the same order, with the
 *        													separator ($glue) string between each element.
 * @uses  preg_replace($pattern, $replacement, $string) 	Perform a regular expression search and replace.
 *
 * @link http://codex.wordpress.org/Function_Reference/get_shortcode_regex
 * @link http://codex.wordpress.org/Function_Reference/strip_shortcodes
 */
function house_remove_inactive_shortcodes( $content ) {
	global $shortcode_tags;

	$keys = array_keys( $shortcode_tags );
	$exclude_codes = implode( '|', $keys );
	$the_content = get_the_content();

	// strip all shortcodes but keep content
	// $the_content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $the_content);

	// strip all shortcodes except $exclude_codes and keep all content
	$the_content = preg_replace( "~(?:\[/?)(?!(?:$exclude_codes))[^/\]]+/?\]~s", '', $the_content );

	$content = $the_content;
	return $content;
}

/**
 * Enable shortcodes on various locked places
 *
 * Shortcodes in text widgets
 * Shortcodes in excerpts
 * Shortcodes in Category, Tag, and Taxonomy Descriptions
 *
 * @link http://sillybean.net/2010/02/using-shortcodes-everywhere/
 * @link https://developer.wordpress.org/reference/functions/do_shortcode/
 */
// Shortcodes in text widgets
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );

// Shortcodes in Excerpts
// if not working take a look at:
// http://pastebin.com/9pS9iCx2
add_filter( 'the_excerpt', 'shortcode_unautop' );
add_filter( 'the_excerpt', 'do_shortcode' );
add_filter( 'get_the_excerpt', 'shortcode_unautop' );
add_filter( 'get_the_excerpt', 'do_shortcode' );

// Shortcodes in Category, Tag, and Taxonomy Descriptions
add_filter( 'term_description', 'shortcode_unautop' );
add_filter( 'term_description', 'do_shortcode' );