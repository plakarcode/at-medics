<?php
/**
 * Here include all shortcodes related functions
 */

// General shortcodes functions
include( get_template_directory() . '/inc/shortcodes/general.php' );
// Add more quicktags to HTML editor.
include( get_template_directory() . '/inc/shortcodes/quicktags.php' );
// Popup link
include( get_template_directory() . '/inc/shortcodes/basic-shortcodes.php' );