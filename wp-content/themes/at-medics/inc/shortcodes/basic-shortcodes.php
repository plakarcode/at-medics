<?php
/**
 * Popup links buttons
 *
 * Add popup links buttons
 *
 * @link http://codex.wordpress.org/Quicktags_API
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'admin_print_footer_scripts', 'house_add_popup_link', 100 );
/**
 * Popup link buttons
 *
 * Include additional buttons in the Text (HTML) mode of the WordPress editor. This function is attached to the
 * admin_print_footer_scripts action hook. wp_footer action hook can also be used.
 *
 * @param string id                 Required. Button HTML ID
 * @param string display            Required. Button's value="..."
 * @param string|function arg1      Required. Either a starting tag to be inserted like "<span>" or a callback
 * @param string arg2               Optional. Ending tag like "</span>"
 * @param string access_key         Optional. Access key for the button.
 * @param string title              Optional. Button's title="..."
 * @param int priority              Optional. Position of the button in the toolbar. 1 - 9 = first, 11 - 19 = second..
 * @param string instance           Optional. Limit the button to a specifric instance of Quicktags
 *
 * <code>
 * 	QTags.addButton( id, display, arg1, arg2, access_key, title, priority, instance );
 * </code>
 *
 *
 * Defaults:
 * 	edButtons[10] = new qt.TagButton('strong','b','<strong>','</strong>','b');
 * 	edButtons[20] = new qt.TagButton('em','i','<em>','</em>','i'),
 * 	edButtons[30] = new qt.LinkButton(), // special case
 * 	edButtons[40] = new qt.TagButton('block','b-quote','\n\n<blockquote>','</blockquote>\n\n','q'),
 * 	edButtons[50] = new qt.TagButton('del','del','<del datetime="' + _datetime + '">','</del>','d'),
 * 	edButtons[60] = new qt.TagButton('ins','ins','<ins datetime="' + _datetime + '">','</ins>','s'),
 * 	edButtons[70] = new qt.ImgButton(), // special case
 * 	edButtons[80] = new qt.TagButton('ul','ul','<ul>\n','</ul>\n\n','u'),
 * 	edButtons[90] = new qt.TagButton('ol','ol','<ol>\n','</ol>\n\n','o'),
 * 	edButtons[100] = new qt.TagButton('li','li','\t<li>','</li>\n','l'),
 * 	edButtons[110] = new qt.TagButton('code','code','<code>','</code>','c'),
 * 	edButtons[120] = new qt.TagButton('more','more','<!--more-->\n\n','','t'),
 * 	edButtons[140] = new qt.CloseButton();
 *
 * @link https://core.trac.wordpress.org/browser/tags/4.5.2/src/wp-includes/js/quicktags.js?order=name#L692
 * @link http://wordpress.stackexchange.com/a/65248
 * @link http://www.codedevelopr.com/articles/wordpress-custom-editor-quicktag-buttons/
 *
 */
function house_add_popup_link() {
	?>

	<script type="text/javascript">
		// LogIn Button
		QTags.addButton(
			'house-login', 														// $id
			'LogIn Button',														// $display
			'<a href="#open-login" class="js-login-popup">',					// $arg1
			'</a>', 															// $arg2
			'',																	// $access_key
			'LogIn Button',														// $title
			141 																// $priority
		);

		// SignUp Button
		QTags.addButton(
			'house-signup', 													// $id
			'SignUp Button',													// $display
			'<a href="#open-signup" class="js-login-popup">',					// $arg1
			'</a>', 															// $arg2
			'',																	// $access_key
			'SignUp Button',													// $title
			142																	// $priority
		);
	</script>
	<?php
}