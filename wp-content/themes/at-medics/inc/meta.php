<?php
/**
 * Custom functions not related to anything specific
 */
/**
 * Get Login/Logout link
 *
 * Get the login or logout link depending on whether user is longin or guest.
 * Also, redirect user to initial page after logging in or out.
 *
 * @uses is_user_logged_in()  Checks if the current user is logged into the site.
 * @uses wp_logout_url()      Creates a logout URL.
 * @uses wp_login_url()       Creates a login URL.
 *
 * @param $before             Custom content before link
 * @param $after              Custom content after link
 * @return string             Returns login/logout link
 */
function house_get_loginout_link( $before = '', $after = '' ) {

	$redirect = site_url( $_SERVER['REQUEST_URI'] );

	if ( is_user_logged_in() ) {
		$class = 'logout-link';
		$href  = wp_logout_url( $redirect );
		$title = esc_attr__( 'Log out', 'house' );
		$label = __( 'Log out', 'house' );
	} else {
		$class = 'login-link';
		$href  = wp_login_url( $redirect );
		$title = esc_attr__( 'Log in', 'house' );
		$label = __( 'Log in', 'house' );
	}

	$link = '<a href="' . esc_url( $href ) . '" title="' . $title . '" class="' . $class . '">' . $label . '</a>';

	return $before . $link . $after;
}
/**
 * House Login/Logout link
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Echoes login/logout link
 */
function house_loginout( $before = '', $after = '' ) {
	echo house_get_loginout_link( $before, $after );
}
/**
 * Get Post Author Link
 *
 * Build html for post author link.
 *
 * @uses get_the_author()			The author's display name.
 * @uses get_the_author_meta()		Returns the desired meta data for a user.
 * @uses get_author_posts_url()	Gets the URL of the author page for the author with a given ID.
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Returns author's link markup
 */
function house_get_author_link( $before = '', $after = '' ) {

	$author = sprintf( '<a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'house' ), get_the_author() ) ),
		get_the_author()
	);

	return $before . $author . $after;
}
/**
 * Post Author Link
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Echoes author's link
 */
function house_author_link( $before = '', $after = '' ) {
	echo house_get_author_link( $before, $after );
}
/**
 * Get Permalink
 *
 * Render markup for post/page permalink
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Returns post/page permalink
 */
function house_get_permalink( $before = '', $after = '' ) {

	$permalink = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="' . esc_attr( sprintf( __( 'Permalink to %s', 'house' ), the_title_attribute( 'echo=0' ) ) ) . '">' . get_the_title() . '</a>';

	return $before . $permalink . $after;
}
/**
 * Post/page permalink
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Echoes post/page permalink
 */
function house_permalink( $before = '', $after = '' ) {
	echo house_get_permalink( $before, $after );
}

/**
 * Get Comments Link
 *
 * Displays a post's number of comments wrapped in a link to the comments area.
 *
 * @uses get_comments_link() 		Returns the URL to the specified or current post's comments.
 * @uses get_comments_number() 	    Retrieves the value of the total number of comments, Trackbacks, and Pingbacks.
 * @uses comments_open() 			Checks if comments are allowed for the current Post or a given Post ID.
 * @uses pings_open()				Checks if trackbacks are allowed for the current Post or a given Post ID.
 * @uses the_title_attribute()		Displays or returns the title of the current post.
 * @uses number_format_i18n()		Convert an integer number to format based on the locale.
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Returns comments link
 */
function house_get_comments_link( $before = '', $after = '' ) {

	$array = array(
		'zero'      => __( 'Leave a comment', 'house' ),
		'one'       => __( '%1$s Comment', 'house' ),
		'more'      => __( '%1$s Comments', 'house' ),
	);

	$number        = doubleval( get_comments_number() );
	$title         = sprintf( esc_attr__( 'Comment on %1$s', 'house' ), the_title_attribute( 'echo=0' ) );
	$number_format = number_format_i18n( $number );

	if ( comments_open() ) {

		if ( 0 == $number ) {
			$comments_link = '<a href="' . get_permalink() . '#respond" title="' . $title . '">' . sprintf( $array['zero'], $number_format ) . '</a>';
		}
		elseif ( 1 == $number ) {
			$comments_link = '<a href="' . get_comments_link() . '" title="' . $title . '">' . sprintf( $array['one'], $number_format ) . '</a>';
		}
		elseif ( 1 < $number ) {
			$comments_link = '<a href="' . get_comments_link() . '" title="' . $title . '">' . sprintf( $array['more'], $number_format ) . '</a>';
		}

	} else {

		$comments_link = '';

	}

	$link = $before . $comments_link . $after;

	return $link;
}
/**
 * Comments link
 *
 * @param $before    Custom content before link
 * @param $after     Custom content after link
 * @return string    Echoes comments link
 */
function house_comments_link( $before = '', $after = '' ) {
	echo house_get_comments_link( $before, $after );
}
/**
 * Filter the human readable difference between two timestamps.
 *
 * Change 'hours' to 'h', 'days' to 'd' etc. This function is attached to
 * 'human_time_diff' filter hook.
 *
 * @since 4.0.0
 *
 * @param string $since The difference in human readable text.
 * @param int    $diff  The difference in seconds.
 * @param int    $from  Unix timestamp from which the difference begins.
 * @param int    $to    Unix timestamp to end the time difference.
 */
function house_short_time_ago( $since, $diff, $from, $to ) {

    $replace = array(
        ' min'   => 'm',
        ' mins'  => 'm',
        ' hour'  => 'h',
        ' hours' => 'h',
        ' day'   => 'd',
        ' days'  => 'd',
        ' week'  => 'w',
        ' weeks' => 'w',
        ' year'  => 'y',
        ' years' => 'y',
    );

    return strtr( $since, $replace );
}
/**
 * Get published time ago
 *
 * Get the post's published time and convert it to 'time ago' format. Add filter
 * if shorter version for timestamps is needed.
 *
 * @param  boolean $short    Short or full version for timestamp
 * @param  int     $post_id  ID of the post we want published time for, get_the_ID() is default
 * @param  string  $before   Custom content before timestamp
 * @param  string  $after    Custom content after timestamp
 * @return string            Returns post's published time ago
 */
function house_get_publish_time_ago( $short = false, $post_id = '', $before = '', $after = '' ) {

	if ( $post_id ) {
		$id = $post_id;
	} else {
		$id = get_the_ID();
	}

	if ( $short === true ) {
		add_filter( 'human_time_diff', 'house_short_time_ago', 10, 4 );
	}

	$ago = human_time_diff( get_the_time( 'U', $id ), current_time( 'timestamp' ) );

	return $before . $ago . $after;
}
/**
 * Published time ago
 *
 * Echo post's published time in 'time ago' format.
 *
 * @param  boolean $short    Short or full version for timestamp
 * @param  int     $post_id  ID of the post we want published time for, get_the_ID() is default
 * @param  string  $before   Custom content before timestamp
 * @param  string  $after    Custom content after timestamp
 * @return string            Echoes post's published time ago
 */
function house_publish_time_ago( $short = false, $post_id = '', $before = '', $after = '' ) {
	echo house_get_publish_time_ago( $short, $post_id, $before, $after );
}
/**
 * Get entry taxonomies
 *
 * Use the same function for any taxonomy (buit in or custom), get the terms and
 * format them for use in the loop.
 *
 * @param  string $taxonomy  Taxonomy name
 * @param  string $separator Separator for terms
 * @param  string $before    Custom content before list
 * @param  string $after     Custom content after list
 * @return string            Returns taxonomy terms list
 */
function house_get_entry_taxonomies( $taxonomy = 'category', $separator = ' / ', $before = 'Categories ', $after = '' ) {

	$terms_list = get_the_term_list( get_the_ID(), $taxonomy, $before, $separator, $after );

	if ( $terms_list ) {
		$list = sprintf( '<div class="meta-%2$s">%1$s</div>',
			$terms_list,
			$taxonomy
		);

		return $list;
	}
}
/**
 * Entry taxonomies
 *
 * Echo the taxonomy terms for post.
 *
 * @param  string $taxonomy  Taxonomy name
 * @param  string $separator Separator for terms
 * @param  string $before    Custom content before list
 * @param  string $after     Custom content after list
 * @return string            Echoes taxonomy terms list
 */
function house_entry_taxonomies( $taxonomy = 'category', $separator = ' / ', $before = 'Categories ', $after = '' ) {
	echo house_get_entry_taxonomies( $taxonomy, $separator, $before, $after );
}
/**
 * First category name
 * @return string Echoes name of the first category
 */
function first_category_name() {
	$categories = get_the_category();

	if ( ! empty( $categories ) ) {
		echo esc_html( $categories[0]->name );
	}
}
/**
 * Custom meta
 *
 * Write less code for custom fields, check if the field is populated
 * and echo it when it's safe.
 *
 * @param  string $custom_field Unique id for custom field
 * @return string               Echoes value for the custom field
 */
function custom_meta( $custom_field ) {
	// Translators: post id, custom field id, return a single value
	if ( get_post_meta( get_the_ID(), $custom_field, true ) ) {
		echo get_post_meta( get_the_ID(), $custom_field, true );
	}
}