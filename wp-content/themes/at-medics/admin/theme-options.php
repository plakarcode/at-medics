<?php
/**
 * Theme options pages
 *
 * @package  WordPress
 */

/**
 * Top level page options
 *
 */
$theme_page = array(
	/* (string) The title displayed on the options page. Required. */
	'page_title' => '',

	/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
	'menu_title' => __( 'Theme options', 'house' ),

	/* (string) The slug name to refer to this menu by (should be unique for this menu).
	Defaults to a url friendly version of menu_slug */
	'menu_slug' => 'theme-options',

	/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
	Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
	'capability' => 'edit_theme_options',

	/* (int|string) The position in the menu order this menu should appear.
	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
	Defaults to bottom of utility menu items */
	'position' => 4,

	/* (string) The slug of another WP admin page. if set, this will become a child page. */
	'parent_slug' => '',

	/* (string) The icon url for this menu. Defaults to default WordPress gear */
	'icon_url' => false,

	/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
	If set to false, this parent page will appear alongside any child pages. Defaults to true */
	'redirect' => true,

	/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
	Defaults to 'options'. Added in v5.2.7 */
	'post_id' => 'theme_options',

	/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
	Defaults to false. Added in v5.2.8. */
	'autoload' => false
);

$guides_page = array(
	/* (string) The title displayed on the options page. Required. */
	'page_title' => '',

	/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
	'menu_title' => __( 'Guides', 'house' ),

	/* (string) The slug name to refer to this menu by (should be unique for this menu).
	Defaults to a url friendly version of menu_slug */
	'menu_slug' => 'theme-guidelines',

	/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
	Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
	'capability' => 'edit_roles',

	/* (int|string) The position in the menu order this menu should appear.
	WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
	Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
	Defaults to bottom of utility menu items */
	// 'position' => 4,

	/* (string) The slug of another WP admin page. if set, this will become a child page. */
	'parent_slug' => '',

	/* (string) The icon url for this menu. Defaults to default WordPress gear */
	'icon_url' => 'dashicons-sos',

	/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
	If set to false, this parent page will appear alongside any child pages. Defaults to true */
	'redirect' => true,

	/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
	Defaults to 'options'. Added in v5.2.7 */
	'post_id' => 'theme_guidelines',

	/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
	Defaults to false. Added in v5.2.8. */
	'autoload' => false
);
// add the page
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( $theme_page );
	acf_add_options_page( $guides_page );
}
/**
 * Add subpages
 *
 * The first one is overriding the parent so that we don't
 * stuck with repeated menu item.
 */
if ( function_exists( 'acf_add_options_sub_page' ) ) {
	// general options
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'General Settings', 'house' ),
		'menu_title' 	=> __( 'General', 'house' ),
		'parent_slug' 	=> 'theme-options',
		'capability'    => 'edit_theme_options',
		'menu_slug' 	=> 'theme-options-general'
	));
	// branding options
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'Branding Options', 'house' ),
		'menu_title' 	=> __( 'Branding', 'house' ),
		'parent_slug' 	=> 'theme-options',
		'capability'    => 'edit_theme_options',
		'menu_slug' 	=> 'theme-options-branding'
	));
	// mailchimp options
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'Subscribe Options', 'house' ),
		'menu_title' 	=> __( 'Subscribe', 'house' ),
		'parent_slug' 	=> 'theme-options',
		'menu_slug' 	=> 'theme-options-mailchimp'
	));
	// users permissions
	// This page is subpage of WordPress' default Users menu
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'Users Permissions', 'house' ),
		'menu_title' 	=> __( 'Permissions', 'house' ),
		'parent_slug' 	=> 'users.php',
		'menu_slug' 	=> 'options-users-permissions',
		'capability'    => 'edit_theme_options', // we don't want users with permissions to edit other users here, just admins
	));
	// Guides
	// usergroups
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'User Management Overview', 'house' ),
		'menu_title' 	=> __( 'Overview', 'house' ),
		'parent_slug' 	=> 'theme-guidelines',
		'capability'    => 'edit_roles',
		'menu_slug' 	=> 'theme-guidelines-user-overview'
	));
	// usergroups
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'User groups', 'house' ),
		'menu_title' 	=> __( 'User groups', 'house' ),
		'parent_slug' 	=> 'theme-guidelines',
		'capability'    => 'edit_roles',
		'menu_slug' 	=> 'theme-guidelines-usergroups'
	));
	// permissions
	acf_add_options_sub_page( array(
		'page_title' 	=> __( 'Permissions', 'house' ),
		'menu_title' 	=> __( 'Permissions', 'house' ),
		'parent_slug' 	=> 'theme-guidelines',
		'capability'    => 'edit_roles',
		'menu_slug' 	=> 'theme-guidelines-permissions'
	));
}




