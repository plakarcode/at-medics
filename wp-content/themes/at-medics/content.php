<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>

	<?php
		/**
		 * Breadcrumbs
		 */
		get_template_part( 'partials/navigations/breadcrumbs' );

	?>
<div class="container">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
			/**
			 * Get post title
			 */
			//get_template_part( 'partials/content/title-singular' );

			/**
			 * Get flexible content fields if any,
			 * otherwise get regular content
			 */
			if ( function_exists( 'get_field' ) && get_field( 'all_content_fields' ) ) :

				get_template_part( 'partials/flexible-templates/sections' );

			else :

				get_template_part( 'partials/content/post-content' );

			endif; // get_field( 'all_content_fields' )

			/**
			 * Get post author meta
			 */
			get_template_part( 'partials/meta/post-author' );
		?>

	</article><!-- #post -->
</div>