
<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
global $globalSite; ?>

		<?php
			/**
			 * Get subscribe
			 */
			get_template_part( 'partials/forms/subscribe' );
		?>
	</main><!-- #content -->

	<footer id="mainfooter" class="footer-main" role="contentinfo">

		<div class="footer-main__top">
			<div class="justifize">
				<div class="justifize__box">
					<a href="<?php echo $globalSite['home']; ?>" class="footer-main__logo" title="<?php echo $globalSite['name']; ?>">
						<?php echo house_image( 'logo-footer.svg', '', $globalSite['name'] ); ?>
					</a>
				</div><!-- justifize__box -->
				<div class="justifize__box">
					<?php
						/**
						 * Get footer navigation
						 */
						get_template_part( 'partials/navigations/secondary' );
					?>
				</div><!-- justifize__box -->
			</div><!-- justifize -->
		</div><!-- footer-main__top -->

		<div class="footer-main__bottom">
			<div class="justifize">
				<div class="justifize__box">
					<small>
						<?php
							/**
							 * Get footer copyrights
							 */
							if ( function_exists( 'house_footer_copyrights' ) ) {
								house_footer_copyrights();
							}
							/**
							 * If we have both, email and phone, render separator
							 */
							if ( get_field( 'footer_email_address', 'option' ) ) {
								echo ' | ';
							}
							/**
							 * Get contact email address
							 */
							if ( get_field( 'footer_email_address', 'option' ) ) {
								the_field( 'footer_email_address', 'option' );
							}
						?>
					</small>
				</div><!-- justifize__box -->

				<div class="justifize__box">
					<a href="javascript:;" class="back-to-top">back to top</a>
				</div><!-- justifize__box -->
				
			</div><!-- justifize -->
		</div><!-- footer-main__bottom -->

	</footer><!-- #mainfooter -->

	<?php
		/**
		 * Get register and login forms popups
		 */
		get_template_part( 'partials/forms/login' );
		get_template_part( 'partials/forms/register' );
	?>

<?php wp_footer(); ?>
</body>
</html>