{
    "key": "group_57ab243613cdf",
    "title": "User Groups Guidelines",
    "fields": [
        {
            "key": "field_57ab25c77405b",
            "label": "New User Group (Role)",
            "name": "new_user_group_role",
            "type": "tab",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "placement": "top",
            "endpoint": 0
        },
        {
            "key": "field_57ab2586e2fb6",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "Role or Usergroup is a set of capabilities which defines what user can access and do in Dashboard. An Role can be assigned to unlimited number of users and there can be unlimited number of Roles created, with same or different capabilities.\r\n\r\nThere are a couple of ways to create new Role:\r\n\r\n<ol>\r\n<li>Go to <code>Users &rsaquo; <a href=\"\/wp-admin\/users.php?page=role-new\" target=\"_blank\">Add New Role<\/a><\/code><\/li>\r\n<li>Clone existing role with same or similar capabilities needed for new role, <code>Users &rsaquo; <a href=\"\/wp-admin\/users.php?page=roles\" target=\"_blank\">Roles<\/a><\/code><\/li>\r\n<\/ol>\r\n\r\n<div class=\"warning\">\r\n<h3>WARNING<\/h3> \r\nApproving some capabilities can, potentially, be dangerous if in wrong hands. Not that anyone would do harm on purpose but rather out of curiosity without knowledge of what is possible. Worst case scenario, with too much power someone could accidentally demote or delete main administrator and leave a large portion of website inaccessible. With great power comes great responsibility and when in doubt do consult WordPress' official <a href=\"https:\/\/codex.wordpress.org\/Roles_and_Capabilities\" target=\"_blank\">documentation on Roles and Capabilities<\/a>.\r\n<\/div>\r\n\r\nThat being said, rule of thumb is: if you need to create new Role with all (or most of) capabilities the existing one already have, always go with cloning option. When cloning, you simply have to change the Role name, check if selected capabilities are desired ones and save new role by clicking <code>Add Role<\/code>. When creating completely new role all capabilities are unchecked. This means you need to be very careful with what you grant.",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57b2c289a4d8e",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "<h3>Cloning Existing Role<\/h3>\r\n\r\nTake a look at <code>Existing User Groups<\/code> tab to see all roles you can choose from. All you need to do is clone role whose capabilities you need, change the role name (which will have <strong>Clone<\/strong> included) and save new role by clicking <code>Add Role<\/code> in right hand corner.\r\n\r\nGo to <a href=\"wp-admin\/users.php?page=roles\" target=\"_blank\">Roles listing<\/a> and select the role for cloning.\r\n\r\n<img src=\"\/wp-content\/themes\/house\/admin\/images\/clone.png\" \/>\r\n\r\n<h3>Renaming Role<\/h3>\r\n\r\n<strong>Once created, role can NOT be renamed<\/strong>. If you need to rename custom role simply clone it and, before saving, change the name. After that you must move all users from old to this new role and then delete the old role.",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57b2c2d2b7dbe",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "<h1>Moving users to new Role<\/h1>\r\n\r\n<h3>Change role for single user<\/h3>\r\n\r\nGo to <a href=\"wp-admin\/users.php\" target=\"_blank\">All Users listing<\/a> and select the user for editing.\r\n\r\n<img src=\"\/wp-content\/themes\/house\/admin\/images\/edit-user.png\" \/>\r\n\r\nIn new screen (user profile) select new role from dropdown list and click <code>Update User<\/code> at the bottom of the page.\r\n\r\n<img src=\"\/wp-content\/themes\/house\/admin\/images\/edit-user-role.png\" \/>",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57b2c54f58290",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "<h3>Change role for all users in another role<\/h3>\r\n\r\nChanging roles for a lot of users can be easily done from <a href=\"wp-admin\/users.php\" target=\"_blank\">All Users listing<\/a>. Let's say you want to move all users from <code>Usergroup 1<\/code> role to <code>Usergroup 2<\/code> role. You can go and edit one by one user but much quicker is to select <code>Usergroup 1<\/code> filter from above the users listing, select all users by checking the <code>all<\/code> checkbox on the top or bottom of list, select new role from <code>Change role to...<\/code> dropdown and click <code>Change<\/code>.\r\n\r\n<div class=\"image-wrap\">\r\n<img src=\"\/wp-content\/themes\/house\/admin\/images\/change-role.png\" \/>\r\n<\/div>",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57ab25ec7405c",
            "label": "Capabilities Explained",
            "name": "capabilities_explained",
            "type": "tab",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "placement": "top",
            "endpoint": 0
        },
        {
            "key": "field_57ab25fc7405d",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "There are many capabilities in WordPress and most of them are named in such a way that we can assume what they are granting to user. However, it is important to know that WordPress has two types of capabilities: \r\n\r\n<ol>\r\n<li><code>primitives<\/code> - these are granting one new thing to user. E.G. <code>list_users<\/code> capability allows user access to <code>Users &rsaquo; All Users<\/code> page while, without it, user can access only own profile on <code>Users &rsaquo; Your Profile<\/code><\/li>\r\n<li><code>meta<\/code> - these are sets of capabilities. They don't necessarily introduce new access for user but rather grant or implies that several other capabilities are granted. E.G. <code>edit_users<\/code> allows editing other users' profiles which includes changing their roles independently of <code>promote_users<\/code> capability.<\/li>\r\n<\/ol>\r\n\r\nWhen working with roles and capabilities always consult official <a href=\"https:\/\/codex.wordpress.org\/Roles_and_Capabilities\" target=\"_blank\">documentation on Roles and Capabilities<\/a>.\r\n\r\n<h3>Users Related Capabilities<\/h3>\r\n\r\nWe will discuss here capabilities for managing users and roles. Obviously, <code>Administrator<\/code> must have all capabilities and, obviously, not many people should have access with this level. \r\n\r\nWhen you go to <a href=\"\/wp-admin\/users.php?page=roles\" target=\"_blank\">create\/edit role<\/a> screen and select capabilities for users, you'll find following list:\r\n\r\n<div class=\"image-wrap\">\r\n<img src=\"\/wp-content\/themes\/house\/admin\/images\/administrator-capabilities.png\" \/>\r\n<\/div>\r\n\r\nEverything <code>_roles<\/code> related should never be granted to any role except for <code>Administrator<\/code>. Also, only administrators should be able to <code>delete_users<\/code>. Otherwise, user with deleting capability could accidentally delete administrator even though all users are denied capability of editing users in higher roles. Note here that editing and deleting users are two completely independent capabilities which gives a lot of flexibility but can be dangerous as well.\r\n\r\nBasically, there are <strong>4 capability levels<\/strong> (roles) when setting up user management:\r\n\r\n<h4><code>1. USER - Can manage only own profile<\/code><\/h4>\r\n\r\nThis is the case of built in role <code>Subscriber<\/code> which has zero capabilities granted from the list above. The only capability required for logged in user to see and manage own profile is <code>read<\/code>.\r\n\r\n<h4><code>2. USER MANAGER - Can manage other users (who can manage only their own profile)<\/code><\/h4>\r\n\r\nBesides the required <code>read<\/code> capability, these <strong>custom roles<\/strong> should have two more:\r\n\r\n<ol>\r\n<li><code>edit_users<\/code> - allows editing other's users profiles<\/li>\r\n<li><code>list_users<\/code> - allows seeing the list of all users. However, links to edit page is only for those users logged in user has permissions to edit (more about permissions <a href=\"\/wp-admin\/admin.php?page=theme-guidelines-permissions\" target=\"_blank\">here<\/a>)<\/li>\r\n<\/ol>\r\n\r\n\r\n<h4><code>3. USER ADMINISTRATOR - Can manage other users (both previous groups)<\/code><\/h4>\r\n\r\nThis is the higher level role so it has all previously mentioned capabilities - <code>read<\/code>, <code>edit_users<\/code> and <code>list_users<\/code>, plus another one <code>create_users<\/code>. This capability will allow user to add new users and set role for them (only those roles this user has permissions to manage).\r\n\r\n<h4><code>4. ADMINISTRATOR - Can manage everyone and everything<\/code><\/h4>\r\n\r\nAdministrator, all capabilities checked.\r\n\r\n<hr>\r\n\r\nAs you can see, roles are not specific about what is editable, they can just give access or \"all-or-nothing\" ability. The real flexibility and complexity of user management comes with <a href=\"\/wp-admin\/admin.php?page=theme-guidelines-permissions\" target=\"_blank\">permissions<\/a>.",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57ab29df84f8d",
            "label": "Existing User Groups",
            "name": "existing_user_groups",
            "type": "tab",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "placement": "top",
            "endpoint": 0
        },
        {
            "key": "field_57ac75e8f1cf7",
            "label": "",
            "name": "",
            "type": "enhanced_message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": 30,
                "class": "",
                "id": ""
            },
            "enhanced_message": "<?php\r\nglobal $wp_roles;\r\n$roles = $wp_roles->get_names(); ?>\r\n<h3>All Existing Roles<\/h3>\r\n\r\nHere are all roles on this website, built in and custom.\r\n\r\n<ol>\r\n\t<?php foreach ( $roles as $role ) : ?>\r\n\t\t<li><strong><?php echo $role; ?><\/strong><\/li>\r\n\t<?php endforeach; ?>\r\n<\/ol>",
            "hide_label": "no"
        },
        {
            "key": "field_57ac78a64f585",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": 70,
                "class": "",
                "id": ""
            },
            "message": "<h3>Built In Roles<\/h3>\r\n\r\nThese are pre-made roles that comes with fresh WordPress installation:\r\n\r\n<ul>\r\n<li><code>Administrator<\/code> - by default first user is administrator. This role can do everything and shouldn't be used for more than 2 users.<\/li>\r\n<li><code>Editor<\/code> - can do anything with all posts, pages, attachments and comments. In users domain, by default this role can <code>create_users<\/code>, <code>list_users<\/code>, <code>edit_users<\/code> and <code>delete_users<\/code>. However, we have forbid all users except Administrators to delete any other user.<\/li>\r\n<li><code>Author<\/code> - this role can do anything with own posts, own attachments and comments on own posts. In terms of users, this role can only see and edit own profile.<\/li>\r\n<li><code>Contributor<\/code> - can edit and delete own, already published by editor, posts. This role can not upload media or publish posts, just save as draft. As a user, Contributor can see and edit only own profile.<\/li>\r\n<li><code>Subscriber<\/code> - can only see and edit own profile.<\/li>\r\n<\/ul>",
            "new_lines": "wpautop",
            "esc_html": 0
        },
        {
            "key": "field_57adb8acb3cab",
            "label": "",
            "name": "",
            "type": "message",
            "instructions": "",
            "required": 0,
            "conditional_logic": 0,
            "wrapper": {
                "width": "",
                "class": "",
                "id": ""
            },
            "message": "<h3>Clonable Roles<\/h3>\r\n\r\nThese roles are created with specific set of capabilities in order to make creating new role process easier. \r\n\r\n<ol>\r\n<li><code>User<\/code> - can see and edit own profile.<\/li>\r\n<li><code>User Manager<\/code> - can edit other's profiles. Built to manage <strong>User<\/strong> alike usergroups.<\/li>\r\n<li><code>User Administrator<\/code> - can edit other's profiles and add new users. Built to manage <strong>User<\/strong> and <strong>User Manager<\/strong> alike usergroups.<\/li>\r\n<\/ol>",
            "new_lines": "wpautop",
            "esc_html": 0
        }
    ],
    "location": [
        [
            {
                "param": "options_page",
                "operator": "==",
                "value": "theme-guidelines-usergroups"
            }
        ]
    ],
    "menu_order": 0,
    "position": "normal",
    "style": "default",
    "label_placement": "top",
    "instruction_placement": "label",
    "hide_on_screen": "",
    "active": 1,
    "description": "",
    "modified": 1471334435
}