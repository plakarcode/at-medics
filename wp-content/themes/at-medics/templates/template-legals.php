<?php
/**
 * Template Name: Legals
 *
 * The template for displaying legals pages.
 *
 * @package WordPress
 */
get_header();
	/**
	* Breadcrumbs
	*/
	get_template_part( 'partials/navigations/breadcrumbs' ); ?>

	<section class="terms-and-conditions">
		<div class="container">

			<?php
				/**
				 * Get blue heading
				 */
				get_template_part( 'partials/content/title-blue' );
			?>

			<div class="entry-content">

				<?php
					/**
					 * Get flexible content fields if any
					 */
					if ( function_exists( 'get_field' ) && get_field( 'legal_pages_content' ) ) :

						get_template_part( 'partials/flexible-templates/sections', 'legal' );

					endif; // function_exists( 'get_field' ) && get_field( 'home_sections' )
				?>

			</div>
		</div>
	</section>

<?php get_footer();