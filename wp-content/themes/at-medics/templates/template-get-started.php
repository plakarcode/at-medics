<?php
/**
 * Template Name: Get Started
 *
 * The template for displaying steps pages.
 *
 * @package WordPress
 */
get_header(); ?>

	<?php
		/**
		 * Get steps nav
		 */
		get_template_part( 'partials/navigations/steps' );
	?>

	<div class="container">

			<?php
				/**
				 * Get flexible content fields if any
				 */
				if ( function_exists( 'get_field' ) && get_field( 'get_started_content' ) ) :

					get_template_part( 'partials/flexible-templates/sections', 'get-started' );

				endif; // function_exists( 'get_field' ) && get_field( 'get_started_content' )
			?>
	</div>

<?php get_footer();