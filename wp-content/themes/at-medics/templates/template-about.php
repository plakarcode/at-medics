<?php
/**
 * Template Name: Information Pages
 *
 * The template for displaying about page and it's children.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' );

	/**
	 * Get subnav
	 */
	get_template_part( 'partials/navigations/subnav' );

	/**
	 * Get flexible content fields if any
	 */
	if ( function_exists( 'get_field' ) && get_field( 'about_sections' ) ) :

		get_template_part( 'partials/flexible-templates/sections', 'about' );

	endif; // function_exists( 'get_field' ) && get_field( 'about_sections' )

get_footer();