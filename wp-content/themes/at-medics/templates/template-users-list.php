<?php
/**
 * Template Name: User List
 *
 * The Template for displaying users list.
 *
 * Consider rewriting rules for authors
 * @link http://wordpress.stackexchange.com/a/17142
 *
 * @link https://codex.wordpress.org/Class_Reference/WP_User_Query
 * @link https://codex.wordpress.org/Function_Reference/the_author_meta
 * @package WordPress
 */
get_header(); ?>

<div class="container">

	<?php
		/**
		 * This page can not be seen by guests
		 */
		if ( ! is_user_logged_in() ) {
			echo '<h3>Sorry, you have to be logged in to see this page</h3>';
			return;
		}

	/**
	 * Get currently logged in user
	 * @var obj WP_User Object
	 */
	$user = wp_get_current_user();
	/**
	 * Userdata object
	 *
	 * We are using 'get_userdata' because 'wp_get_current_user'
	 * doesn't return values for $user->roles
	 * @var obj WP_User Object
	 */
	$userdata = get_userdata( $user->ID );
	/**
	 * Get allowed rols for user
	 * @var array
	 */
	$admins = user_get_allowed_roles();
	/**
	 * Get all roles with users, built in and custom
	 * @var array
	 */
	$all = house_all_roles();
	/**
	 * Declare array of allowed roles to be used in search query
	 * @var array
	 */
	$roles = array();

	/**
	 * If site administrator,
	 * show all usergroups with users assigned
	 */
	if ( in_array( 'administrator', $userdata->roles ) ) :
		/**
		 * Currently logged in user can search
		 * all users in $all usergroups
		 * @var array
		 */
		$roles = $all;

		/**
		 * Get select form populated with all usergroups
		 * currently logged in user can manage
		 */
		echo 'Select all users from usergroup or search users by name, username or email address. Search terms can be whole or just parts of mentioned parameters.';
		echo house_select_usergroup( $all );
		echo house_search_user();
	/**
	 * If not administrator and we have set
	 * usergroups who manage other groups
	 */
	elseif ( $admins ) :

		foreach ( $admins as $admin => $allowed ) :
			/**
			 * If currently logged in user has as a role
			 * the one that can manage other usergroups
			 */
			if ( in_array( $admin, $userdata->roles ) ) :
				/**
				 * Currently logged in user can search
				 * all users in $allowed usergroups
				 * @var array
				 */
				$roles = $allowed;
				/**
				 * How many usergroups currently logged in user can manage
				 * @var int
				 */
				$count = count( $allowed );
				/**
				 * If currently logged in user cana manage only one usergroup
				 * set the $_GET['usergroup'] to that one
				 * @var bool
				 */
				if ( $count == 1 ) :
					$_GET['usergroup'] = $allowed[0];

					echo 'Search users by name, username or email address. Search terms can be whole or just parts of mentioned parameters.';
				/**
				 * If currently logged in user can manage more than
				 * one usergroup, display the select form populated with
				 * all those usergroups
				 */
				elseif ( $count > 1 ) :
					echo 'Select all users from usergroup or search users by name, username or email address. Search terms can be whole or just parts of mentioned parameters.';
					echo house_select_usergroup( $allowed );
				endif; // $count == 1

				echo house_search_user();

			endif; // in_array( $admin, $userdata->roles )
		endforeach; // $admins as $admin
	endif; // in_array( 'administrator', $userdata->roles )

	$args = '';
	/**
	 * If select form is submitted,
	 * set the $role to selected one,
	 * show subscribers otherwise.
	 */
	if ( isset( $_GET['usergroup'] ) ) {
		$role = $_GET['usergroup'];
		/**
		 * Set query arguments
		 * @var array
		 */
		$args = array(
			'role' => $role
		);
	}
	/**
	 * If search term is submitted
	 * run the query for it
	 */
	if ( isset( $_GET['user'] ) ) {
		$search = $_GET['user'];
		/**
		 * Set query arguments
		 * @var array
		 */
		$args = array(
			'role__in' => $roles,
			'search' => '*' . esc_attr( $search ) . '*',
			'search_columns' => array(
				'user_login',
				'user_nicename',
				'user_email'
			)
		);
	}

	/**
	 * Perform user query
	 * @var WP_User_Query
	 */
	$wp_user_query = new WP_User_Query( $args );
	// Get the results
	$authors = $wp_user_query->get_results();

	$output = '';

	// Check for results
	if ( ! empty( $authors ) ) :

		$output .= '<ul class="list-bare table userlist">';

			$output .= '<li class="thead">';

				$output .= '<ul class="list-bare list-inline">';

					$output .= '<li class="th"><span>ID</span></li>';
					$output .= '<li class="th"><span>Username</span></li>';
					$output .= '<li class="th"><span>Display name</span></li>';
					$output .= '<li class="th"><span>Email</span></li>';
					$output .= '<li class="th"><span>Registered</span></li>';

				$output .= '</ul><!-- list-bare list-inline -->';

			$output .= '</li><!-- thead -->';

			$output .= '<li class="tbody">';

			// loop through each author
			foreach ( $authors as $author ) :
				/**
				 * User capabilities
				 * @var array
				 */
				$user_capabilities = $author->caps;
				/**
				 * User roles
				 * @var array Array of user roles ids
				 */
				$user_roles = $author->roles;
				/**
				 * Build the edit user link
				 * @var string
				 */
				$edit_user = admin_url( 'user-edit.php?user_id=' . $author->ID );
				/**
				 * Build the author archive link
				 * @var string
				 */
				$user_archive = get_author_posts_url( $author->ID );
				/**
				 * Get the registered date
				 * @var string
				 */
				$user_registered = $author->user_registered;
				// $registered = date( "d F, Y", strtotime( $user_registered ) ); // 04 August, 2016
				$registered = date( "j M, Y", strtotime( $user_registered ) ); // 4 Aug, 2016

				$output .= '<ul class="list-bare list-inline">';

					$output .= '<li class="td">' . $author->ID . '</li>';
					$output .= '<li class="td"><a href="' . $edit_user . '">' . $author->user_nicename . '</a></li>';
					$output .= '<li class="td"><a href="' . $user_archive . '">' . $author->display_name . '</a></li>';
					$output .= '<li class="td"><a href="mailto:' . $author->user_email . '">' . $author->user_email . '</a></li>';
					$output .= '<li class="td">' . $registered . '</li>';

				$output .= '</ul><!-- list-bare list-inline -->';

			endforeach; // $authors as $author

			$output .= '</li><!-- tbody -->';

		$output .= '</ul><!-- table userlist -->';

	else :

		$output .= 'No members found';

	endif; // ! empty( $authors )

	echo $output;

	wp_reset_query(); ?>

</div><!-- container -->

<?php get_footer();