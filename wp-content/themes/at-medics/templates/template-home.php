<?php
/**
 * Template Name: Home page
 *
 * The template for displaying home page.
 *
 * @package WordPress
 */
get_header();

	/**
	 * Get hero slider
	 */
	get_template_part( 'partials/sliders/hero' );

	/**
	 * Get flexible content fields if any
	 */
	if ( function_exists( 'get_field' ) && get_field( 'home_sections' ) ) :

		get_template_part( 'partials/flexible-templates/sections', 'home' );

	endif; // function_exists( 'get_field' ) && get_field( 'home_sections' )

get_footer();