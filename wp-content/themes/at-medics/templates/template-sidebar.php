<?php
/**
 * Template Name: Page with sidebar
 *
 * Page template for rendering page content with sidebar.
 *
 * @package  WordPress
 */
get_header();

	/**
	 * Get the featured image
	 * if one is set
	 */
	if ( function_exists( 'house_featured_image' ) ) {
		/**
		 * Translators: image size, link to post, echo the image
		 */
		house_featured_image( 'full', false, true );
	}

	// start loop
	while ( have_posts() ) : the_post();

		get_template_part( 'content', 'page' );

	endwhile; // end of the loop.

get_sidebar();

get_footer();