<?php
/**
 * Template Name: Case Study
 *
 * The template for displaying case study page.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' );

	/**
	 * Get subnav
	 */
	get_template_part( 'partials/navigations/subnav' ); ?>

	<div class="container">
		<div class="entry-content">

		<?php
			/**
			 * Get blue heading
			 */
			get_template_part( 'partials/content/title-blue' );

			/**
			 * Get flexible content fields if any
			 */
			if ( function_exists( 'get_field' ) && get_field( 'case_study_sections' ) ) :

				get_template_part( 'partials/flexible-templates/sections', 'case-study' );

			endif; // function_exists( 'get_field' ) && get_field( 'case_study_sections' ) ?>

		</div><!-- entry-content -->
	</div><!-- container -->

<?php get_footer();