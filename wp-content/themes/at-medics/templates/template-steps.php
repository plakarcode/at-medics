<?php
/**
 * Template Name: Steps
 *
 * The template for displaying steps pages.
 *
 * @package WordPress
 */
acf_form_head();
get_header();

	/**
	 * Get steps nav
	 */
	get_template_part( 'partials/navigations/steps' ); ?>

	<div class="container">
		<div class="page-content">

			<?php if ( have_rows( 'steps_checklist' ) ) :
				/**
				 * Initiate counter
				 * @var integer
				 */
				$check = 1; ?>

				<aside class="sidebar">
					<div class="sidebar__list">
						<h4><?php
							/**
							 * Get checklist title
							 */
							if ( get_field( 'steps_checklist_title' ) ) {
								the_field( 'steps_checklist_title' );
							} else {
								_e( 'Check list', 'house' );
							}
						?></h4>

						<form method="post">
							<?php while ( have_rows( 'steps_checklist' ) ) : the_row( 'steps_checklist' );
								/**
								 * Counter will be reused, save number in var
								 * @var int
								 */
								$check_number = $check++; ?>

								<div class="checkbox_wrap">
									<input class="styled-checkbox" type="checkbox" id="chk-<?php echo $check_number; ?>" name="chk-<?php echo $check_number; ?>" <?php // disabled // for now we have no validation ?> />
									<label for="chk-<?php echo $check_number; ?>"><span><b></b></span><?php the_sub_field( 'item' ); ?></label>
								</div><!-- checkbox_wrap -->

							<?php endwhile; // have_rows( 'steps_checklist' )  ?>
						</form>

						<div class="info-popup-wrap">
							<div class="info-popup">
								<div class="info-popup__item">
									<?php echo house_svg_icon( 'info' ); ?>
								</div><!-- end of .info-popup__item -->
								<div class="info-popup__item">
									<span><?php _e( "You can't move to next step before completing checklist!", "house" ); ?></span>
									<a href="javascript:;" class="btn btn--primary btn--info">ok</a>
								</div><!-- end of .info-popup__item -->
							</div><!-- end of .info-popup -->
						</div><!-- end of .info-popup-wrap -->

					</div><!-- /.sidebar__list -->
				</aside><!-- sidebar -->
			<?php endif; // have_rows( 'steps_checklist' ) ?>

			<div class="main-content">
				<div class="entry-content">

					<?php
						/**
						 * Get subheading and append it to title
						 */
						if ( get_field( 'step_subheading' ) ) {
							$subheading = get_field( 'step_subheading' );
						} else {
							$subheading = '';
						}

						the_title( '<h1>', ' <span class="text-style-2">' . $subheading . '</span></h1>' );


						/**
						 * Get flexible content fields if any
						 */
						if ( function_exists( 'get_field' ) && get_field( 'content_fields' ) ) :

							get_template_part( 'partials/flexible-templates/sections', 'steps' );

						endif; // function_exists( 'get_field' ) && get_field( 'content_fields' )
					?>
				</div><!-- entry-content -->
			</div><!-- main-content -->
		</div><!-- page-content -->

		<?php
			/**
			 * Get prev/next nav for steps
			 */
			get_template_part( 'partials/navigations/steps', 'prev-next' );
		?>

	</div><!-- container -->

<?php get_footer();