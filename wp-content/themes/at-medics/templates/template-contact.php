<?php
/**
 * Template Name: Contact
 *
 * The template for displaying contact page.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' );

	/**
	 * Get subnav
	 */
	get_template_part( 'partials/navigations/subnav' ); ?>

	<div class="container">
		<div class="map">
			<div id="map"></div><!-- /#map -->
			<div class="container container--narrow">
				<?php
					/**
					 * Get blue heading
					 */
					get_template_part( 'partials/content/title-blue' ); ?>

				<div class="contact-form">
					<div class="contact-form__item">
					<?php
						/**
						 * Get contact form
						 */
						get_template_part( 'partials/forms/contact' ); ?>
					</div><!-- end of .contact-form__item -->

					<div class="contact-form__item flex">
						<div class="contact-form__item-data">
						<?php
							/**
							 * Get address section title
							 */
							if ( get_field( 'address_section_title' ) ) : ?>
								<h2><?php the_field( 'address_section_title' ); ?></h2>
							<?php endif; // get_field( 'address_section_title' )

							/**
							 * Get contact info
							 */
							$info = get_contact_info_array(); ?>

							<ul class="list-bare list-contact">
								<li><?php echo $info['office']; ?></li>
								<li><?php echo $info['street']; ?></li>
								<li><?php echo $info['city']; ?></li>
							</ul>

							<strong><?php the_field( 'business_hours_title' ); ?></strong>
							<ul class="list-bare list-contact">
								<li><?php echo $info['business_hours']; ?></li>
							</ul>

							<a href="#" class="text-style-1" onclick="myClick(0);">View on map</a>
						</div><!-- end of .contact-form__item-data -->

					</div><!-- contact-form__item flex -->
				</div><!-- contact-form -->

			</div><!-- /.container container--narrow -->
		</div><!-- /.map -->
	</div><!-- container -->

	<?php

get_footer();