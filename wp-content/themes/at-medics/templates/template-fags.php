<?php
/**
 * Template Name: FAQs
 *
 * The template for displaying members page.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' );

	/**
	 * Get subnav
	 */
	get_template_part( 'partials/navigations/subnav' ); ?>

	<div class="container">
		<div class="faq__hero">
			<?php
				/**
				 * Get the icon
				 */
				echo house_image( 'help.svg', 'icon-help' );

				/**
				 * Get blue heading
				 */
				get_template_part( 'partials/content/title-blue' );
			?>
			<a href="">Click for details</a>
		</div><!-- faq__hero -->

		<?php
			/**
			 * Get faqs
			 */
			get_template_part( 'partials/content/faq' ); ?>

	</div><!-- container -->

	<?php

get_footer();