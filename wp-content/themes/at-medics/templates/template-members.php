<?php
/**
 * Template Name: Our Members
 *
 * The template for displaying members page.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' );

	/**
	 * Get subnav
	 */
	get_template_part( 'partials/navigations/subnav' ); ?>

	<div class="container">
		<?php
			/**
			 * Get blue heading
			 */
			get_template_part( 'partials/content/title-blue' );

			/**
			 * Get intro, if any
			 */
			if ( get_field( 'page_intro' ) ) : ?>
				<p class="text-highlighted"><?php the_field( 'page_intro' ); ?></p>
			<?php endif; // get_field( 'page_intro' )

			/**
			 * Get members
			 */
			get_template_part( 'partials/content/archive', 'members' );
		?>
	</div><!-- container -->

	<?php

get_footer();