<?php
/**
 * Template Name: Resources
 *
 * The template for displaying resources page.
 *
 * @package WordPress
 */
get_header();
	/**
	 * Breadcrumbs
	 */
	get_template_part( 'partials/navigations/breadcrumbs' ); ?>

	<div class="container">

		<?php
			/**
			 * Get blue heading
			 */
			get_template_part( 'partials/content/title-blue' );
		
			/**
			 * Get intro, if any
			 */
			if ( get_field( 'page_intro' ) ) : ?>
				<p class="text-highlighted"><?php the_field( 'page_intro' ); ?></p>
			<?php endif; // get_field( 'page_intro' )
		
			/**
			 * Get Resources
			 */
			get_template_part( 'partials/content/archive', 'resources' );
		?>

	</div><!-- container -->

	<?php

get_footer();